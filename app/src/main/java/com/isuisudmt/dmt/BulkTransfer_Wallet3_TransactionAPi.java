package com.isuisudmt.dmt;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface BulkTransfer_Wallet3_TransactionAPi {
//    @POST("/transferreq")
    @POST()
    @Headers("Content-Type: application/json")
//    Call<TransactionResponse> getTransectionReport(@Header("Authorization") String token, @Body TransactionRequest transactionBody/*, @Url String url*/);
    Call<BulkTransactionResponse> getTransectionReport(@Header("Authorization") String token, @Body Wallet3TransactionRequest transactionBody, @Url String url);
}
