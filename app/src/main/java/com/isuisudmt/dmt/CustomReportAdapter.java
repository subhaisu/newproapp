package com.isuisudmt.dmt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;

import java.util.ArrayList;
import java.util.List;

public class CustomReportAdapter extends RecyclerView.Adapter<CustomReportAdapter.ViewHolder> implements Filterable {

    private Context context;
    private List<FinoTransactionReports> finoTransactionReports;
    private List<FinoTransactionReports> finoTransactionReportsFiltered;
    public CustomReportAdapter(Context context , List finoTransactionReports) {
        this.context = context;
        this.finoTransactionReports = finoTransactionReports;
        this.finoTransactionReportsFiltered = finoTransactionReports;
    }




    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.dmt_report_row, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        try{
        holder.itemView.setTag(finoTransactionReportsFiltered.get(position));

        FinoTransactionReports pu = finoTransactionReportsFiltered.get(position);

         holder.accNo.setText("Account No: "+pu.getToAccount());
        // holder.previousAmount.setText(String.valueOf(pu.getPreviousAmount()));
       // holder.amountTransacted.setText(String.valueOf(pu.getAmountTransacted()));
            holder.amount.setText(String.valueOf("₹"+pu.getAmountTransacted()));
            holder.bankName.setText(pu.getBankName());
        //holder.beniMobile.setText(pu.getBeniMobile());
        //holder.benificiaryName.setText(pu.getBenificiaryName());
        //holder.transactionMode.setText(pu.getTransactionMode());
        holder.tnxType.setText("Txn ID: "+pu.getApiTid()+"  "+"( "+pu.getTransactionMode()+" )");
        holder.dateTime.setText("Date: "+ Util.getDateFromTime(pu.getCreatedDate()));
        holder.route.setText("Route: "+pu.getRouteID());
        if (pu.getStatus().equalsIgnoreCase("Success")){
            holder.statusTextView.setText(pu.getStatus());
            holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.color_report_green));
        }
        else {
            holder.statusTextView.setText(pu.getStatus());
            holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.color_report_red));

        }

        if (pu.getApiComment()!=null){
            holder.api_comment.setText(""+pu.getApiComment());
        }




        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return finoTransactionReportsFiltered.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView dateTime,statusTextView,bankName,amount,accNo,tnxType,route,api_comment;


        public ViewHolder(View itemView) {
            super(itemView);

            dateTime = itemView.findViewById(R.id.dateTime);
            statusTextView = (TextView) itemView.findViewById(R.id.statusTextView);
            bankName = (TextView) itemView.findViewById(R.id.bankName);
            amount = (TextView) itemView.findViewById(R.id.amount);
            accNo = (TextView) itemView.findViewById(R.id.accNo);
            tnxType = (TextView) itemView.findViewById(R.id.tnxType);
            route = (TextView) itemView.findViewById(R.id.route);
            api_comment =  itemView.findViewById(R.id.api_comment);

        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    finoTransactionReportsFiltered = finoTransactionReports;
                } else {
                    List<FinoTransactionReports> filteredList = new ArrayList<>();
                    for (FinoTransactionReports row : finoTransactionReports) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getApiTid().toLowerCase().contains(charString.toLowerCase()) || row.getApiTid().contains(charSequence)) {
                            filteredList.add(row);
                        }
                        if (String.valueOf(row.getBankName()).toLowerCase().contains(charString.toLowerCase()) || String.valueOf(row.getBankName()).contains(charSequence)) {
                            filteredList.add(row);
                        }

                        if (String.valueOf(row.getId()).toLowerCase().contains(charString.toLowerCase()) || String.valueOf(row.getId()).contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }

                    finoTransactionReportsFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = finoTransactionReportsFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                finoTransactionReportsFiltered = (ArrayList<FinoTransactionReports>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
