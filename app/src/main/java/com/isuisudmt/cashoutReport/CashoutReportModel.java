package com.isuisudmt.cashoutReport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CashoutReportModel {
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("previousAmount")
    @Expose
    private Double previousAmount;
    @SerializedName("amountTransacted")
    @Expose
    private Integer amountTransacted;
    @SerializedName("apiTId")
    @Expose
    private String apiTId;
    @SerializedName("balanceAmount")
    @Expose
    private Double balanceAmount;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("transactionType")
    @Expose
    private String transactionType;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("distributerName")
    @Expose
    private String distributerName;
    @SerializedName("masterName")
    @Expose
    private String masterName;

    @SerializedName("userTrackId")
    @Expose
    private String userTrackId;

    @SerializedName("cardDetail")
    @Expose
    private String cardDetail;

    @SerializedName("createdDate")
    @Expose
    private String createdDate;

    @SerializedName("updatedDate")
    @Expose
    private String updatedDate;

    @SerializedName("operationPerformed")
    @Expose
    private String operationPerformed;

    @SerializedName("transactionMode")
    @Expose
    private String transactionMode;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getPreviousAmount() {
        return previousAmount;
    }

    public void setPreviousAmount(Double previousAmount) {
        this.previousAmount = previousAmount;
    }

    public Integer getAmountTransacted() {
        return amountTransacted;
    }

    public void setAmountTransacted(Integer amountTransacted) {
        this.amountTransacted = amountTransacted;
    }

    public String getApiTId() {
        return apiTId;
    }

    public void setApiTId(String apiTId) {
        this.apiTId = apiTId;
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDistributerName() {
        return distributerName;
    }

    public void setDistributerName(String distributerName) {
        this.distributerName = distributerName;
    }

    public String getMasterName() {
        return masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getUserTrackId() {
        return userTrackId;
    }

    public void setUserTrackId(String userTrackId) {
        this.userTrackId = userTrackId;
    }

    public String getCardDetail() {
        return cardDetail;
    }

    public void setCardDetail(String cardDetail) {
        this.cardDetail = cardDetail;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String  createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getOperationPerformed() {
        return operationPerformed;
    }

    public void setOperationPerformed(String operationPerformed) {
        this.operationPerformed = operationPerformed;
    }

    public String getTransactionMode() {
        return transactionMode;
    }

    public void setTransactionMode(String transactionMode) {
        this.transactionMode = transactionMode;
    }
}
