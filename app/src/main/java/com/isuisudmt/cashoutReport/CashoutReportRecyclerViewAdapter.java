package com.isuisudmt.cashoutReport;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;

import java.util.ArrayList;
import java.util.List;

public class CashoutReportRecyclerViewAdapter extends RecyclerView.Adapter<CashoutReportRecyclerViewAdapter.ReportViewhOlder> implements Filterable {

private List<CashoutReportModel> reportModelListFiltered;
private List<CashoutReportModel> reportModels;
    private IMethodCaller listener;
    private int lastSelectedPosition = -1;
    Context context;


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    reportModelListFiltered = reportModels;
                } else {
                    List<CashoutReportModel> filteredList = new ArrayList<>();
                    for (CashoutReportModel row : reportModels) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (
                                row.getUserName().toLowerCase().contains(charString.toLowerCase())||
                                        row.getStatus().toLowerCase().contains(charString.toLowerCase()) ||
                                        row.getId().toString().contains(charString.toLowerCase())||
                                        row.getOperationPerformed().toLowerCase().contains(charString.toLowerCase())

                                ) {
                            filteredList.add(row);
                        }
                    }

                    reportModelListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = reportModelListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                reportModelListFiltered = (ArrayList<CashoutReportModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public class ReportViewhOlder extends RecyclerView.ViewHolder {

   /* public ImageView refreshImage;
    public TextView updatedDateTextView;
    public TextView statusTextView,createdDateTextView,apiDateTextView;
    public TextView rrnTextView,transactionTypeTextView,IdTextView,apiCommentTextView,amountTransactedTextView,userNameReportTextView,opertionPerformedTextView,cardNumberTextView;

*/
    public TextView openingBal,closingBal,dateTime,statusTextView,tnxId,tnxType,userName,createdDate,operationPerformed,flag,amount;
    LinearLayout mainView,topLayout,buttomlayout;


    public ReportViewhOlder(View view) {
        super ( view );



        dateTime = view.findViewById(R.id.dateTime);
        amount = view.findViewById(R.id.amount);
        statusTextView = view.findViewById(R.id.statusTextView);
        tnxId = view.findViewById(R.id.tnxId);
        tnxType = view.findViewById(R.id.tnxType);
        userName = view.findViewById(R.id.userName);
        createdDate = view.findViewById(R.id.createdDate);
        operationPerformed = view.findViewById(R.id.operationPerformed);
        flag =view.findViewById(R.id.flag);
        topLayout = view.findViewById(R.id.topLayout);
        buttomlayout = view.findViewById(R.id.buttomlayout);
        mainView =view.findViewById(R.id.mainView);
        openingBal = view.findViewById(R.id.openingBal);
        closingBal = view.findViewById(R.id.closingBal);
        topLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttomlayout.setVisibility(View.VISIBLE);
                //mainView.set
                if(flag.getText().toString().equalsIgnoreCase("false")){
                    buttomlayout.setVisibility(View.VISIBLE);
                    flag.setText("true");
                    mainView.setBackgroundColor(Color.parseColor("#DCE0E0"));
                }else{
                    mainView.setBackgroundColor(Color.parseColor("#00FFFFFF"));
                    buttomlayout.setVisibility(View.GONE);
                    flag.setText("false");
                }
            }
        });


    }
}

    public CashoutReportRecyclerViewAdapter(Context context, List<CashoutReportModel> reportModels, IMethodCaller listener) {
        this.reportModels = reportModels;
        this.reportModelListFiltered = reportModels;
        this.listener =listener;
        this.context = context;

    }

    public ReportViewhOlder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cashout_report_row, parent, false);

        return new ReportViewhOlder(itemView);
    }

    public void onBindViewHolder(ReportViewhOlder holder, int position) {
        CashoutReportModel reportModel = reportModelListFiltered.get(position);



        if(reportModel.getUserName() != null && !reportModel.getUserName().matches("")){
            holder.userName.setVisibility(View.VISIBLE);
            holder.userName.setText(reportModel.getUserName());
        }else{
            holder.userName.setVisibility(View.GONE);
        }
        if(reportModel.getUpdatedDate() != null && !reportModel.getUpdatedDate().matches("")){
            holder.createdDate.setVisibility(View.VISIBLE);
            holder.createdDate.setText(reportModel.getOperationPerformed());
        }else{
            holder.createdDate.setVisibility(View.GONE);
        }
        if(reportModel.getStatus() != null && !reportModel.getStatus().matches("")){
            holder.statusTextView.setVisibility(View.VISIBLE);
            if(reportModel.getStatus().equalsIgnoreCase("SUCCESS")){
                holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.color_report_green));
            }else{
                holder.statusTextView.setTextColor(ContextCompat.getColor(context, R.color.red));
            }
            holder.statusTextView.setText(reportModel.getStatus());
        }else{
            holder.statusTextView.setVisibility(View.GONE);
        }


       /* if(reportModel.getOperationPerformed() != null && !reportModel.getOperationPerformed().matches("")){
            holder.operationPerformed.setVisibility(View.VISIBLE);
            holder.operationPerformed.setText(reportModel.getOperationPerformed());
        }else{
            holder.operationPerformed.setVisibility(View.GONE);
        }*/
        if(reportModel.getId() != null){
            holder.tnxId.setVisibility(View.VISIBLE);
            holder.tnxId.setText("Txn ID: "+reportModel.getId());
        }else{
            holder.tnxId.setVisibility(View.GONE);
        }
        if(reportModel.getTransactionType() != null){
            holder.tnxType.setVisibility(View.VISIBLE);
            holder.tnxType.setText("Txn ID: "+reportModel.getTransactionType());
        }else{
            holder.tnxType.setVisibility(View.GONE);
        }

        if(reportModel.getAmountTransacted() != null){
            holder.amount.setVisibility(View.VISIBLE);
            holder.amount.setText("₹ "+reportModel.getAmountTransacted());
        }else{
            holder.amount.setVisibility(View.GONE);
        }
        if(reportModel.getCreatedDate() != null && !reportModel.getCreatedDate().matches("")){
            holder.dateTime.setVisibility(View.VISIBLE);
            holder.dateTime.setText( "Date: "+Util.getDateFromTime(Long.parseLong(reportModel.getCreatedDate())));
        }else{
            holder.dateTime.setVisibility(View.GONE);
        }
        holder.openingBal.setText(reportModel.getBalanceAmount().toString());
        holder.closingBal.setText(reportModel.getPreviousAmount().toString());

    }

    public int getItemCount() {
        return reportModelListFiltered.size();
    }

    public interface IMethodCaller{
        void refreshMethod(CashoutReportModel microReportModel);
    }

}
