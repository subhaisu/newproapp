package com.isuisudmt;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isuisudmt.aeps.ReportActivity;
import com.isuisudmt.bbps.ActivitybMobilePrepaid;
import com.isuisudmt.bbps.ActivitycDTH;
import com.isuisudmt.bbps.ActivityaElectricityBill;
import com.isuisudmt.bbps.ActivitybMobilePostpaid;
import com.isuisudmt.bbps.ActivitydBroadbandPostpaid;
import com.isuisudmt.bbps.ActivityeWater;
import com.isuisudmt.bbps.ActivityfLandlinePostpaid;
import com.isuisudmt.bbps.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.isuisudmt.bbps.utils.Const.URL_IS_UPDATED;
import static com.isuisudmt.bbps.utils.Const.URL_VIEW_REQUIRED_INFO;
import static com.isuisudmt.utils.Constants.BBPS_PREF;
import static com.isuisudmt.utils.Constants.BROADBAND_POSTPAID;
import static com.isuisudmt.utils.Constants.DTH;
import static com.isuisudmt.utils.Constants.ELECTRICITY;
import static com.isuisudmt.utils.Constants.LANDLINE_POSTPAID;
import static com.isuisudmt.utils.Constants.MOBILE_POSTPAID;
import static com.isuisudmt.utils.Constants.SF_AGENT_ID;
import static com.isuisudmt.utils.Constants.SF_KEYWORD;
import static com.isuisudmt.utils.Constants.SF_LAT_LONG;
import static com.isuisudmt.utils.Constants.SF_MOBILE_NUMBER;
import static com.isuisudmt.utils.Constants.SF_PINCODE;
import static com.isuisudmt.utils.Constants.SF_TERMINAL_ID;
import static com.isuisudmt.utils.Constants.WATER;

/**
 * A simple {@link Fragment} subclass.
 */
public class PayBillFragment1 extends Fragment {

    View view;
    String userName = "", tokenStr = "";
    SessionManager session;
    SharedPreferences sp, spBbps;
    public static final String ISU_PREF = "isuPref";
    public static final String USER_NAME = "userNameKey";
    private static final String TAG = ReportActivity.class.getSimpleName();
    Const con = new Const();

    public PayBillFragment1() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_recharge, container, false);

        session = new SessionManager(getActivity());
        sp = getActivity().getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);
        spBbps = getActivity().getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE);

        HashMap<String, String> _user = session.getUserDetails();
        HashMap<String, String> user = session.getUserSession();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        userName = Constants.USER_NAME;

        LinearLayout mobile_postpaid = view.findViewById(R.id.postpaid);
        LinearLayout dth = view.findViewById(R.id.dth);
        LinearLayout electricity = view.findViewById(R.id.electricity);
        LinearLayout broadband = view.findViewById(R.id.broadband);
        LinearLayout water = view.findViewById(R.id.water);
        LinearLayout prepaid = view.findViewById(R.id.prepaid);

        mobile_postpaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isApiCalledInSession(MOBILE_POSTPAID);
            }
        });

        dth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isApiCalledInSession(DTH);
            }
        });

        electricity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isApiCalledInSession(ELECTRICITY);
            }
        });

        broadband.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isApiCalledInSession(BROADBAND_POSTPAID);
            }
        });

        water.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isApiCalledInSession(WATER);
            }
        });
        prepaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // isApiCalledInSession(WATER);
                Intent intent = new Intent(getActivity(), ActivitybMobilePrepaid.class);
                getActivity().startActivity(intent);
            }
        });




        return view;
    }

    private void isApiCalledInSession(String CATEGORY) {
        if (Const.isUpdatedResponse.equals(""))
            isUpdated(CATEGORY);
        else {
            //Perform here
            try {
                JSONObject objIsUpdated = new JSONObject(Const.isUpdatedResponse);
                int status = objIsUpdated.getInt("status");
                String statusDescription = objIsUpdated.getString("statusDescription");

                if (status == 0) {
                    if (Const.viewRequiredInfoResponse.equals(""))
                        viewRequiredInfo(CATEGORY);
                    else
                        intentToActivity(Const.viewRequiredInfoResponse, CATEGORY);

                } else {
                    Toast.makeText(getActivity(), "" + statusDescription, Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void intentToActivity(String response, String CATEGORY) {
        try {
            JSONObject objViewRqrdDetails = new JSONObject(response);
            String _status = objViewRqrdDetails.getString("status");
            String pincode = objViewRqrdDetails.getString("pincode");
            String mobilenumber = objViewRqrdDetails.getString("mobilenumber");
            String latlong = objViewRqrdDetails.getString("latlong");
            String terminalid = objViewRqrdDetails.getString("terminalid");
            String agentid = objViewRqrdDetails.getString("agentid");
            String city = objViewRqrdDetails.getString("city");
            String state = objViewRqrdDetails.getString("state");
            String keyword = objViewRqrdDetails.getString("keyword");
            String accountcode = objViewRqrdDetails.getString("accountcode");

            SharedPreferences.Editor editor = spBbps.edit();
            editor.putString(SF_PINCODE, pincode);
            editor.putString(SF_MOBILE_NUMBER, mobilenumber);
            editor.putString(SF_LAT_LONG, latlong);
            editor.putString(SF_TERMINAL_ID, terminalid);
            editor.putString(SF_AGENT_ID, agentid);
            editor.putString(SF_KEYWORD, keyword);
            editor.apply();


            if (CATEGORY.equals(ELECTRICITY)) {
                Intent intent = new Intent(getActivity(), ActivityaElectricityBill.class);
                getActivity().startActivity(intent);
            } else if (CATEGORY.equals(MOBILE_POSTPAID)) {
                Intent intent = new Intent(getActivity(), ActivitybMobilePostpaid.class);
                getActivity().startActivity(intent);
            } else if (CATEGORY.equals(DTH)) {
                Intent intent = new Intent(getActivity(), ActivitycDTH.class);
                getActivity().startActivity(intent);
            } else if (CATEGORY.equals(BROADBAND_POSTPAID)) {
                Intent intent = new Intent(getActivity(), ActivitydBroadbandPostpaid.class);
                getActivity().startActivity(intent);
            } else if (CATEGORY.equals(Const.WATER)) {
                Intent intent = new Intent(getActivity(), ActivitybMobilePrepaid.class);
                getActivity().startActivity(intent);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void isUpdated(String CategoryID) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        AndroidNetworking.post(URL_IS_UPDATED)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", tokenStr)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            dialog.dismiss();
                            Log.d(TAG, "isUpdatedResponse: "+response.toString());

                            Const.isUpdatedResponse = response.toString();

                            int status = response.getInt("status");
                            String statusDescription = response.getString("statusDescription");

                            if (status == 0) {
                                viewRequiredInfo(CategoryID);
                            } else {
                                Toast.makeText(getActivity(), "" + statusDescription, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {

                            dialog.dismiss();
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());

                            Log.d(TAG, "isUpdatedResponse: "+errorObject.toString());

                            String statusDescription = errorObject.optString("statusDescription");
                            String statusDesc = errorObject.optString("statusDesc");
                            if (statusDescription.equals(""))
                                Toast.makeText(getActivity(), statusDesc, Toast.LENGTH_LONG).show();
                            else
                                Toast.makeText(getActivity(), statusDescription, Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void viewRequiredInfo(String CategoryID) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();
        try {
            obj.put("username", userName);
            Log.d(TAG,"obj"+obj);

            AndroidNetworking.post(URL_VIEW_REQUIRED_INFO)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            dialog.dismiss();

                            Const.viewRequiredInfoResponse = response.toString();

                            intentToActivity(response.toString(), CategoryID);

                        }

                        @Override
                        public void onError(ANError anError) {
                            try {
                                dialog.dismiss();
                                JSONObject errorObject = new JSONObject(anError.getErrorBody());
                                String statusDescription = errorObject.optString("statusDescription");
                                String statusDesc = errorObject.optString("statusDesc");
                                if (statusDescription.equals(""))
                                    Toast.makeText(getActivity(), statusDesc, Toast.LENGTH_LONG).show();
                                else
                                    Toast.makeText(getActivity(), statusDescription, Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
