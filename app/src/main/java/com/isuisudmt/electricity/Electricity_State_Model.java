package com.isuisudmt.electricity;

public class Electricity_State_Model {

    String state_id;
    String state_nm;

    public Electricity_State_Model(String state_id, String state_nm) {
        this.state_id = state_id;
        this.state_nm = state_nm;
    }

    public Electricity_State_Model() {
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getState_nm() {
        return state_nm;
    }

    public void setState_nm(String state_nm) {
        this.state_nm = state_nm;
    }

    @Override
    public String toString() {
        return  state_nm;
    }
}
