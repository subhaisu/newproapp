package com.isuisudmt.electricity;

public class BillerModel {

    String biller_Id;
    String biller_Name;

    public BillerModel(String biller_Id, String biller_Name) {
        this.biller_Id = biller_Id;
        this.biller_Name = biller_Name;
    }

    public BillerModel() {
    }

    public String getBiller_Id() {
        return biller_Id;
    }

    public void setBiller_Id(String biller_Id) {
        this.biller_Id = biller_Id;
    }

    public String getBiller_Name() {
        return biller_Name;
    }

    public void setBiller_Name(String biller_Name) {
        this.biller_Name = biller_Name;
    }
}
