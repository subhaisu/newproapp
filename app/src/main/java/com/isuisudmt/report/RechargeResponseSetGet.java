package com.isuisudmt.report;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RechargeResponseSetGet {
    @SerializedName("amountTransacted")
    @Expose
    private Integer amountTransacted;
    @SerializedName("apiComment")
    @Expose
    private String apiComment;
    @SerializedName("apiName")
    @Expose
    private String apiName;
    @SerializedName("balanceAmount")
    @Expose
    private Double balanceAmount;
    @SerializedName("createdDate")
    @Expose
    private long createdDate;
    @SerializedName("id")
    @Expose

    /* renamed from: id */
    private String f328id;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("operatorDescription")
    @Expose
    private String operatorDescription;
    @SerializedName("operatorTransactionId")
    @Expose
    private String operatorTransactionId;
    @SerializedName("previousAmount")
    @Expose
    private Double previousAmount;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("transactionType")
    @Expose
    private String transactionType;
    @SerializedName("updatedDate")
    @Expose
    private long updatedDate;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("userTrackId")
    @Expose
    private String userTrackId;

    public String getId() {
        return this.f328id;
    }

    public void setId(String id) {
        this.f328id = id;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName2) {
        this.userName = userName2;
    }

    public String getUserTrackId() {
        return this.userTrackId;
    }

    public void setUserTrackId(String userTrackId2) {
        this.userTrackId = userTrackId2;
    }

    public String getOperatorTransactionId() {
        return this.operatorTransactionId;
    }

    public void setOperatorTransactionId(String operatorTransactionId2) {
        this.operatorTransactionId = operatorTransactionId2;
    }

    public String getOperatorDescription() {
        return this.operatorDescription;
    }

    public void setOperatorDescription(String operatorDescription2) {
        this.operatorDescription = operatorDescription2;
    }

    public String getMobileNumber() {
        return this.mobileNumber;
    }

    public void setMobileNumber(String mobileNumber2) {
        this.mobileNumber = mobileNumber2;
    }

    public Double getPreviousAmount() {
        return this.previousAmount;
    }

    public void setPreviousAmount(Double previousAmount2) {
        this.previousAmount = previousAmount2;
    }

    public Integer getAmountTransacted() {
        return this.amountTransacted;
    }

    public void setAmountTransacted(Integer amountTransacted2) {
        this.amountTransacted = amountTransacted2;
    }

    public Double getBalanceAmount() {
        return this.balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount2) {
        this.balanceAmount = balanceAmount2;
    }

    public String getTransactionType() {
        return this.transactionType;
    }

    public void setTransactionType(String transactionType2) {
        this.transactionType = transactionType2;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status2) {
        this.status = status2;
    }

    public String getApiComment() {
        return this.apiComment;
    }

    public void setApiComment(String apiComment2) {
        this.apiComment = apiComment2;
    }

    public String getApiName() {
        return this.apiName;
    }

    public void setApiName(String apiName2) {
        this.apiName = apiName2;
    }

    public long getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(long createdDate2) {
        this.createdDate = createdDate2;
    }

    public long getUpdatedDate() {
        return this.updatedDate;
    }

    public void setUpdatedDate(long updatedDate2) {
        this.updatedDate = updatedDate2;
    }
}
