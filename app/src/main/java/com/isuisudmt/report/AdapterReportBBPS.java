package com.isuisudmt.report;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.isuisudmt.R;
import com.isuisudmt.Util;
import com.paxsz.easylink.model.AppSelectResponse;

import java.util.ArrayList;
import java.util.List;

public class AdapterReportBBPS extends RecyclerView.Adapter<AdapterReportBBPS.ViewHolder> implements Filterable {
    private Context context;
    /* access modifiers changed from: private */
    public List<PojoReportBBPS> pojoReportBBPS;
    public List<PojoReportBBPS> pojoReportBBPSFilter;
    /* access modifiers changed from: private */

    public AdapterReportBBPS(Context context2, ArrayList<PojoReportBBPS> pojoReportBBPS) {
        this.context = context2;
        this.pojoReportBBPS = pojoReportBBPS;
        this. pojoReportBBPSFilter = pojoReportBBPS;

    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_report_bbps, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PojoReportBBPS current = pojoReportBBPS.get(position);

        holder.amount_transacted_recharge.setText(String.valueOf(new StringBuilder().append("₹ ").append(current.getAmountTransacted()).toString()));
        holder.updated_date.setText(new StringBuilder().append("Date: ").append(Util.getDateFromTime(current.getUpdatedDate())).toString());
        holder.status.setText(current.getStatus());
        holder.transaction_id_recharge.setText(new StringBuilder().append("Transaction Id: ").append(String.valueOf(current.getId())).toString());
        holder.mobile_number.setText(new StringBuilder().append("Mobile No:").append(current.getMobileNumber()).toString());
        holder.operator_performed.setText(current.operationPerformed);
        holder.transaction_mode.setText(current.transactionMode);


        if (current.getStatus().equals(AppSelectResponse.SUCCESS)) {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.color_report_green));
        } else {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.red));
        }
    }

    public int getItemCount() {
        return this.pojoReportBBPSFilter.size();
    }

    public Filter getFilter() {
        return new Filter() {
            /* access modifiers changed from: protected */
            public FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    AdapterReportBBPS adapterReportBBPS = AdapterReportBBPS.this;
                    adapterReportBBPS.pojoReportBBPSFilter = adapterReportBBPS.pojoReportBBPS;
                } else {
                    List<PojoReportBBPS> filteredList = new ArrayList<>();
                    for (PojoReportBBPS row : AdapterReportBBPS.this.pojoReportBBPS) {
                        if (row.getId().contains(charString.toLowerCase()) || row.getId().contains(charSequence)) {
                            filteredList.add(row);
                        }
                        if (String.valueOf(row.getMobileNumber()).toLowerCase().contains(charString.toLowerCase()) || String.valueOf(row.getMobileNumber()).contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    AdapterReportBBPS.this.pojoReportBBPSFilter = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = AdapterReportBBPS.this.pojoReportBBPSFilter;
                return filterResults;
            }

            /* access modifiers changed from: protected */
            public void publishResults(CharSequence charSequence, FilterResults filterResults) {
                pojoReportBBPSFilter = (ArrayList) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView amount_transacted_recharge;
        public TextView mobile_number;
        public TextView operator_performed;
        public TextView status;
        public TextView transaction_id_recharge;
        public TextView transaction_mode;
        public TextView updated_date;

        public ViewHolder(View itemView) {
            super(itemView);

            updated_date =  itemView.findViewById(R.id.updated_date);
            status =  itemView.findViewById(R.id.status);
            transaction_id_recharge =  itemView.findViewById(R.id.transaction_id_recharge);
            amount_transacted_recharge =  itemView.findViewById(R.id.amount_transacted);
            mobile_number =  itemView.findViewById(R.id.mobile_number);
            operator_performed =  itemView.findViewById(R.id.operation_performed);
            transaction_mode =  itemView.findViewById(R.id.transactionMode);
        }
    }
}
