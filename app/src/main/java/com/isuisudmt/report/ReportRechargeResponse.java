package com.isuisudmt.report;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ReportRechargeResponse {
    @SerializedName("transactionReports")
    private ArrayList<RechargeResponseSetGet> rechargeResponseSetGet;

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("ReportRechargeResponse{rechargeResponseSetGet=");
        sb.append(this.rechargeResponseSetGet);
        sb.append('}');
        return sb.toString();
    }

    public ArrayList<RechargeResponseSetGet> getRechargeResponseSetGet() {
        return this.rechargeResponseSetGet;
    }

    public void setRechargeResponseSetGet(ArrayList<RechargeResponseSetGet> rechargeResponseSetGet2) {
        this.rechargeResponseSetGet = rechargeResponseSetGet2;
    }
}
