package com.isuisudmt.report;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.dmt.FinoTransactionReports;
import com.isuisudmt.dmt.ReportFragmentRequest;
import com.matm.matmsdk.aepsmodule.utils.AEPSAPIService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.isuisudmt.AppController.TAG;
import static com.isuisudmt.bbps.utils.Const.URL_GET_REPORT_BBPS;


public class BbpsFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    TextView noData;
    Spinner spinnerOperationPerformed, spinnerStatus;
    LinearLayout ll_main;
    private RecyclerView reportRecyclerView;
    Button btnGetReport;

    ArrayAdapter adapterOperationPerformed, adapterStatus;

    AdapterReportBBPS mAdapter;
    //  RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager layoutManager;
    String transactionType = "ISU_FT";
    //String transactionType = "RECHARGE";
    private AEPSAPIService apiService;
    // Session session;
    ReportFragmentRequest reportFragmentRequest;
    ArrayList<FinoTransactionReports> finoTransactionReports;

    SessionManager session;
    String tokenStr = "";
    ProgressBar progressV;
    Context context;
    String[] arr_operation_performed = {"All","BBPS_Mobile Postpaid", "BBPS_DTH", "BBPS_Electricity", "BBPS_Broadband Postpaid",
            "BBPS_Water", "BBPS_Landline Postpaid", "BBPS_Gas", "BBPS_Education Fees",
            "BBPS_Cable TV", "BBPS_LPG Gas", "BBPS_Fastag", "BBPS_Life Insurance",
            "BBPS_Loan Repayment", "BBPS_Health Insurance",};
    String[] arr_status = { "INITIATED", "PENDING", "SUCCESS", "FAILED"};

    String selectedStatus = "All", fromdate = "", todate = "", api_todate = "";
    SharedPreferences sp;
    public static final String ISU_PREF = "isuPref";
    public static final String USER_NAME = "userNameKey";
    JSONArray jArrayOpsPerformed, jArrayStatus, jArrayTransactionType;
    ArrayList<PojoReportBBPS> data = new ArrayList<>();

    public BbpsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_bbps, container, false);

        initView(rootView);

        return rootView;
    }

    private void initView(View rootView) {

        ((ReportDashboardActivity) getActivity()).getSupportActionBar().setTitle("BBPS Report");

        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        sp = getActivity().getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);

        progressV = rootView.findViewById(R.id.progressV);
        noData = rootView.findViewById(R.id.noData);
        reportRecyclerView = rootView.findViewById(R.id.reportRecyclerView);
        reportRecyclerView.setNestedScrollingEnabled(false);
        ll_main = rootView.findViewById(R.id.ll_main_bbps_report);
        spinnerOperationPerformed = rootView.findViewById(R.id.spinner_operation_performed);
        spinnerStatus = rootView.findViewById(R.id.spinner_status);
        btnGetReport = rootView.findViewById(R.id.btn_get_report);
        btnGetReport.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        reportRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        reportRecyclerView.setLayoutManager(layoutManager);

        jArrayOpsPerformed = new JSONArray();
        for (int i = 1; i < arr_operation_performed.length; i++) {
            jArrayOpsPerformed.put(arr_operation_performed[i]);
        }



       /* adapterOperationPerformed = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, arr_operation_performed);
        spinnerOperationPerformed.setAdapter(adapterOperationPerformed);

        spinnerOperationPerformed.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    jArrayOpsPerformed = new JSONArray();
                    for (int i = 1; i < arr_operation_performed.length; i++) {
                        jArrayOpsPerformed.put(arr_operation_performed[i]);
                    }
                } else {
                    jArrayOpsPerformed = new JSONArray();
                    jArrayOpsPerformed.put(arr_operation_performed[position]);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterStatus = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, arr_status);
        spinnerStatus.setAdapter(adapterStatus);

        spinnerStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    selectedStatus = "All";
                } else {
                    selectedStatus = "";
                    jArrayStatus = new JSONArray();
                    jArrayStatus.put(arr_status[position]);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/





  /*      btnGetReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                jArrayTransactionType = new JSONArray();
                jArrayTransactionType.put("BBPS");

                if (fromdate.equals(""))
                    Toast.makeText(context, "Please select From Date from calender", Toast.LENGTH_SHORT).show();
                else if (todate.equals(""))
                    Toast.makeText(context, "Please select To Date from calender", Toast.LENGTH_SHORT).show();
                else {
                    getReport();
                }
            }
        });*/
    }

    private void getReport() {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();
        try {

            obj.put("start_date", fromdate);
            obj.put("end_date", api_todate);
            obj.put("userName", sp.getString(USER_NAME, ""));
            obj.put("transaction_type", jArrayTransactionType);
            obj.put("operationPerformed", jArrayOpsPerformed);
            obj.put("status", selectedStatus);

          /*  if (selectedStatus.equals(""))
                obj.put("status", arr_status);
            else
                obj.put("status", arr_status);
*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(URL_GET_REPORT_BBPS)
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .addHeaders("Authorization", tokenStr)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.cancel();

                        try {
                            data.clear();
                            noData.setVisibility(View.GONE);

                            JSONObject obj = new JSONObject(response.toString());
                            String status = obj.getString("status");
                            String message = obj.getString("message");
                            String results = obj.getString("results");
                            String length = obj.getString("length");

                            if (length.equals("0")) {
                                noData.setVisibility(View.VISIBLE);
                                noData.setText(message);
                            }

                            if (status.equals("200")) {
                                JSONObject objCommonClass = new JSONObject(results);
                                String BQReport = objCommonClass.getString("BQReport");

                                JSONArray jsonArray = new JSONArray(BQReport);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    PojoReportBBPS pojoReportBBPS = new PojoReportBBPS();

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    pojoReportBBPS.Id = jsonObject.getString("Id");
                                    pojoReportBBPS.previousAmount = jsonObject.getString("previousAmount");
                                    pojoReportBBPS.amountTransacted = jsonObject.getString("amountTransacted");
                                    pojoReportBBPS.balanceAmount = jsonObject.getString("balanceAmount");
                                    pojoReportBBPS.apiTid = jsonObject.getString("apiTid");
                                    pojoReportBBPS.apiComment = jsonObject.getString("apiComment");
                                    pojoReportBBPS.operationPerformed = jsonObject.getString("operationPerformed");
                                    pojoReportBBPS.status = jsonObject.getString("status");
                                    pojoReportBBPS.transactionMode = jsonObject.getString("transactionMode");
                                    pojoReportBBPS.userName = jsonObject.getString("userName");
                                    pojoReportBBPS.masterName = jsonObject.getString("masterName");
                                    pojoReportBBPS.createdDate = jsonObject.getString("createdDate");
                                    pojoReportBBPS.updatedDate = jsonObject.getString("updatedDate");
                                    pojoReportBBPS.referenceNo = jsonObject.getString("referenceNo");
                                    pojoReportBBPS.mobileNumber = jsonObject.getString("mobileNumber");
                                    pojoReportBBPS.param_a = jsonObject.getString("param_a");
                                    pojoReportBBPS.param_b = jsonObject.getString("param_b");
                                    pojoReportBBPS.param_c = jsonObject.getString("param_c");

                                    data.add(pojoReportBBPS);
                                }

                                mAdapter = new AdapterReportBBPS(getActivity(), data);
                                reportRecyclerView.setAdapter(mAdapter);

                            } else {
                                Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.cancel();

                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(anError.getErrorBody().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Toast.makeText(getActivity(), obj.getString("statusDescription").toString(), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        } else if (id == R.id.filterBar) {
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                Toast.makeText(getActivity(), "SearchOnQueryTextSubmit: " + query, Toast.LENGTH_SHORT).show();


                if (!searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);

                if (finoTransactionReports != null && finoTransactionReports.size() > 0) {
                    // filter recycler view when text is changed
                    mAdapter.getFilter().filter(s);
                    mAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.empty_date), Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });

    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }

    private void callCalenderFunction() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(BbpsFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
        dpd.setMaxDate(Calendar.getInstance());
        dpd.setAutoHighlight(true);
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        fromdate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
        todate = yearEnd + "-" + (monthOfYearEnd + 1) + "-" + (dayOfMonthEnd);
        api_todate = Util.getNextDate(todate);

        String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year + " To " + (dayOfMonthEnd) + "/" + (monthOfYearEnd + 1) + "/" + yearEnd;
        ((ReportDashboardActivity) getActivity()).getSupportActionBar().setSubtitle(date);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date from_date = null, to_date = null;

        try {
            from_date = formatter.parse(fromdate);
            to_date = formatter.parse(todate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (from_date.before(to_date) || from_date.equals(to_date)) {

        } else {
            Toast.makeText(getActivity(), "From date should be less than To date.", Toast.LENGTH_LONG).show();
        }

        jArrayTransactionType = new JSONArray();
        jArrayTransactionType.put("BBPS");

        if (fromdate.equals(""))
            Toast.makeText(context, "Please select From Date from calender", Toast.LENGTH_SHORT).show();
        else if (todate.equals(""))
            Toast.makeText(context, "Please select To Date from calender", Toast.LENGTH_SHORT).show();
        else {
            getReport();
        }
    }

}
