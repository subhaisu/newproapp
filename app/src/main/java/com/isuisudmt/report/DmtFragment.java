package com.isuisudmt.report;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.dmt.CustomReportAdapter;
import com.isuisudmt.dmt.FinoTransactionReports;
import com.isuisudmt.dmt.ReportFragmentRequest;
import com.isuisudmt.dmt.ReportFragmentResponse;
import com.matm.matmsdk.aepsmodule.utils.AEPSAPIService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;


public class DmtFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    TextView noData;

    private RecyclerView reportRecyclerView;
    CustomReportAdapter mAdapter;
    //  RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager layoutManager;
    String transactionType = "ISU_FT";
    //String transactionType = "RECHARGE";
    private AEPSAPIService apiService;
    // Session session;
    AutoCompleteTextView transaction_spinner;
    ReportFragmentRequest reportFragmentRequest;
    ArrayList<FinoTransactionReports> finoTransactionReports;

    SessionManager session;
    String tokenStr="";
    ProgressBar progressV;
    Context context;

    public DmtFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_dmt,container,false);
        initView(rootView);


        return rootView;
    }

    private void initView(View rootView) {

        ((ReportDashboardActivity)getActivity()).getSupportActionBar().setTitle("DMT Report");

        session = new SessionManager(getActivity());
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        progressV = rootView.findViewById(R.id.progressV);
        noData = rootView.findViewById(R.id.noData);
        transaction_spinner = rootView.findViewById(R.id.transaction_spinner);
        reportRecyclerView = rootView.findViewById ( R.id.reportRecyclerView );
        reportRecyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        reportRecyclerView.setLayoutManager(layoutManager);

        transaction_spinner.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // mAdapter.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    if (mAdapter != null) {
                        mAdapter.getFilter().filter(editable.toString());
                        if (mAdapter.getItemCount() == 0) {
                            noData.setVisibility(View.VISIBLE);
                        } else {
                            noData.setVisibility(View.GONE);
                        }
                    }
                }catch (Exception e){

                }
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_calender) {
            callCalenderFunction();
            return true;
        }
        else if(id == R.id.filterBar){
            callFilterFunction(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void callFilterFunction(MenuItem item) {

        MenuItem myActionMenuItem = item;
        SearchView searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                Toast.makeText(getActivity(), "SearchOnQueryTextSubmit: " + query, Toast.LENGTH_SHORT).show();


                if( ! searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);

                if (finoTransactionReports!=null && finoTransactionReports.size() > 0) {
                    // filter recycler view when text is changed
                    mAdapter.getFilter().filter(s);
                    mAdapter.notifyDataSetChanged();
                }else{
                    Toast.makeText(getActivity(),getResources().getString(R.string.empty_date),Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        getActivity().getMenuInflater().inflate(R.menu.report_menu, menu);

    }

    private void callCalenderFunction() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(DmtFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
        dpd.setMaxDate ( Calendar.getInstance () );
        dpd.setAutoHighlight ( true );
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String fromdate = year + "-" + (monthOfYear+1) + "-" + dayOfMonth;
        String todate = yearEnd + "-" + (monthOfYearEnd+1)+ "-" + (dayOfMonthEnd);
        String api_todate = yearEnd + "-" + (monthOfYearEnd+1)+ "-" + (dayOfMonthEnd+1);

        String date = dayOfMonth+"/"+(monthOfYear+1)+"/"+year+" To "+(dayOfMonthEnd)+"/"+(monthOfYearEnd+1)+"/"+yearEnd;
        ((ReportDashboardActivity)getActivity()).getSupportActionBar().setSubtitle(date);




        Date from_date=null;
        Date to_date=null;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            from_date = formatter.parse(fromdate);
            to_date = formatter.parse(todate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(from_date.before(to_date) || from_date.equals(to_date)){
            reportCall(fromdate,api_todate,tokenStr);
            showLoader();
        }else{
            Toast.makeText(getActivity(), "From date should be less than To date.", Toast.LENGTH_LONG).show();
        }
    }

    private void reportCall(String fromdate,String todate,String token) {
        if (this.apiService == null) {
            this.apiService = new AEPSAPIService();
        }
        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v67")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            // encriptedReportCall(encodedUrl);
                            encryptedReport(fromdate,todate,token,encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    public void encryptedReport(String fromDate, String toDate, String token, String encodedUrl){
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("transactionType",transactionType);
            jsonObject.put("fromDate",fromDate);
            jsonObject.put("toDate",toDate);

            AndroidNetworking.post(encodedUrl)
                    .setPriority(Priority.HIGH)
                    .addHeaders("Authorization",token)
                    .addJSONObjectBody(jsonObject)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                // Toast.makeText(, "", Toast.LENGTH_SHORT).show();
                                JSONArray jsonArray = obj.getJSONArray("BQReport");

                                ReportFragmentResponse reportResponse =new ReportFragmentResponse() ;
                                ArrayList<FinoTransactionReports> reportModels = new ArrayList<>();
                                for(int i =0 ; i<jsonArray.length();i++){
                                    FinoTransactionReports reportModel = new FinoTransactionReports();
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    reportModel.setId(jsonObject1.getString("Id"));
                                    reportModel.setPreviousAmount(Double.valueOf(jsonObject1.getString("previousAmount")));
                                    reportModel.setBalanceAmount(Double.valueOf(jsonObject1.getString("balanceAmount")));
                                    reportModel.setAmountTransacted(jsonObject1.getInt("amountTransacted"));
                                    reportModel.setBeniMobile(jsonObject1.getString("beniMobile"));
                                    reportModel.setApiTid(jsonObject1.getString("apiTid"));
                                    reportModel.setApiComment(jsonObject1.getString("apiComment"));
                                    reportModel.setBankName(jsonObject1.getString("bankName"));
                                    reportModel.setTransactionMode(jsonObject1.getString("transactionMode"));
                                    reportModel.setStatus(jsonObject1.getString("status"));
                                    reportModel.setUserName(jsonObject1.getString("userName"));
                                    reportModel.setDistributerName(jsonObject1.getString("distributerName"));
                                    reportModel.setMasterName(jsonObject1.getString("masterName"));
                                    reportModel.setAPI(jsonObject1.getString("API"));
                                    reportModel.setBenificiaryName(jsonObject1.getString("benificiaryName"));
                                    reportModel.setCreatedDate(jsonObject1.getLong("createdDate"));
                                    reportModel.setUpdatedDate(jsonObject1.getLong("updatedDate"));
                                    reportModel.setApiComment(jsonObject1.getString("apiComment"));
                                    reportModel.setRouteID(jsonObject1.getString("routeID"));
                                    reportModel.setToAccount(jsonObject1.getString("toAccount"));

                                    reportModels.add(reportModel);
                                }

                                reportResponse.setFinoTransactionReports(reportModels);

                                finoTransactionReports = reportResponse.getFinoTransactionReports();
                                // for( int i=0;i<finoTransactionReports.size();i++) {

                                transaction_spinner.setVisibility(View.GONE);
                                Collections.reverse(finoTransactionReports);
                                mAdapter = new CustomReportAdapter(getActivity(), finoTransactionReports);
                                reportRecyclerView.setAdapter(mAdapter);

                                hideLoader();
                                //  }

                                if(finoTransactionReports.size()<=0){
                                    noData.setVisibility(View.VISIBLE);
                                }else{
                                    noData.setVisibility(View.GONE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                transaction_spinner.setVisibility(View.GONE);
                                hideLoader();
                                if(finoTransactionReports.size()<=0){
                                    noData.setVisibility(View.VISIBLE);
                                }else{
                                    noData.setVisibility(View.GONE);
                                }
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            anError.getErrorDetail();
                            transaction_spinner.setVisibility(View.GONE);
                            if(finoTransactionReports.size()<=0){
                                noData.setVisibility(View.VISIBLE);
                            }else{
                                noData.setVisibility(View.GONE);
                            }
                        }
                    });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void showLoader() {
        progressV.setVisibility(View.VISIBLE);
    }

    private void hideLoader() {
        progressV.setVisibility(View.GONE);


    }
}
