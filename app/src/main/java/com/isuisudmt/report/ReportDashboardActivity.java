package com.isuisudmt.report;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import com.google.android.material.tabs.TabLayout;
import com.isuisudmt.R;
import com.isuisudmt.bbps.MobilePrepaidFragment;


public class ReportDashboardActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private LinearLayout fragment_container;
    private TabLayout tablayout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_dashboard);

        setUpToolBar();
        initView();
        callTablistener();
    }


    private void setUpToolBar() {
        // Inflate the layout for this fragment
        mToolbar = findViewById ( R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setSubtitle("");

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                //What to do on back clicked
            }
        });
    }
    private void initView() {
        tablayout = findViewById(R.id.tablayout);
        fragment_container = findViewById(R.id.fragment_container);


    }

    private void callTablistener() {
        replaceFragment(new AepsFragment());
        tablayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    replaceFragment(new AepsFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("AePS Report");
                } else if (tab.getPosition() == 1) {
                    replaceFragment(new MatmFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("mATM Report");
                }
                else if(tab.getPosition() == 2) {
                    replaceFragment(new DmtFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("DMT Report");
                }
                else if(tab.getPosition() == 3){
                    replaceFragment(new BbpsFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("BBPS");
                }
                else if(tab.getPosition() == 4){
                    replaceFragment(new MobilePrepaidFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("Mobile Prepaid");
                }
                else if(tab.getPosition() == 5){
                    replaceFragment(new CashOutFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle("Cashout");
                }
                 else if (tab.getPosition() == 6) {
                    replaceFragment(new WalletFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle((CharSequence) "Wallet");
                } else if (tab.getPosition() == 7) {
                    replaceFragment(new InsuranceFragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle((CharSequence) "Insurance Report");
                } else {
                    replaceFragment(new Recharge_Fragment());
                    getSupportActionBar().setSubtitle("");
                    getSupportActionBar().setTitle((CharSequence) "Recharge Report");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
    private void replaceFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fragment_container,fragment);
        transaction.commit();
    }





}
