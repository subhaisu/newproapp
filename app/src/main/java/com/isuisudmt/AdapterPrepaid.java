package com.isuisudmt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.paxsz.easylink.model.AppSelectResponse;

import java.util.ArrayList;
import java.util.List;

public class AdapterPrepaid extends RecyclerView.Adapter<AdapterPrepaid.ViewHolder> implements Filterable {

    private Context context;
    /* access modifiers changed from: private */
    public List<PrepaidBBPS> pojoReportBBPS;
    public List<PrepaidBBPS> pojoReportBBPSFilter;
    /* access modifiers changed from: private */

    public AdapterPrepaid(Context context2, ArrayList<PrepaidBBPS> pojoReportBBPS) {
        this.context = context2;
        this.pojoReportBBPS = pojoReportBBPS;
        this. pojoReportBBPSFilter = pojoReportBBPS;
       // Collections.reverse(pojoReportBBPS);

    }

    public AdapterPrepaid.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AdapterPrepaid.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_report_prepaid, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterPrepaid.ViewHolder holder, int position) {
        PrepaidBBPS current = pojoReportBBPS.get(position);

        holder.amount_transacted_recharge.setText(String.valueOf(new StringBuilder().append("₹ ").append(current.getAmountTransacted()).toString()));
        holder.updated_date.setText(new StringBuilder().append("Date: ").append(Util.getDateFromTime(current.getUpdatedDate())).toString());
        holder.status.setText(current.getStatus());
        holder.transaction_id_recharge.setText(new StringBuilder().append("Transaction Id: ").append(String.valueOf(current.getId())).toString());
        if(current.apiTid.equalsIgnoreCase("null") ){
                       holder.trackingId.setText(new StringBuilder().append("User Tracking Id: ").append("NA"));
        }
        else {
            holder.trackingId.setText(new StringBuilder().append("User Tracking Id: ").append(String.valueOf(current.getApiTid())).toString());

        }

        holder.mobile_number.setText(new StringBuilder().append("Mobile No:").append(current.getMobileNumber()).toString());
        holder.operator_performed.setText(current.operationPerformed);
        //holder.transaction_mode.setText(current.transactionMode);


        if (current.getStatus().equals(AppSelectResponse.SUCCESS)) {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.color_report_green));
        } else if(current.getStatus().equalsIgnoreCase("INITIATED")) {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.orange));
        }
        else {
            holder.status.setTextColor(ContextCompat.getColor(this.context, R.color.red));
        }

        holder.operator.setText(new StringBuilder().append("Operator Name: ").append(current.getOperatorDescription()));
 /*
        if(current.getServiceProvider().equalsIgnoreCase("AR")){
           holder.operator.setText(new StringBuilder().append("Operator Name: ").append("AIRTEL"));
        }
        else if(current.getServiceProvider().equalsIgnoreCase("BS")) {
            holder.operator.setText(new StringBuilder().append("Operator Name: ").append("BSNL"));
        }
        else if(current.getServiceProvider().equalsIgnoreCase("ID")) {
            holder.operator.setText(new StringBuilder().append("Operator Name: ").append("IDEA"));
        }
        else if(current.getServiceProvider().equalsIgnoreCase("VF")) {
            holder.operator.setText(new StringBuilder().append("Operator Name: ").append("VODAFONE"));
        }
        else if(current.getServiceProvider().equalsIgnoreCase("RJ")) {
            holder.operator.setText(new StringBuilder().append("Operator Name:  ").append("RELIANCE JIO"));
        }
        else if(current.getServiceProvider().equalsIgnoreCase("AI")) {
            holder.operator.setText(new StringBuilder().append("Operator Name: ").append("AIRCEL"));
        }
        else {
            Toast.makeText(context, "no code found", Toast.LENGTH_LONG).show();
        }
 */
    }

    public int getItemCount() {
        return this.pojoReportBBPSFilter.size();
    }

    public Filter getFilter() {
        return new Filter() {
            /* access modifiers changed from: protected */
            public FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    AdapterPrepaid adapterReportBBPS = AdapterPrepaid.this;
                    adapterReportBBPS.pojoReportBBPSFilter = adapterReportBBPS.pojoReportBBPS;
                } else {
                    List<PrepaidBBPS> filteredList = new ArrayList<>();
                    for (PrepaidBBPS row : AdapterPrepaid.this.pojoReportBBPS) {
                        if (row.getId().contains(charString.toLowerCase()) || row.getId().contains(charSequence)) {
                            filteredList.add(row);
                        }
                        if (String.valueOf(row.getMobileNumber()).toLowerCase().contains(charString.toLowerCase()) || String.valueOf(row.getMobileNumber()).contains(charSequence)) {
                            filteredList.add(row);
                        }
                    }
                    AdapterPrepaid.this.pojoReportBBPSFilter = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = AdapterPrepaid.this.pojoReportBBPSFilter;
                return filterResults;
            }

            /* access modifiers changed from: protected */
            public void publishResults(CharSequence charSequence, FilterResults filterResults) {
                pojoReportBBPSFilter = (ArrayList) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView amount_transacted_recharge;
        public TextView trackingId;
        public TextView mobile_number;
        public TextView operator_performed;
        public TextView status;
        public TextView transaction_id_recharge;
        public TextView transaction_mode;
        public TextView updated_date;
        public TextView operator;

        public ViewHolder(View itemView) {
            super(itemView);

            updated_date =  itemView.findViewById(R.id.updated_date);
            status =  itemView.findViewById(R.id.status);
            transaction_id_recharge =  itemView.findViewById(R.id.transaction_id_recharge);
            amount_transacted_recharge =  itemView.findViewById(R.id.amount_transacted);
            mobile_number =  itemView.findViewById(R.id.mobile_number);
            operator_performed =  itemView.findViewById(R.id.operation_performed);
            transaction_mode =  itemView.findViewById(R.id.transactionMode);
            operator = itemView.findViewById(R.id.operator);
            trackingId = itemView.findViewById(R.id.tracking_id);
        }
    }


}
