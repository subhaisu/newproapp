package com.isuisudmt;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.isuisudmt.bbps.ActivityfLandlinePostpaid;
import com.isuisudmt.bbps.ActivitygGas;
import com.isuisudmt.bbps.ActivityhEducationFees;
import com.isuisudmt.bbps.ActivityiCableTV;
import com.isuisudmt.bbps.ActivityjLpgGas;
import com.isuisudmt.bbps.ActivitykFasttag;
import com.isuisudmt.bbps.ActivitylLifeInsurance;
import com.isuisudmt.bbps.ActivitymLoanRepayment;
import com.isuisudmt.bbps.ActivitynHealthInsurance;
import com.isuisudmt.bbps.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.isuisudmt.bbps.utils.Const.URL_VIEW_REQUIRED_INFO;
import static com.isuisudmt.utils.Constants.BBPS_PREF;
import static com.isuisudmt.utils.Constants.CABLE_TV;
import static com.isuisudmt.utils.Constants.EDUCATION_FEES;
import static com.isuisudmt.utils.Constants.FASTAG;
import static com.isuisudmt.utils.Constants.GAS;
import static com.isuisudmt.utils.Constants.HEALTH_INSURANCE;
import static com.isuisudmt.utils.Constants.LANDLINE_POSTPAID;
import static com.isuisudmt.utils.Constants.LIFE_INSURANCE;
import static com.isuisudmt.utils.Constants.LOAN_REPAYMENT;
import static com.isuisudmt.utils.Constants.LPG_GAS;
import static com.isuisudmt.utils.Constants.SF_AGENT_ID;
import static com.isuisudmt.utils.Constants.SF_KEYWORD;
import static com.isuisudmt.utils.Constants.SF_LAT_LONG;
import static com.isuisudmt.utils.Constants.SF_MOBILE_NUMBER;
import static com.isuisudmt.utils.Constants.SF_PINCODE;
import static com.isuisudmt.utils.Constants.SF_TERMINAL_ID;

public class PayBillFragment3 extends Fragment {

    View view;
    SessionManager session;
    SharedPreferences sp, spBbps;
    LinearLayout loan_repayment,health_insurance;
    String userName = "",tokenStr = "";
    public static final String ISU_PREF = "isuPref" ;
    public static final String USER_NAME = "userNameKey";

    public PayBillFragment3() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_pay_bill_two, container, false);

        session = new SessionManager(getActivity());
        sp = getActivity().getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);
        spBbps = getActivity().getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE);

/*        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        userName = sp.getString(USER_NAME, "");*/

        HashMap<String, String> _user = session.getUserDetails();
        HashMap<String, String> user = session.getUserSession();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
       // userName = user.get(session.userName);
        userName = Constants.USER_NAME;

        loan_repayment = view.findViewById(R.id.loan_repayment);
        health_insurance = view.findViewById(R.id.health_insurance);
        LinearLayout landline_postpaid = view.findViewById(R.id.landline_postpaid);


        loan_repayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isApiCalledInSession(LOAN_REPAYMENT);
            }
        });
        health_insurance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isApiCalledInSession(HEALTH_INSURANCE);
            }
        });
        landline_postpaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isApiCalledInSession(LANDLINE_POSTPAID);
            }
        });

        return view;
    }

    private void isUpdated(String CategoryID) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        String url = "https://indusindtest.iserveu.online/BBPS/bbps/isUpdated";

        AndroidNetworking.post(url)
                .setPriority(Priority.HIGH)
                .addHeaders("Authorization", tokenStr)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            dialog.dismiss();
                            Const.isUpdatedResponse = response.toString();

                            int status = response.getInt("status");
                            String statusDescription = response.getString("statusDescription");

                            if (status == 0) {
                                viewRequiredInfo(CategoryID);
                            } else {
                                Toast.makeText(getActivity(), ""+ statusDescription, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        try {
                            dialog.dismiss();
                            JSONObject errorObject = new JSONObject(anError.getErrorBody());
                            String message = errorObject.getString("message");
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void viewRequiredInfo(String CategoryID) {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();
        try {
            obj.put("username", userName);

            AndroidNetworking.post(URL_VIEW_REQUIRED_INFO   )
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            dialog.dismiss();

                            Const.viewRequiredInfoResponse = response.toString();

                            intentToActivity(response.toString(), CategoryID);

                            /*try {
                                JSONObject obj = new JSONObject(response.toString());
                                String status = obj.getString("status");
                                String pincode = obj.getString("pincode");
                                String mobilenumber = obj.getString("mobilenumber");
                                String latlong = obj.getString("latlong");
                                String terminalid = obj.getString("terminalid");
                                String agentid = obj.getString("agentid");
                                String city = obj.getString("city");
                                String state = obj.getString("state");
                                String keyword = obj.getString("keyword");
                                String accountcode = obj.getString("accountcode");

                                SharedPreferences.Editor editor = spBbps.edit();
                                editor.putString(SF_PINCODE, pincode);
                                editor.putString(SF_MOBILE_NUMBER, mobilenumber);
                                editor.putString(SF_LAT_LONG, latlong);
                                editor.putString(SF_TERMINAL_ID, terminalid);
                                editor.putString(SF_AGENT_ID, agentid);
                                editor.putString(SF_KEYWORD, keyword);
                                editor.apply();

                                if (CategoryID.equals(LOAN_REPAYMENT)){
                                    Intent intent = new Intent(getActivity(), ActivitymLoanRepayment.class);
                                    getActivity().startActivity(intent);
                                } else if (CategoryID.equals(HEALTH_INSURANCE)){
                                    Intent intent = new Intent(getActivity(), ActivitynHealthInsurance.class);
                                    getActivity().startActivity(intent);
                                }


                            } catch (JSONException e) {
                                dialog.dismiss();
                                e.printStackTrace();
                            }*/
                        }

                        @Override
                        public void onError(ANError anError) {
                            dialog.dismiss();
                            JSONObject obj = null;
                            try {
                                obj = new JSONObject(anError.getErrorBody().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void isApiCalledInSession(String CATEGORY) {
        if (Const.isUpdatedResponse.equals(""))
            isUpdated(CATEGORY);
        else {
            //Perform here
            try {
                JSONObject objIsUpdated = new JSONObject(Const.isUpdatedResponse);
                int status = objIsUpdated.getInt("status");
                String statusDescription = objIsUpdated.getString("statusDescription");

                if (status == 0) {
                    if (Const.viewRequiredInfoResponse.equals(""))
                        viewRequiredInfo(CATEGORY);
                    else
                        intentToActivity(Const.viewRequiredInfoResponse, CATEGORY);

                } else {
                    Toast.makeText(getActivity(), "" + statusDescription, Toast.LENGTH_SHORT).show();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private void intentToActivity(String response, String CATEGORY) {
        try {
            JSONObject objViewRqrdDetails = new JSONObject(response);
            String _status = objViewRqrdDetails.getString("status");
            String pincode = objViewRqrdDetails.getString("pincode");
            String mobilenumber = objViewRqrdDetails.getString("mobilenumber");
            String latlong = objViewRqrdDetails.getString("latlong");
            String terminalid = objViewRqrdDetails.getString("terminalid");
            String agentid = objViewRqrdDetails.getString("agentid");
            String city = objViewRqrdDetails.getString("city");
            String state = objViewRqrdDetails.getString("state");
            String keyword = objViewRqrdDetails.getString("keyword");
            String accountcode = objViewRqrdDetails.getString("accountcode");

            SharedPreferences.Editor editor = spBbps.edit();
            editor.putString(SF_PINCODE, pincode);
            editor.putString(SF_MOBILE_NUMBER, mobilenumber);
            editor.putString(SF_LAT_LONG, latlong);
            editor.putString(SF_TERMINAL_ID, terminalid);
            editor.putString(SF_AGENT_ID, agentid);
            editor.putString(SF_KEYWORD, keyword);
            editor.apply();


            if (CATEGORY.equals(LOAN_REPAYMENT)){
                Intent intent = new Intent(getActivity(), ActivitymLoanRepayment.class);
                getActivity().startActivity(intent);
            } else if (CATEGORY.equals(HEALTH_INSURANCE)){
                Intent intent = new Intent(getActivity(), ActivitynHealthInsurance.class);
                getActivity().startActivity(intent);
            }
            else if (CATEGORY.equals(LANDLINE_POSTPAID)) {
                Intent intent = new Intent(getActivity(), ActivityfLandlinePostpaid.class);
                getActivity().startActivity(intent);
            }


        }catch (Exception e){
            e.printStackTrace();
        }
    }

}