package com.isuisudmt.retrofitService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class DMTRetrofitInstance {

    private static Retrofit retrofit;
    public static  Retrofit getretrofitInstance(String baseurl){

        if(retrofit == null){

            retrofit = new Retrofit.Builder()
                    .baseUrl(baseurl)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();
        }

        return retrofit;
    }
}

