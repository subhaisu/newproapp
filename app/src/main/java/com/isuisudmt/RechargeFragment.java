package com.isuisudmt;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.isuisudmt.recharge.PrepaidActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class RechargeFragment extends Fragment {

    View view;

    public RechargeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_recharge, container, false);
        LinearLayout prepaid = view.findViewById(R.id.prepaid);
        LinearLayout dth = view.findViewById(R.id.dth);
        LinearLayout datacard = view.findViewById(R.id.datacard);
        LinearLayout postpaid = view.findViewById(R.id.postpaid);
        LinearLayout special = view.findViewById(R.id.special);
        LinearLayout broadband = view.findViewById(R.id.broadband);

        dth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), DTHActivity.class));
                Toast.makeText(getContext(), "Coming Soon !", Toast.LENGTH_SHORT).show();
            }
        });

        prepaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), RechargeActivity.class).putExtra("index",0));
                startActivity(new Intent(getActivity(), PrepaidActivity.class));
            }
        });

        datacard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), DataCardActivity.class));
                Toast.makeText(getContext(), "Coming Soon !", Toast.LENGTH_SHORT).show();

            }
        });

        postpaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), RechargeActivity.class).putExtra("index",1));
                Toast.makeText(getContext(), "Coming Soon !", Toast.LENGTH_SHORT).show();

            }
        });

        special.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(getActivity(), SpecialActivity.class));
                Toast.makeText(getContext(), "Coming Soon !", Toast.LENGTH_SHORT).show();

            }
        });

        broadband.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        return view;
    }

}
