package com.isuisudmt.matm;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface RefreshApi {
//sendEncryptedRequestTransactionEnquiryMATM
    @POST()
    Call<RefreshModel> insertUser(@Header("Authorization") String token, @Body RefreshRequest body, @Url String url);
}
