package com.isuisudmt.matm;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class UserInfoModel {
    @SerializedName("userBalance")
    @Expose
    private String userBalance;

    @SerializedName("userFeature")
    @Expose
    private ArrayList<userFeature> featureIdList = new ArrayList<>();
    //getter and setters


    public ArrayList<userFeature> getFeatureIdList() {
        return featureIdList;
    }

    public void setFeatureIdList(ArrayList<userFeature> featureIdList) {
        this.featureIdList = featureIdList;
    }

    public class userFeature{
        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("featureName")
        @Expose
        private String featureName;

        @SerializedName("active")
        @Expose
        private boolean active;
        //getters and setters

        public userFeature(){

        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFeatureName() {
            return featureName;
        }

        public void setFeatureName(String featureName) {
            this.featureName = featureName;
        }

        public boolean getActive() {
            return active;
        }

        public void setActive(boolean active) {
            this.active = active;
        }
    }

        public UserInfoModel() {
        }


        public String getUserBalance() {
        return userBalance;
    }

    public void setUserBalance(String userBalance) {
        this.userBalance = userBalance;
    }

    @SerializedName("userBrand")
    @Expose
    private String userBrand;

    public String getUserBrand() {
        return userBrand;
    }

    public void setUserBrand(String userBrand) {
        this.userBrand = userBrand;
    }

    @SerializedName("userProfile")
    @Expose
    private UserProfileModel userProfile;

    public UserProfileModel getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfileModel userProfile) {
        this.userProfile = userProfile;
    }

    public class UserProfileModel {


        @SerializedName("mobileNumber")
        @Expose
        private String mobileNumber;

        @SerializedName("shopName")
        @Expose
        private String shopName;
        @SerializedName("firstName")
        @Expose
        private String firstName;

        @SerializedName("lastName")
        @Expose
        private String lastName;



        public UserProfileModel() {
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getMobileNumber() {
            return mobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
        }

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }
    }

    public class UserBrandModel {


        @SerializedName("brand")
        @Expose
        private String brand;


        public UserBrandModel() {
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }
    }
}
