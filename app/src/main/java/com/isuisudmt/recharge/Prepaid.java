package com.isuisudmt.recharge;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.isuisudmt.CommonUtil;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.dmt.MyErrorMessage;
import com.isuisudmt.dmt.TransferFragment;
import com.isuisudmt.utils.APIService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.isuisudmt.BuildConfig.GET_AEPS_RECHARGE_URL;


/**
 * A simple {@link Fragment} subclass.
 */
public class Prepaid extends Fragment {


    String[] operator = {"AIRCEL", "AIRTEL", "BSNL", "IDEA", "JIO", "LOOP", "MTNL DELHI", "MTNL MUMBAI", "MTS",
            "RELIANCE CDMA", "RELIANCE GSM", "TATA DOCOMO", "TELENOR", "VIDEOCON", "VIRGIN CDMA", "VIRGIN GSM", "VODAFONE",
            "TataIndicom", "T24(Flexi)"};
    View view;

    TextInputEditText _mobile, _operator, _circel, _amount;
    Button submit;

    private String mobilenoString, amountString, operatorString, rechargetypeString, stdCodeString, pincodeString, accountNoString, latitudeString, circleCodeString, longitudeString;

    TextInputLayout layout_mobile, layout_operator, Layout_circel, layout_amount;

    private APIService apiService;
    SessionManager session;

    String _token = "", _admin = "";

    ProgressDialog pd;


    public Prepaid() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_prepaid, container, false);

        _mobile = view.findViewById(R.id.mobile);
        _circel = view.findViewById(R.id.circel);
        _amount = view.findViewById(R.id.amount);
        _operator = view.findViewById(R.id.operator);
        submit = view.findViewById(R.id.submit);

        Layout_circel = view.findViewById(R.id.Layout_circel);
        layout_mobile = view.findViewById(R.id.layout_mobile);
        layout_operator = view.findViewById(R.id.layout_operator);
        layout_amount = view.findViewById(R.id.layout_amount);

        Layout_circel.setVisibility(View.GONE);
        rechargetypeString = "Rr";
        stdCodeString = "";
        pincodeString = "";
        accountNoString = "";
        latitudeString = "";
        circleCodeString = "";
        longitudeString = "";


        session = new SessionManager(getActivity());

        HashMap<String, String> user = session.getUserDetails();
        _token = user.get(SessionManager.KEY_TOKEN);
        _admin = user.get(SessionManager.KEY_ADMIN);

        _mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 10) {
                    CommonUtil.hideKeyboard(getContext(), _mobile);
                    mobilenoString = _mobile.getText().toString();
                } else {
                    mobilenoString = null;
                }
            }
        });

        _amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                try {
                    if (editable.length() == 0) {
                        amountString = null;
                    } else {
                        amountString = _amount.getText().toString();
                        if ((operatorString.equals("AIRTEL") || operatorString.equals("VODAFONE") || operatorString.equals("IDEA")) && (amountString.equals("10") || amountString.equals("20") || amountString.equals("30") || amountString.equals("50"))) {
                            submit.setEnabled(false);
                            submit.setBackgroundColor(getActivity().getResources().getColor(R.color.colorGrey));
                        } else {
                            submit.setEnabled(true);
                            submit.setBackground(getActivity().getResources().getDrawable(R.drawable.bottomnavigation_bg));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (_mobile.getText().toString().isEmpty()) {
                    layout_mobile.setError("Enter 10 Digit MobileNo ");
                } else if (_operator.getText().toString().isEmpty()) {
                    layout_operator.setError("Select Operator");
                } else if (_amount.getText().toString().isEmpty()) {
                    layout_amount.setError("Enter amount");
                } else if ((operatorString.equals("AIRTEL") || operatorString.equals("VODAFONE") || operatorString.equals("IDEA")) && (amountString.equals("10") || amountString.equals("20") || amountString.equals("30") || amountString.equals("50"))) {
                    ErrorDialog("This denomination is blocked by operator. Kindly try with a valid amount.");
                } else {
                    layout_mobile.setError(null);
                    layout_operator.setError(null);
                    layout_amount.setError(null);

                    rechargePrepaid();
                }
            }
        });

        _operator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                View _view = getLayoutInflater().inflate(R.layout.custom_postpaid_operator, null);
                BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
                dialog.setContentView(_view);

                ListView listView = _view.findViewById(R.id.listing);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, operator);
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String itemValue = (String) listView.getItemAtPosition(position);
                        operatorString = itemValue;
                        _operator.setText(itemValue);
                        Layout_circel.setVisibility(View.VISIBLE);
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

        return view;

    }

    private void rechargePrepaid() {
        showLoader();
        final PrepaidRequest prepaidRequest = new PrepaidRequest(stdCodeString, pincodeString, amountString, mobilenoString, accountNoString, latitudeString, circleCodeString, operatorString, rechargetypeString, longitudeString);

        if (this.apiService == null) {
            this.apiService = new APIService();
        }
        AndroidNetworking.get(GET_AEPS_RECHARGE_URL)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----" + key);
                            byte[] data = Base64.decode(key, Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");
                            proceedRecharge(encodedUrl);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    public void proceedRecharge(String url) {

        JSONObject obj = new JSONObject();
        try {
            obj.put("stdCode", stdCodeString);
            obj.put("amount", amountString);
            obj.put("mobileNumber", mobilenoString);
            obj.put("accountNo", accountNoString);
            obj.put("circleCode", circleCodeString);
            obj.put("operatorCode", operatorString);
            obj.put("rechargeType", rechargetypeString);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", _token)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                System.out.println(obj.toString());
                                hideLoader();
                                if (obj.getString("status").equalsIgnoreCase("0")) {
                                    SuccessDialog(obj.getString("statusDesc"));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                hideLoader();
                                ErrorDialog("RECHARGE FAILED! undefined");
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            hideLoader();
                            Gson gson = new Gson();
                            MyErrorMessage message = gson.fromJson(anError.getErrorBody(), MyErrorMessage.class);
                            //transaction failed
                            ErrorDialog("RECHARGE FAILED! " + message.getMessage());
                            //  anError.getResponse();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void ErrorDialog(String msg) {

        clear();

        Intent intent = new Intent(getActivity(), ReacargeStatusActivity.class);
        intent.putExtra("title", "Prepaid Mobile");
        intent.putExtra("status", "false");
        intent.putExtra("message", msg);
        startActivity(intent);
        /*AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(msg);
        builder1.setTitle("Info!");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        clear();
                        dialog.cancel();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();*/
    }

    private void SuccessDialog(String msg) {

        clear();

        Intent intent = new Intent(getActivity(), ReacargeStatusActivity.class);
        intent.putExtra("title", "Prepaid Mobile");
        intent.putExtra("status", "true");
        intent.putExtra("message", msg);
        startActivity(intent);

     /*   AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(msg);
        builder1.setTitle("Success!");
        builder1.setCancelable(false);
        builder1.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                clear();
                dialog.cancel();
            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.show();*/
    }

    public void showLoader() {
        pd = new ProgressDialog(getActivity());
        pd.setMessage("loading");
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    public void hideLoader() {
        pd.hide();
    }

    private void clear() {

        rechargetypeString = "Rr";
        stdCodeString = "";
        pincodeString = "";
        accountNoString = "";
        latitudeString = "";
        circleCodeString = "";
        longitudeString = "";

        _mobile.setText("");
        _operator.setText("");
        _circel.setText("");
        _amount.setText("");

        layout_mobile.setError(null);
        layout_operator.setError(null);
        layout_amount.setError(null);
    }
}
