package com.isuisudmt.bbps;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isuisudmt.MainActivity;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.bbps.utils.BillerListPojo;
import com.isuisudmt.bbps.utils.Const;
import com.isuisudmt.bbps.utils.GetBillerParameters;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.isuisudmt.bbps.utils.Const.BBPS_PREF;
import static com.isuisudmt.bbps.utils.Const.DEFAULT_BILLER_COLLECTION;
import static com.isuisudmt.bbps.utils.Const.LPG_GAS;
import static com.isuisudmt.bbps.utils.Const.PAY_CHANNEL;
import static com.isuisudmt.bbps.utils.Const.PAY_MODE;
import static com.isuisudmt.bbps.utils.Const.PREFIX_DEFAULT_BILLER_DOCUMENT;
import static com.isuisudmt.bbps.utils.Const.SF_AGENT_ID;
import static com.isuisudmt.bbps.utils.Const.SF_KEYWORD;
import static com.isuisudmt.bbps.utils.Const.SF_LAT_LONG;
import static com.isuisudmt.bbps.utils.Const.SF_MOBILE_NUMBER;
import static com.isuisudmt.bbps.utils.Const.SF_PINCODE;
import static com.isuisudmt.bbps.utils.Const.SF_TERMINAL_ID;
import static com.isuisudmt.bbps.utils.Const.URL_FETCH_BILL;
import static com.isuisudmt.bbps.utils.Const.URL_GET_CCF;
import static com.isuisudmt.bbps.utils.Const.URL_PAY_BILL;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_LPG_GAS_1;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_LPG_GAS_2;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_LPG_GAS_3;

public class ActivityjLpgGas extends AppCompatActivity {

    //Part1
    Spinner spinnerOperator;
    EditText etField26, etField1, etField27, etField28, etField29;
    TextView tvField26, tvField1, tvField27, tvField28, tvField29;
    LinearLayout llMainContentOne, llField26, llField1, llField27, llField28, llField29;

    ImageView ivOperator, ivPaymentType;
    ProgressBar pbOperator, pbPaymentType;

    //Part2
    CardView cvMainContentTwo;
    TextView tvCustomerName, tvBillDate, tvBillAmount, tvBillPeriod, tvBillNumber, tvDueDate, tvFixedCharges, tvAdditionalCharges,
            tvEarlyPaymentFees, tvLatePaymentFees, tvModifySearch;
    Button btnProceedToPay;
    LinearLayout llFetchResponseCustomerName, llFetchResponseBillDate, llFetchResponseBillAmount, llFetchResponseBillPeriod,
            llFetchResponseBillNumber, llFetchResponseDueDate, llFetchResponseFixedCharges, llFetchResponseAdditionalCharges,
            llFetchResponseEarlyPaymentFees, llFetchResponseLatePaymentFees;

    //Part3
    EditText etOptionAmount, etCCF, etTotalAmount, etRemarks;
    Spinner spinnerPaymentMode;
    LinearLayout llMainThree;


    //Buttons
    LinearLayout llBtnFetchPay;
    Button btnPayBill, btnFetchBill;


    String BlrName = "", CatName = "", Bill_Amount = "", Reference_id = "", selected_type = "", selected_payment_type = "", AgentReferenceId = "", tokenStr = "", ccf = "", QuickPay = "", TextField1 = "", AdhocPayment = "";
    Boolean IS_BILL_FETCHED = false;
    Double dTotalAmount = 0.0;

    SessionManager session;
    ArrayList<BillerListPojo> biller_item_arr;
    ArrayAdapter adapterOperator;
    String blrID = "";
    Map<String, Object> otherParameters;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bbps_lpg_gas);

        initValue();

        getOperators();

        setupPaymentMode();

        btnFetchBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IS_BILL_FETCHED = true;

                String _field26 = etField26.getText().toString().trim();
                String _field1 = etField1.getText().toString().trim();
                String _field27 = "";
                String _field28 = "";
                String _field29 = "";

                //Different IF condition for different fields
                if (blrID.equals(BLR_ID_LPG_GAS_1) || blrID.equals(BLR_ID_LPG_GAS_2) || blrID.equals(BLR_ID_LPG_GAS_3))
                    _field27 = etField27.getText().toString().trim();
                if (blrID.equals(BLR_ID_LPG_GAS_3))
                    _field28 = etField28.getText().toString().trim();
                if (blrID.equals(BLR_ID_LPG_GAS_3))
                    _field29 = etField29.getText().toString().trim();


                if (_field26.equals("")) {
                    Toast.makeText(ActivityjLpgGas.this, "Please enter " + TextField1, Toast.LENGTH_SHORT).show();
                } else if (_field1.equals("")) {
                    Toast.makeText(ActivityjLpgGas.this, Const.ERROR_MSG_NULL_CUSTOMER_MOBILE_NO, Toast.LENGTH_SHORT).show();
                } else if (_field1.length() != 10)
                    Toast.makeText(ActivityjLpgGas.this, Const.ERROR_MSG_INVALID_CUSTOMER_MOBILE_NO, Toast.LENGTH_SHORT).show();
                else {
                    fetchBill(blrID, _field26, _field1, _field27, _field28, _field29);
                }

            }
        });

        btnPayBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IS_BILL_FETCHED == true)
                    QuickPay = "0";
                else
                    QuickPay = "1";


                String _postpaid_mobile_no = etField26.getText().toString().trim();
                String _customer_mobile_no = etField1.getText().toString().trim();
                String _amount = etOptionAmount.getText().toString().trim();
                String _field27 = "";
                String _field28 = "";
                String _field29 = "";
                int checkWalletBalance = Double.compare(Const.Wallet2Amount, dTotalAmount);

                //Different IF condition for different fields
                if (blrID.equals(BLR_ID_LPG_GAS_1) || blrID.equals(BLR_ID_LPG_GAS_2) || blrID.equals(BLR_ID_LPG_GAS_3))
                    _field27 = etField27.getText().toString().trim();
                if (blrID.equals(BLR_ID_LPG_GAS_3))
                    _field28 = etField28.getText().toString().trim();
                if (blrID.equals(BLR_ID_LPG_GAS_3))
                    _field29 = etField29.getText().toString().trim();


                if (_postpaid_mobile_no.equals("")) {
                    Toast.makeText(ActivityjLpgGas.this, "Please enter " + TextField1, Toast.LENGTH_SHORT).show();
                } else if (_customer_mobile_no.equals("")) {
                    Toast.makeText(ActivityjLpgGas.this, Const.ERROR_MSG_NULL_CUSTOMER_MOBILE_NO, Toast.LENGTH_SHORT).show();
                } else if (_amount.equals("")) {
                    Toast.makeText(ActivityjLpgGas.this, "Please enter Amount", Toast.LENGTH_SHORT).show();
                } else if (_customer_mobile_no.length() != 10)
                    Toast.makeText(ActivityjLpgGas.this, Const.ERROR_MSG_INVALID_CUSTOMER_MOBILE_NO, Toast.LENGTH_SHORT).show();
                else if (selected_payment_type.equals("Choose Payment Mode") || selected_payment_type.equals(""))
                    Toast.makeText(ActivityjLpgGas.this, "Please select a payment mode", Toast.LENGTH_SHORT).show();
                else if (checkWalletBalance<0)
                    Toast.makeText(ActivityjLpgGas.this, Const.ERROR_MSG_LOW_BALANCE_WALLET_2, Toast.LENGTH_SHORT).show();
                else {
                    payFinalBill(_amount, blrID, _postpaid_mobile_no, _customer_mobile_no, _field27, _field28, _field29);
                }

            }
        });

        btnProceedToPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvMainContentTwo.setVisibility(View.GONE);
                llMainThree.setVisibility(View.VISIBLE);
                llBtnFetchPay.setVisibility(View.VISIBLE);
                btnFetchBill.setVisibility(View.GONE);
                btnPayBill.setVisibility(View.VISIBLE);

                etOptionAmount.setText(Bill_Amount);
                setEdittextEditable(etField26, false, R.drawable.disable_edittext);
                setEdittextEditable(etField1, false, R.drawable.disable_edittext);
                setEdittextEditable(etField27, false, R.drawable.disable_edittext);
                setEdittextEditable(etField28, false, R.drawable.disable_edittext);
                setEdittextEditable(etField29, false, R.drawable.disable_edittext);
                setEdittextEditable(etCCF, false, R.drawable.disable_edittext);
                setEdittextEditable(etTotalAmount, false, R.drawable.disable_edittext);
                if (AdhocPayment.equals("0")) {
                    setEdittextEditable(etOptionAmount, false, R.drawable.disable_edittext);
                } else if (AdhocPayment.equals("1")) {
                    setEdittextEditable(etOptionAmount, true, R.drawable.border_white_bg);
                }

                spinnerOperator.setEnabled(false);
                spinnerOperator.setBackgroundResource(R.drawable.disable_edittext);

            }
        });

        tvModifySearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvMainContentTwo.setVisibility(View.GONE);
                llMainContentOne.setVisibility(View.VISIBLE);
                llBtnFetchPay.setVisibility(View.VISIBLE);
                btnFetchBill.setVisibility(View.VISIBLE);
                btnPayBill.setVisibility(View.GONE);
            }
        });

        etOptionAmount.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    if (selected_payment_type.equals("") || selected_payment_type.equals("Choose Payment Mode")) {

                    } else {
                        //Reset Payment Mode
                        setupPaymentMode();

                        etTotalAmount.setText("");
                        etCCF.setText("");

                        setEdittextEditable(etCCF, true, R.drawable.border_white_bg);
                        setEdittextEditable(etTotalAmount, true, R.drawable.border_white_bg);
                    }
                }
            }
        });

    }

    public void initValue() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("LPG Gas");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        session = new SessionManager(ActivityjLpgGas.this);
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);

        llMainContentOne = (LinearLayout) findViewById(R.id.ll_main_content_one);
        spinnerOperator = (Spinner) findViewById(R.id.operator_spinner);

        TextView tvBoardOrOperator = (TextView) findViewById(R.id.tv_board_operator);
        tvBoardOrOperator.setText("Select Provider");

        tvField26 = (TextView) findViewById(R.id.tv_field26);
        tvField1 = (TextView) findViewById(R.id.tv_field1);
        tvField27 = (TextView) findViewById(R.id.tv_field27);
        tvField28 = (TextView) findViewById(R.id.tv_field28);
        tvField29 = (TextView) findViewById(R.id.tv_field29);

        etField26 = (EditText) findViewById(R.id.et_field26);
        etField1 = (EditText) findViewById(R.id.et_field1);
        etField27 = (EditText) findViewById(R.id.et_field27);
        etField28 = (EditText) findViewById(R.id.et_field28);
        etField29 = (EditText) findViewById(R.id.et_field29);

        llField26 = (LinearLayout) findViewById(R.id.ll_field26);
        llField1 = (LinearLayout) findViewById(R.id.ll_field1);
        llField27 = (LinearLayout) findViewById(R.id.ll_field27);
        llField28 = (LinearLayout) findViewById(R.id.ll_field28);
        llField29 = (LinearLayout) findViewById(R.id.ll_field29);
        ivOperator = findViewById(R.id.iv_arrowdown_select_operator);
        pbOperator = findViewById(R.id.progressbar_operator);
        ivPaymentType = findViewById(R.id.iv_arrowdown_select_payment_mode);
        pbPaymentType = findViewById(R.id.progressbar_payment_mode);

        /*Part 2 of electricity_bill_layout*/
        cvMainContentTwo = findViewById(R.id.cv_bill_details);
        llFetchResponseCustomerName = findViewById(R.id.ll_fetch_response_customer_name);
        llFetchResponseBillDate = findViewById(R.id.ll_fetch_response_bill_date);
        llFetchResponseBillAmount = findViewById(R.id.ll_fetch_response_bill_amount);
        llFetchResponseBillPeriod = findViewById(R.id.ll_fetch_response_bill_period);
        llFetchResponseBillNumber = findViewById(R.id.ll_fetch_response_bill_number);
        llFetchResponseDueDate = findViewById(R.id.ll_fetch_response_due_date);
        llFetchResponseFixedCharges = findViewById(R.id.ll_fetch_response_fixed_charges);
        llFetchResponseAdditionalCharges = findViewById(R.id.ll_fetch_response_additional_charges);
        llFetchResponseEarlyPaymentFees = findViewById(R.id.ll_fetch_response_early_payment_fee);
        llFetchResponseLatePaymentFees = findViewById(R.id.ll_fetch_response_late_payment_fee);


        tvCustomerName = findViewById(R.id.tv_bill_details_customer_name);
        tvBillDate = findViewById(R.id.tv_bill_details_bill_date);
        tvBillAmount = findViewById(R.id.tv_bill_details_bill_amount);
        tvBillPeriod = findViewById(R.id.tv_bill_details_bill_period);
        tvBillNumber = findViewById(R.id.tv_bill_details_bill_number);
        tvDueDate = findViewById(R.id.tv_bill_details_due_date);
        tvFixedCharges = findViewById(R.id.tv_bill_details_fixed_charges);
        tvAdditionalCharges = findViewById(R.id.tv_bill_details_additional_charges);
        tvEarlyPaymentFees = findViewById(R.id.tv_bill_details_early_payment_fee);
        tvLatePaymentFees = findViewById(R.id.tv_bill_details_late_payment_fee);


        tvModifySearch = findViewById(R.id.tv_modify_input_data);
        btnProceedToPay = findViewById(R.id.btn_proceed_to_pay);
        /*----------*/

        /*Part 3*/
        etOptionAmount = findViewById(R.id.et_amount);
        spinnerPaymentMode = findViewById(R.id.spinner_payment_mode);
        etCCF = findViewById(R.id.et_ccf);
        etTotalAmount = findViewById(R.id.et_total_amount);
        etRemarks = findViewById(R.id.et_remarks);
        llMainThree = findViewById(R.id.ll_main_three);
        /*----------*/

        /*Buttons*/
        llBtnFetchPay = findViewById(R.id.ll_button_fetch_pay);
        btnFetchBill = findViewById(R.id.btn_fetch_bill);
        btnPayBill = findViewById(R.id.btn_paybill);
        /*----------*/


        biller_item_arr = new ArrayList<>();

    }

    private void getOperators() {
        ivOperator.setVisibility(View.GONE);
        pbOperator.setVisibility(View.VISIBLE);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents

        DocumentReference docRef = db.collection(DEFAULT_BILLER_COLLECTION).document(PREFIX_DEFAULT_BILLER_DOCUMENT + LPG_GAS);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                ivOperator.setVisibility(View.VISIBLE);
                pbOperator.setVisibility(View.GONE);

                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if (document.contains("billerList")) {
                                //String value = "" + document.getData().values();
                                List<Map<String, Object>> billerList = (List<Map<String, Object>>) document.get("billerList");
                                biller_item_arr.clear();

                                setOperator("NOT_AN_ID", "Choose Provider", null);

                                for (int i = 0; i < billerList.size(); i++) {
                                    setOperator("" + billerList.get(i).get("blrId"), "" + billerList.get(i).get("blrName"), (Map<String, Object>) billerList.get(i).get(billerList.get(i).get("blrId")));
                                }

                                adapterOperator = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, biller_item_arr);
                                spinnerOperator.setAdapter(adapterOperator);

                                spinnerOperator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                        resetAllFields();
                                        ((TextView)parent.getChildAt(0)).setTextColor(Color.BLACK);
                                        if (!biller_item_arr.get(position).blrId.equalsIgnoreCase("NOT_AN_ID")) {
                                            llField26.setVisibility(View.VISIBLE);
                                            llField1.setVisibility(View.VISIBLE);

                                            blrID = biller_item_arr.get(position).blrId;
                                            BlrName = biller_item_arr.get(position).blrName;
                                            otherParameters = biller_item_arr.get(position).other_parameters;

                                            CatName = otherParameters.get("blrCatName").toString();

                                            TextField1 = GetBillerParameters.getParamLpgGas(blrID);
                                            if (!TextField1.equals("")) {
                                                tvField26.setText(TextField1 + ": ");
                                                etField26.setHint("Enter " + TextField1);
                                                edittextValidation(etField26, blrID);
                                            } else
                                                resetAllFields();

                                            if (blrID.equals(BLR_ID_LPG_GAS_1) || blrID.equals(BLR_ID_LPG_GAS_2)) {
                                                llField27.setVisibility(View.VISIBLE);
                                                tvField27.setText(GetBillerParameters.getParamCommonField27(blrID));
                                                field27Validation(etField27, blrID);
                                            }
                                            if (blrID.equals(BLR_ID_LPG_GAS_3)) {
                                                llField27.setVisibility(View.VISIBLE);
                                                llField28.setVisibility(View.VISIBLE);
                                                llField29.setVisibility(View.VISIBLE);
                                                tvField27.setText(GetBillerParameters.getParamCommonField27(blrID));
                                                tvField28.setText(GetBillerParameters.getParamCommonField28(blrID));
                                                tvField29.setText(GetBillerParameters.getParamCommonField29(blrID));
                                                field27Validation(etField27, blrID);
                                                field28Validation(etField28, blrID);
                                                field29Validation(etField29, blrID);
                                            }

                                            AdhocPayment = otherParameters.get("adhocPayment").toString();
                                            buttonVisibilityAmountEditableOperation(otherParameters.get("billAcceptanceType").toString(), AdhocPayment);

                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
//                        updateSession(user_name);
                    }
                } else {

                }
            }
        });
    }

    private void fetchBill(String blrID, String field26, String field1, String field27, String field28, String field29) {
        ProgressDialog dialog = new ProgressDialog(ActivityjLpgGas.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();


        JSONObject obj = new JSONObject();
        try {

            obj.put("payChannel", PAY_CHANNEL);
            obj.put("payMode", PAY_MODE);
            obj.put("blrId", blrID);
            obj.put("fieldValues", GetBillerParameters.getFieldValuesJsonCommonFetchBill(ActivityjLpgGas.this, field1, field26, field27, field28, field29));

            Log.e("Auth: ", tokenStr);

            AndroidNetworking.post(URL_FETCH_BILL)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            dialog.cancel();
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String statusCode = obj.getString("statusCode");
                                String statusDescription = obj.getString("statusDescription");

                                if (statusCode.equals("0")) {
                                    llMainContentOne.setVisibility(View.VISIBLE);
                                    llBtnFetchPay.setVisibility(View.GONE);
                                    cvMainContentTwo.setVisibility(View.VISIBLE);

                                    String commonClassObject = obj.getString("commonClassObject");
                                    JSONObject objCommonClass = new JSONObject(commonClassObject);

                                    String Customer_Name = objCommonClass.optString("Customer_Name");
                                    String Bill_Date = objCommonClass.optString("Bill_Date");
                                    Bill_Amount = objCommonClass.optString("Bill_Amount");
                                    String Bill_Period = objCommonClass.optString("Bill_Period");
                                    String Bill_Number = objCommonClass.optString("Bill_Number");
                                    String Due_Date = objCommonClass.optString("Due_Date");
                                    String Fixed_Charges = objCommonClass.optString("Fixed_Charges");
                                    String Additional_Charges = objCommonClass.optString("Additional_Charges");
                                    String Early_Payment_Fees = objCommonClass.optString("Early_Payment_Fee");
                                    String Late_Payment_Fees = objCommonClass.optString("Late_Payment_Fee");
                                    Reference_id = objCommonClass.optString("Reference_id");
                                    AgentReferenceId = objCommonClass.optString("Agnt_Reference_Id");

                                    checkFetchResponse(Customer_Name, llFetchResponseCustomerName, tvCustomerName);
                                    checkFetchResponse(Bill_Date, llFetchResponseBillDate, tvBillDate);
                                    checkFetchResponse(Bill_Amount, llFetchResponseBillAmount, tvBillAmount);
                                    checkFetchResponse(Bill_Period, llFetchResponseBillPeriod, tvBillPeriod);
                                    checkFetchResponse(Bill_Number, llFetchResponseBillNumber, tvBillNumber);
                                    checkFetchResponse(Due_Date, llFetchResponseDueDate, tvDueDate);
                                    checkFetchResponse(Fixed_Charges, llFetchResponseFixedCharges, tvFixedCharges);
                                    checkFetchResponse(Additional_Charges, llFetchResponseAdditionalCharges, tvAdditionalCharges);
                                    checkFetchResponse(Early_Payment_Fees, llFetchResponseEarlyPaymentFees, tvEarlyPaymentFees);
                                    checkFetchResponse(Late_Payment_Fees, llFetchResponseLatePaymentFees, tvLatePaymentFees);

                                } else {
                                    Toast.makeText(ActivityjLpgGas.this, GetBillerParameters.popupError(obj), Toast.LENGTH_LONG).show();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            dialog.cancel();
                            try {
                                Toast.makeText(ActivityjLpgGas.this, GetBillerParameters.popupError(new JSONObject(anError.getErrorBody().toString())), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCCF(String SelectedPaymentMode, String BillAmount) {
        ivPaymentType.setVisibility(View.GONE);
        pbPaymentType.setVisibility(View.VISIBLE);

        JSONObject obj = new JSONObject();
        try {
            obj.put("billAmt", BillAmount);
            obj.put("blrId", blrID);
            obj.put("payChannel", PAY_CHANNEL);
            obj.put("payMode", SelectedPaymentMode);

            AndroidNetworking.post(URL_GET_CCF)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            ivPaymentType.setVisibility(View.VISIBLE);
                            pbPaymentType.setVisibility(View.GONE);

                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String statusCode = obj.getString("statusCode");
                                String statusDescription = obj.getString("statusDescription");

                                if (statusCode.equals("0")) {
                                    String commonClassObject = obj.getString("commonClassObject");
                                    JSONObject objCommonClass = new JSONObject(commonClassObject);
                                    ccf = objCommonClass.getString("ccf");
                                    etCCF.setText(ccf);
                                    dTotalAmount = Double.parseDouble(BillAmount) + Double.parseDouble(ccf);
                                    etTotalAmount.setText("" + dTotalAmount);
                                    Log.e("Total Amount: ", "" + dTotalAmount);
                                    Log.e("CCF: ", ccf);

                                    etCCF.setEnabled(false);
                                    etCCF.setBackgroundResource(R.drawable.disable_edittext);
                                    etCCF.setPadding(12, 0, 12, 0);
                                    etTotalAmount.setEnabled(false);
                                    etTotalAmount.setBackgroundResource(R.drawable.disable_edittext);
                                    etTotalAmount.setPadding(12, 0, 12, 0);

                                } else {
                                    Toast.makeText(ActivityjLpgGas.this, GetBillerParameters.popupError(obj), Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            ivPaymentType.setVisibility(View.VISIBLE);
                            pbPaymentType.setVisibility(View.GONE);
                            try {
                                Toast.makeText(ActivityjLpgGas.this, GetBillerParameters.popupError(new JSONObject(anError.getErrorBody().toString())), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void payFinalBill(String billAmount, String blrID, String field26, String field1, String field27, String field28, String field29) {
        ProgressDialog dialog = new ProgressDialog(ActivityjLpgGas.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();
        try {

            obj.put("payChannel", PAY_CHANNEL);
            obj.put("payMode", PAY_MODE);
            obj.put("blrId", blrID);
            obj.put("refId", Reference_id);
            obj.put("debitNar1", etRemarks.getText().toString().trim());
            obj.put("quickPay", QuickPay);//0:Fetch/1:Pay
            obj.put("ccf", ccf);
            obj.put("billAmt", billAmount);
            obj.put("totalAmt", dTotalAmount);
            obj.put("catName", CatName);
            obj.put("mobileNumber", field1);//Payee Mobile Number
            obj.put("blrName", BlrName);
            obj.put("fieldValues",
                    GetBillerParameters.getFieldValuesJsonCommonPayBill(blrID,
                            getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE).getString(SF_PINCODE, ""),
                            getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE).getString(SF_MOBILE_NUMBER, ""),
                            getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE).getString(SF_LAT_LONG, ""),
                            getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE).getString(SF_TERMINAL_ID, ""),
                            getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE).getString(SF_KEYWORD, ""),
                            getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE).getString(SF_AGENT_ID, ""),
                            AgentReferenceId, field1, field26, field27, field28, field29));

            AndroidNetworking.post(URL_PAY_BILL)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            dialog.cancel();

                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String statusCode = obj.getString("statusCode");

                                if (statusCode.equals("0")) {
                                    Intent intent = new Intent(ActivityjLpgGas.this, ActivityReceiptBbps.class);
                                    intent.putExtra("Response", response.toString());
                                    startActivity(intent);
                                    finish();
                                } else {
                                    String _pay_bill_error = GetBillerParameters.getPayBillError(new JSONObject(response.toString()));

                                    if (_pay_bill_error.equals("TAKE_TO_RECEIPT")){
                                        Intent intent = new Intent(ActivityjLpgGas.this, ActivityReceiptBbps.class);
                                        intent.putExtra("Response", response.toString());
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        Toast.makeText(ActivityjLpgGas.this, _pay_bill_error, Toast.LENGTH_LONG).show();
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            dialog.cancel();

                            try {
                                String _pay_bill_error = GetBillerParameters.getPayBillError(new JSONObject(anError.getErrorBody().toString()));

                                if (_pay_bill_error.equals("TAKE_TO_RECEIPT")){
                                    Intent intent = new Intent(ActivityjLpgGas.this, ActivityReceiptBbps.class);
                                    intent.putExtra("Response", anError.getErrorBody().toString());
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(ActivityjLpgGas.this, _pay_bill_error, Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupPaymentMode() {
        selected_payment_type = "";

        final List<String> PaymentList = new ArrayList<>(Arrays.asList(Const.arrPaymentType));
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, PaymentList) {
            @Override
            public boolean isEnabled(int position) {
                if (position != 1) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 1) {
                    // Set the disable item text color
                    tv.setTextColor(Color.BLACK);
                } else {
                    tv.setTextColor(Color.GRAY);
                }
                return view;
            }
        };

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerPaymentMode.setAdapter(spinnerArrayAdapter);
        spinnerArrayAdapter.notifyDataSetChanged();


        spinnerPaymentMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selected_payment_type = PaymentList.get(position);

                if (position == 1) {
                    if (etOptionAmount.getText().toString().trim().equals("") || etOptionAmount.getText().toString().trim().equals("0")
                            || etOptionAmount.getText().toString().trim().equals(null)) {
                        Toast.makeText(ActivityjLpgGas.this, "Please enter amount.", Toast.LENGTH_SHORT).show();
                    } else
                        getCCF(selected_payment_type, etOptionAmount.getText().toString().trim());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void buttonVisibilityAmountEditableOperation(String billAcceptanceType, String adhocPayment) {

        if (billAcceptanceType.equals("0")) {
            //Fetch Bill active
            QuickPay = "0";

            llBtnFetchPay.setVisibility(View.VISIBLE);
            btnFetchBill.setVisibility(View.VISIBLE);
            btnPayBill.setVisibility(View.GONE);
        } else if (billAcceptanceType.equals("1")) {
            //Pay Bill active
            QuickPay = "1";

            llBtnFetchPay.setVisibility(View.VISIBLE);
            llMainThree.setVisibility(View.VISIBLE);
            btnFetchBill.setVisibility(View.GONE);
            btnPayBill.setVisibility(View.VISIBLE);
        } else if (billAcceptanceType.equals("2")) {
            //Both active

            llBtnFetchPay.setVisibility(View.VISIBLE);
            llMainThree.setVisibility(View.VISIBLE);
            btnFetchBill.setVisibility(View.VISIBLE);
            btnPayBill.setVisibility(View.VISIBLE);
        }

        if (adhocPayment.equals("0")) {
            setEdittextEditable(etOptionAmount, false, R.drawable.disable_edittext);
        } else if (adhocPayment.equals("1")) {
            setEdittextEditable(etOptionAmount, true, R.drawable.border_white_bg);
        }

    }

    private void resetAllFields() {
        cvMainContentTwo.setVisibility(View.GONE);
        llMainThree.setVisibility(View.GONE);
        llBtnFetchPay.setVisibility(View.GONE);

        llField26.setVisibility(View.GONE);
        llField1.setVisibility(View.GONE);
        llField27.setVisibility(View.GONE);
        llField28.setVisibility(View.GONE);
        llField29.setVisibility(View.GONE);

        etField26.setText("");
        etField1.setText("");
        etField27.setText("");
        etField28.setText("");
        etField29.setText("");

        etOptionAmount.setText("");
        etTotalAmount.setText("");
        etCCF.setText("");
        etRemarks.setText("");

        //reset bill details(Fetch Bill data)
        tvCustomerName.setText("");
        tvBillDate.setText("");
        tvBillAmount.setText("");
        tvBillPeriod.setText("");
        tvBillNumber.setText("");
        tvDueDate.setText("");

        setEdittextEditable(etCCF, true, R.drawable.border_white_bg);
        setEdittextEditable(etTotalAmount, true, R.drawable.border_white_bg);

        setupPaymentMode();

    }

    public void setOperator(String board_id, String board_name, Map<String, Object> other_parameters) {

        BillerListPojo billerListPojo = new BillerListPojo();
        billerListPojo.setBlrId(board_id);
        billerListPojo.setBlrName(board_name);
        billerListPojo.setOther_parameters(other_parameters);

        biller_item_arr.add(billerListPojo);

    }

    public void setEdittextEditable(EditText et, Boolean editable, int drawable) {
        et.setEnabled(editable);
        et.setBackgroundResource(drawable);
        et.setPadding(12, 0, 12, 0);
    }


    private void checkFetchResponse(String response, LinearLayout llFetchResponseCustomerName, TextView tvCustomerName) {
        if (response.equals("") || response.equals("NA"))
            llFetchResponseCustomerName.setVisibility(View.GONE);
        else {
            llFetchResponseCustomerName.setVisibility(View.VISIBLE);
            tvCustomerName.setText(response);
        }

    }

    private void edittextValidation(EditText etField26, String blrID) {
        etField26.setInputType(InputType.TYPE_CLASS_NUMBER);

        switch (blrID) {
            case "BHAR00000NATR4":
            case "INDI00000NATT5":
                etField26.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                break;

            case "HPCL00000NAT01":
                etField26.setFilters(new InputFilter[]{new InputFilter.LengthFilter(8)});
                break;
            default:
                etField26.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
                break;
        }

    }

    private void field27Validation(EditText etField27, String blrID) {
        switch (blrID) {
            case "BHAR00000NATR4":
            case "HPCL00000NAT01":
                etField27.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;
            case "INDI00000NATT5":
                etField27.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
            default:
                etField26.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
        }

        switch (blrID) {
            case "BHAR00000NATR4":
            case "INDI00000NATT5":
                etField27.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
                break;
            case "HPCL00000NAT01":
                etField27.setFilters(new InputFilter[]{new InputFilter.LengthFilter(8)});
                break;
            default:
                etField27.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
                break;
        }


    }

    private void field28Validation(EditText etField28, String blrID) {
        switch (blrID) {
            case "INDI00000NATT5":
                etField28.setInputType(InputType.TYPE_CLASS_NUMBER);
                etField28.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
                break;
            default:
                etField28.setInputType(InputType.TYPE_CLASS_TEXT);
                etField28.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
                break;
        }
    }

    private void field29Validation(EditText etField29, String blrID) {
        switch (blrID) {
            case "INDI00000NATT5":
                etField29.setInputType(InputType.TYPE_CLASS_TEXT);
                etField29.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
                break;
            default:
                etField29.setInputType(InputType.TYPE_CLASS_TEXT);
                etField29.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
                break;
        }
    }
}
