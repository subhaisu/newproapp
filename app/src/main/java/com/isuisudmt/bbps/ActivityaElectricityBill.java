package com.isuisudmt.bbps;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isuisudmt.MainActivity;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.bbps.utils.BillerListPojo;
import com.isuisudmt.bbps.utils.Const;
import com.isuisudmt.bbps.utils.Electricity_State_Model;
import com.isuisudmt.bbps.utils.GetBillerParameters;
import com.isuisudmt.bbps.utils.GetBillerSampleImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import static com.isuisudmt.bbps.utils.Const.URL_FETCH_BILL;
import static com.isuisudmt.bbps.utils.Const.URL_PAY_BILL;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_ELECTRICITY_24;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_ELECTRICITY_25;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_ELECTRICITY_27;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_ELECTRICITY_34;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_ELECTRICITY_44;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_LOAN_REPAYMENT_10;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_LOAN_REPAYMENT_16;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_LOAN_REPAYMENT_18;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_LOAN_REPAYMENT_19;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_LOAN_REPAYMENT_2;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_LOAN_REPAYMENT_24;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_LOAN_REPAYMENT_29;
import static com.isuisudmt.bbps.utils.GetBillerParameters.BLR_ID_LOAN_REPAYMENT_3;
import static com.isuisudmt.utils.Constants.BBPS_PREF;
import static com.isuisudmt.utils.Constants.SF_AGENT_ID;
import static com.isuisudmt.utils.Constants.SF_KEYWORD;
import static com.isuisudmt.utils.Constants.SF_LAT_LONG;
import static com.isuisudmt.utils.Constants.SF_MOBILE_NUMBER;
import static com.isuisudmt.utils.Constants.PAY_CHANNEL;
import static com.isuisudmt.utils.Constants.PAY_MODE;
import static com.isuisudmt.utils.Constants.SF_PINCODE;
import static com.isuisudmt.utils.Constants.SF_TERMINAL_ID;

public class ActivityaElectricityBill extends AppCompatActivity {

    private static final String TAG = "Electricity Bill";
    //Part1
    LinearLayout llMainContentOne, llState, llElectricBoard, llConsumerNo, llMobileNo, llOption3, llOption4;
    TextView tvConsumerNo, tvPhoneNo, tvOption3, tvOption4, tvSampleBill;
    EditText etConsumerNo, etPhoneNo, etOption3;
    ImageView ivElectricBoardArrow;
    ProgressBar pbElectricBoard;

    //Part2
    CardView cvMainContentTwo;
    TextView tvCustomerName, tvBillDate, tvBillAmount, tvBillPeriod, tvBillNumber, tvDueDate, tvFixedCharges, tvAdditionalCharges,
            tvEarlyPaymentFees, tvLatePaymentFees, tvModifySearch;
    Button btnProceedToPay;
    LinearLayout llFetchResponseCustomerName, llFetchResponseBillDate, llFetchResponseBillAmount, llFetchResponseBillPeriod,
            llFetchResponseBillNumber, llFetchResponseDueDate, llFetchResponseFixedCharges, llFetchResponseAdditionalCharges,
            llFetchResponseEarlyPaymentFees, llFetchResponseLatePaymentFees;

    //Part3
    EditText etOptionAmount, etCCF, etTotalAmount, etRemarks;
    Spinner spinnerPaymentMode;
    LinearLayout llMainThree;

    //Buttons
    LinearLayout llBtnFetchPay;
    Button btnPayBill, btnFetchBill;

    AppCompatSpinner spinnerState, spinnerBoard, spinnerType;
    ArrayList<Electricity_State_Model> state_models_arr;
    ArrayList<BillerListPojo> biller_item_arr;
    String[] arrTypes = {"LT", "HT"};

    ArrayAdapter state_arr_adapter, board_arr_adapter, typeAdapter, paymentTypeAdapter;
    String state_ID = "", blrID = "", selected_payment_type = "", tokenStr = "", ccf = "";
    Map<String, Object> otherParameters;

    String[] arrPaymentType = {"Choose Payment Mode", "Cash", "Credit Card", "Debit Card", "IMPS", "Internet Banking", "NEFT", "Prepaid Card", "UPI", "Wallet"};

    SessionManager session;
    SharedPreferences spBbps;
    String Pincode, PosMobileNumber, LatLong, TerminalID, Keyword, AgentId, BlrName = "", CatName = "", AdhocPayment = "", BillAcceptance = "", Bill_Amount = "", Reference_id = "", AgentReferenceId = "", sampleBillURL = "";
    Double dTotalAmount = 0.0;
    private String QuickPay = "";
    String field27 = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bbps_electricity_bill);

        initValue();

        paymentTypeAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, arrPaymentType);

        state_arr_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, state_models_arr);
        spinnerState.setAdapter(state_arr_adapter);

        spinnerState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                resetAllFields();

                if (state_models_arr.get(position).state_id.equalsIgnoreCase("IND")) {

                } else {
                    state_ID = state_models_arr.get(position).state_id;
                    llElectricBoard.setVisibility(View.VISIBLE);
                    checkElectricity_state(state_ID);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        setupPaymentMode();

    }

    public void initValue() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Electricity Bill");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        session = new SessionManager(ActivityaElectricityBill.this);
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);

        spBbps = getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE);
        Pincode = spBbps.getString(SF_PINCODE, "");
        PosMobileNumber = spBbps.getString(SF_MOBILE_NUMBER, "");
        LatLong = spBbps.getString(SF_LAT_LONG, "");
        TerminalID = spBbps.getString(SF_TERMINAL_ID, "");
        Keyword = spBbps.getString(SF_KEYWORD, "");
        AgentId = spBbps.getString(SF_AGENT_ID, "");


        /*Part 1 of electricity_bill_layout*/
        llMainContentOne = findViewById(R.id.ll_main_content_one);

        llState = findViewById(R.id.state_LL);
        llElectricBoard = findViewById(R.id.electric_board_LL);
        llConsumerNo = findViewById(R.id.ll_option_consumer_no);
        llMobileNo = findViewById(R.id.ll_option_phone_no);
        llOption3 = findViewById(R.id.ll_option_3);
        llOption4 = findViewById(R.id.ll_option_4);

        spinnerState = findViewById(R.id.state_spinner);
        spinnerBoard = findViewById(R.id.elec_board_spinner);
        etConsumerNo = findViewById(R.id.et_option_consumer_no);
        etPhoneNo = findViewById(R.id.et_option_phone_no);
        etOption3 = findViewById(R.id.et_option3);
        spinnerType = findViewById(R.id.spinner_type);


        tvConsumerNo = findViewById(R.id.tv_option_consumer_no);
        tvPhoneNo = findViewById(R.id.tv_option_phone_no);
        tvOption3 = findViewById(R.id.tv_option3);
        tvOption4 = findViewById(R.id.tv_option4);
        tvSampleBill = findViewById(R.id.tv_sample_bill);


        ivElectricBoardArrow = findViewById(R.id.iv_arrowdown_electric_board);
        pbElectricBoard = findViewById(R.id.progressbar_electric_board);
        /*----------*/

        /*Part 2 of electricity_bill_layout*/
        cvMainContentTwo = findViewById(R.id.cv_bill_details);
        llFetchResponseCustomerName = findViewById(R.id.ll_fetch_response_customer_name);
        llFetchResponseBillDate = findViewById(R.id.ll_fetch_response_bill_date);
        llFetchResponseBillAmount = findViewById(R.id.ll_fetch_response_bill_amount);
        llFetchResponseBillPeriod = findViewById(R.id.ll_fetch_response_bill_period);
        llFetchResponseBillNumber = findViewById(R.id.ll_fetch_response_bill_number);
        llFetchResponseDueDate = findViewById(R.id.ll_fetch_response_due_date);
        llFetchResponseFixedCharges = findViewById(R.id.ll_fetch_response_fixed_charges);
        llFetchResponseAdditionalCharges = findViewById(R.id.ll_fetch_response_additional_charges);
        llFetchResponseEarlyPaymentFees = findViewById(R.id.ll_fetch_response_early_payment_fee);
        llFetchResponseLatePaymentFees = findViewById(R.id.ll_fetch_response_late_payment_fee);


        tvCustomerName = findViewById(R.id.tv_bill_details_customer_name);
        tvBillDate = findViewById(R.id.tv_bill_details_bill_date);
        tvBillAmount = findViewById(R.id.tv_bill_details_bill_amount);
        tvBillPeriod = findViewById(R.id.tv_bill_details_bill_period);
        tvBillNumber = findViewById(R.id.tv_bill_details_bill_number);
        tvDueDate = findViewById(R.id.tv_bill_details_due_date);
        tvFixedCharges = findViewById(R.id.tv_bill_details_fixed_charges);
        tvAdditionalCharges = findViewById(R.id.tv_bill_details_additional_charges);
        tvEarlyPaymentFees = findViewById(R.id.tv_bill_details_early_payment_fee);
        tvLatePaymentFees = findViewById(R.id.tv_bill_details_late_payment_fee);


        tvModifySearch = findViewById(R.id.tv_modify_input_data);
        btnProceedToPay = findViewById(R.id.btn_proceed_to_pay);
        /*----------*/


        /*Part 3 of electricity_bill_layout*/
        etOptionAmount = findViewById(R.id.et_amount);
        spinnerPaymentMode = findViewById(R.id.spinner_payment_mode);
        etCCF = findViewById(R.id.et_ccf);
        etTotalAmount = findViewById(R.id.et_total_amount);
        etRemarks = findViewById(R.id.et_remarks);
        llMainThree = findViewById(R.id.ll_main_three);
        /*----------*/

        /*Buttons*/
        llBtnFetchPay = findViewById(R.id.ll_button_fetch_pay);
        btnFetchBill = findViewById(R.id.btn_fetch_bill);
        btnPayBill = findViewById(R.id.btn_paybill);
        /*----------*/

        tvModifySearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvMainContentTwo.setVisibility(View.GONE);
                llMainContentOne.setVisibility(View.VISIBLE);
                llBtnFetchPay.setVisibility(View.VISIBLE);
                btnFetchBill.setVisibility(View.VISIBLE);
                btnPayBill.setVisibility(View.GONE);
            }
        });

        btnFetchBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickPay = "0";

                String _field26 = etConsumerNo.getText().toString().trim();
                String _field1 = etPhoneNo.getText().toString().trim();
                String _field28 = "";
                String _field29 = "";

                if (blrID.equals(BLR_ID_LOAN_REPAYMENT_2) || blrID.equals(BLR_ID_LOAN_REPAYMENT_3) || blrID.equals(BLR_ID_LOAN_REPAYMENT_10) ||
                        blrID.equals(BLR_ID_LOAN_REPAYMENT_16) || blrID.equals(BLR_ID_LOAN_REPAYMENT_18) || blrID.equals(BLR_ID_LOAN_REPAYMENT_19) || blrID.equals(BLR_ID_LOAN_REPAYMENT_24) || blrID.equals(BLR_ID_LOAN_REPAYMENT_29))
                    field27 = etOption3.getText().toString().trim();

                if (_field26.equals("")) {
                    Toast.makeText(ActivityaElectricityBill.this, "Please enter " + TextField1, Toast.LENGTH_SHORT).show();
                } else if (_field1.equals("")) {
                    Toast.makeText(ActivityaElectricityBill.this, Const.ERROR_MSG_NULL_CUSTOMER_MOBILE_NO, Toast.LENGTH_SHORT).show();
                } else if (_field1.length() != 10)
                    Toast.makeText(ActivityaElectricityBill.this, Const.ERROR_MSG_INVALID_CUSTOMER_MOBILE_NO, Toast.LENGTH_SHORT).show();
                else {
                    fetchBill(blrID, _field26, _field1, field27, _field28, _field29);
                }

            }
        });

        btnPayBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickPay = "1";

                String billAmount = etOptionAmount.getText().toString().trim();
                String field1 = etPhoneNo.getText().toString().trim();
                String field26 = etConsumerNo.getText().toString();
                String field28 = "";
                String field29 = "";

                int checkWalletBalance = Double.compare(Const.Wallet2Amount, dTotalAmount);

                if (blrID.equals(BLR_ID_ELECTRICITY_24) || blrID.equals(BLR_ID_ELECTRICITY_25) || blrID.equals(BLR_ID_ELECTRICITY_27) ||
                        blrID.equals(BLR_ID_ELECTRICITY_34)) {
                    field27 = etOption3.getText().toString().trim();
                }

                if (field26.equals("")) {
                    Toast.makeText(ActivityaElectricityBill.this, "Please enter " + TextField1, Toast.LENGTH_SHORT).show();
                } else if (field1.equals("")) {
                    Toast.makeText(ActivityaElectricityBill.this, Const.ERROR_MSG_NULL_CUSTOMER_MOBILE_NO, Toast.LENGTH_SHORT).show();
                } else if (field1.length() != 10)
                    Toast.makeText(ActivityaElectricityBill.this, Const.ERROR_MSG_INVALID_CUSTOMER_MOBILE_NO, Toast.LENGTH_SHORT).show();
                else if (billAmount.equals("")) {
                    Toast.makeText(ActivityaElectricityBill.this, "Please enter Amount", Toast.LENGTH_SHORT).show();
                } else if (selected_payment_type.equals("Choose Payment Mode") || selected_payment_type.equals(""))
                    Toast.makeText(ActivityaElectricityBill.this, "Please select a payment mode", Toast.LENGTH_SHORT).show();
                else if (checkWalletBalance < 0)
                    Toast.makeText(ActivityaElectricityBill.this, Const.ERROR_MSG_LOW_BALANCE_WALLET_2, Toast.LENGTH_SHORT).show();
                else {
                    payFinalBill(billAmount, blrID, field26, field1, field27, field28, field29);
                }

            }
        });

        btnProceedToPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cvMainContentTwo.setVisibility(View.GONE);
                llMainThree.setVisibility(View.VISIBLE);
                llBtnFetchPay.setVisibility(View.VISIBLE);
                btnFetchBill.setVisibility(View.GONE);
                btnPayBill.setVisibility(View.VISIBLE);

                etOptionAmount.setText(Bill_Amount);
                setEdittextEditable(etConsumerNo, false, R.drawable.disable_edittext);
                setEdittextEditable(etPhoneNo, false, R.drawable.disable_edittext);
                setEdittextEditable(etOption3, false, R.drawable.disable_edittext);
                setEdittextEditable(etCCF, false, R.drawable.disable_edittext);
                setEdittextEditable(etTotalAmount, false, R.drawable.disable_edittext);

                if (AdhocPayment.equals("0")) {
                    setEdittextEditable(etOptionAmount, false, R.drawable.disable_edittext);
                } else if (AdhocPayment.equals("1")) {
                    setEdittextEditable(etOptionAmount, true, R.drawable.border_white_bg);
                }

                spinnerState.setEnabled(false);
                spinnerState.setBackgroundResource(R.drawable.disable_edittext);
                spinnerBoard.setEnabled(false);
                spinnerBoard.setBackgroundResource(R.drawable.disable_edittext);
                spinnerType.setEnabled(false);
                spinnerType.setBackgroundResource(R.drawable.disable_edittext);

            }
        });

        etOptionAmount.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    if (selected_payment_type.equals("") || selected_payment_type.equals("Choose Payment Mode")) {

                    } else {
                        //Reset Payment Mode
                        setupPaymentMode();

                        etTotalAmount.setText("");
                        etCCF.setText("");

                        setEdittextEditable(etCCF, true, R.drawable.border_white_bg);
                        setEdittextEditable(etTotalAmount, true, R.drawable.border_white_bg);
                    }

                }
            }
        });

        tvSampleBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!sampleBillURL.equals("")){
                    Intent intent = new Intent(ActivityaElectricityBill.this, ActivityPreviewBill.class);
                    intent.putExtra("SampleBillURL", sampleBillURL);
                    startActivity(intent);
                } else {
                    Toast.makeText(ActivityaElectricityBill.this, "Oops Something went wrong. Please try again later", Toast.LENGTH_SHORT).show();
                }
            }
        });

        state_models_arr = new ArrayList<>();
        biller_item_arr = new ArrayList<>();

        setState("IND", "Please select state");
        setState("IND-PUD", "Puducherry");
        setState("IND-BIH", "Bihar");
        setState("IND-WBL", "West Bengal");
        setState("IND-DAD", "Daman and Diu");
        setState("IND-PUN", "Punjab");
        setState("IND-ODI", "Odisha");
        setState("IND-UTT", "Uttarakhand");
        setState("IND-UTP", "Uttar Pradesh");
        setState("IND-ASM", "Assam");
        setState("IND-GUJ", "Gujarat");
        setState("IND-GOA", "Goa");
        setState("IND-ANP", "Andhra Pradesh");
        setState("IND-CHH", "Chhattisgarh");
        setState("IND-JHA", "Jharkhand");
        setState("IND-DNH", "Dadra and Nagar Haveli");
        setState("IND-MIZ", "Mizoram");
        setState("IND-TND", "Tamil Nadu");
        setState("IND-DEL", "Delhi");
        setState("IND-TRI", "Tripura");
        setState("IND-MAH", "Maharashtra");
        setState("IND-NAG", "Nagaland");
        setState("IND-SIK", "Sikkim");
        setState("IND-HIP", "Himachal Pradesh");
        setState("IND-MEG", "Meghalaya");
        setState("IND-RAJ", "Rajasthan");
        setState("IND-CHA", "Chandigarh");
        setState("IND-KAR", "Karnataka");
        setState("IND-MAP", "Madhya Pradesh");
        setState("IND-KER", "Kerala");
        setState("IND-HAR", "Haryana");

    }

    String TextField1 = "";

    private void checkElectricity_state(String state_id) {
        ivElectricBoardArrow.setVisibility(View.GONE);
        pbElectricBoard.setVisibility(View.VISIBLE);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents

        DocumentReference docRef = db.collection("Electricity_Collection").document(state_id);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                ivElectricBoardArrow.setVisibility(View.VISIBLE);
                pbElectricBoard.setVisibility(View.GONE);

                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if (document.contains("billerList")) {
                                //String value = "" + document.getData().values();

                                List<Map<String, Object>> billerList = (List<Map<String, Object>>) document.get("billerList");
                                biller_item_arr.clear();

                                setBoard("NOT_AN_ID", "Please select electricity board", null);

                                for (int i = 0; i < billerList.size(); i++) {
                                    setBoard("" + billerList.get(i).get("blrId"), "" + billerList.get(i).get("blrName"), (HashMap<String, Object>) billerList.get(i).get(billerList.get(i).get("blrId")));
                                }

                                board_arr_adapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, biller_item_arr);
                                spinnerBoard.setAdapter(board_arr_adapter);

                                spinnerBoard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        resetAllFields();
                                        ((TextView)parent.getChildAt(0)).setTextColor(Color.BLACK);
                                        if (biller_item_arr.get(position).blrId.equalsIgnoreCase("NOT_AN_ID")) {

                                        } else {

                                            llElectricBoard.setVisibility(View.VISIBLE);
                                            llConsumerNo.setVisibility(View.VISIBLE);
                                            llMobileNo.setVisibility(View.VISIBLE);

                                            blrID = biller_item_arr.get(position).blrId;
                                            BlrName = biller_item_arr.get(position).blrName;
                                            otherParameters = biller_item_arr.get(position).other_parameters;

                                            CatName = otherParameters.get("blrCatName").toString();

                                            String _blr_id = String.valueOf(otherParameters.get("blrId"));
                                            TextField1 = GetBillerParameters.getParamElectricity(_blr_id);

                                            //For Sample Bill
                                            sampleBillURL = GetBillerSampleImage.getBillImage(blrID);
                                            if (!sampleBillURL.equals("")){
                                                tvSampleBill.setVisibility(View.VISIBLE);
                                            } else {
                                                tvSampleBill.setVisibility(View.GONE);
                                            }

                                            //For TextView field26
                                            if (!TextField1.equals("")) {
                                                tvConsumerNo.setText(TextField1 + ": ");
                                                etConsumerNo.setHint("Enter " + TextField1);
                                                field26Validation(etConsumerNo, blrID);
                                            } else
                                                resetAllFields();


                                            tvPhoneNo.setText("Customer Mobile Number");
                                            etPhoneNo.setHint("Enter Customer Mobile Number");

                                            if (_blr_id.equals(BLR_ID_ELECTRICITY_44)) {
                                                //Pundychery
                                                llOption3.setVisibility(View.GONE);
                                                llOption4.setVisibility(View.VISIBLE);
                                                tvOption4.setText("Type");

                                                typeAdapter = new ArrayAdapter(ActivityaElectricityBill.this, android.R.layout.simple_spinner_dropdown_item, arrTypes);
                                                spinnerType.setAdapter(typeAdapter);

                                                spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                                    @Override
                                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                        field27 = arrTypes[position];
                                                    }

                                                    @Override
                                                    public void onNothingSelected(AdapterView<?> parent) {

                                                    }
                                                });


                                            } else if (_blr_id.equals(BLR_ID_ELECTRICITY_24) || _blr_id.equals(BLR_ID_ELECTRICITY_25) ||
                                                    _blr_id.equals(BLR_ID_ELECTRICITY_27) || _blr_id.equals(BLR_ID_ELECTRICITY_34)) {
                                                llOption3.setVisibility(View.VISIBLE);
                                                llOption4.setVisibility(View.GONE);
                                                tvOption3.setText(GetBillerParameters.getParamCommonField27(blrID));
                                                etOption3.setHint("Enter " + GetBillerParameters.getParamCommonField27(blrID));
                                                field27Validation(etOption3, blrID);

                                            }

                                            AdhocPayment = otherParameters.get("adhocPayment").toString();
                                            buttonVisibilityAmountEditableOperation(otherParameters.get("billAcceptanceType").toString(), AdhocPayment);

                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
//                        updateSession(user_name);
                    }
                } else {

                }
            }
        });
    }

    private void fetchBill(String blrID, String field26, String field1, String field27, String field28, String field29) {
        ProgressDialog dialog = new ProgressDialog(ActivityaElectricityBill.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();
        try {
            obj.put("payChannel", PAY_CHANNEL);
            obj.put("payMode", PAY_MODE);
            obj.put("blrId", blrID);
            obj.put("fieldValues", GetBillerParameters.getFieldValuesJsonCommonFetchBill(ActivityaElectricityBill.this, field1, field26, field27, field28, field29));
            Log.e("Auth: ", obj +"jsvdjhasvjhdvjhsajh");
            Log.e("Auth: ", tokenStr);

            Log.d(TAG, "OBJ: " +obj);
            Log.d(TAG, "TOKEN: "+ tokenStr);


            AndroidNetworking.post(URL_FETCH_BILL)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            dialog.cancel();
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String statusCode = obj.getString("statusCode");
                                String statusDescription = obj.getString("statusDescription");

                                if (statusCode.equals("0")) {
                                    llMainContentOne.setVisibility(View.VISIBLE);
                                    llBtnFetchPay.setVisibility(View.GONE);
                                    cvMainContentTwo.setVisibility(View.VISIBLE);

                                    String commonClassObject = obj.getString("commonClassObject");
                                    JSONObject objCommonClass = new JSONObject(commonClassObject);

                                    String Customer_Name = objCommonClass.optString("Customer_Name");
                                    String Bill_Date = objCommonClass.optString("Bill_Date");
                                    Bill_Amount = objCommonClass.optString("Bill_Amount");
                                    String Bill_Period = objCommonClass.optString("Bill_Period");
                                    String Bill_Number = objCommonClass.optString("Bill_Number");
                                    String Due_Date = objCommonClass.optString("Due_Date");
                                    String Fixed_Charges = objCommonClass.optString("Fixed_Charges");
                                    String Additional_Charges = objCommonClass.optString("Additional_Charges");
                                    String Early_Payment_Fees = objCommonClass.optString("Early_Payment_Fee");
                                    String Late_Payment_Fees = objCommonClass.optString("Late_Payment_Fee");
                                    Reference_id = objCommonClass.optString("Reference_id");
                                    AgentReferenceId = objCommonClass.optString("Agnt_Reference_Id");

                                    checkFetchResponse(Customer_Name, llFetchResponseCustomerName, tvCustomerName);
                                    checkFetchResponse(Bill_Date, llFetchResponseBillDate, tvBillDate);
                                    checkFetchResponse(Bill_Amount, llFetchResponseBillAmount, tvBillAmount);
                                    checkFetchResponse(Bill_Period, llFetchResponseBillPeriod, tvBillPeriod);
                                    checkFetchResponse(Bill_Number, llFetchResponseBillNumber, tvBillNumber);
                                    checkFetchResponse(Due_Date, llFetchResponseDueDate, tvDueDate);
                                    checkFetchResponse(Fixed_Charges, llFetchResponseFixedCharges, tvFixedCharges);
                                    checkFetchResponse(Additional_Charges, llFetchResponseAdditionalCharges, tvAdditionalCharges);
                                    checkFetchResponse(Early_Payment_Fees, llFetchResponseEarlyPaymentFees, tvEarlyPaymentFees);
                                    checkFetchResponse(Late_Payment_Fees, llFetchResponseLatePaymentFees, tvLatePaymentFees);

                                } else {
                                    Toast.makeText(ActivityaElectricityBill.this, GetBillerParameters.popupError(obj), Toast.LENGTH_LONG).show();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            dialog.cancel();
                            try {
                                Toast.makeText(ActivityaElectricityBill.this, GetBillerParameters.popupError(new JSONObject(anError.getErrorBody().toString())), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkFetchResponse(String response, LinearLayout llFetchResponseCustomerName, TextView tvCustomerName) {
        if (response.equals("") || response.equals("NA"))
            llFetchResponseCustomerName.setVisibility(View.GONE);
        else {
            llFetchResponseCustomerName.setVisibility(View.VISIBLE);
            tvCustomerName.setText(response);
        }

    }

    private void payFinalBill(String BillAmount, String blrID, String field26, String field1, String field27, String field28, String field29) {
        ProgressDialog dialog = new ProgressDialog(ActivityaElectricityBill.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();
        try {
            obj.put("payChannel", PAY_CHANNEL);
            obj.put("payMode", PAY_MODE);
            obj.put("blrId", blrID);
            obj.put("refId", Reference_id);
            obj.put("debitNar1", etRemarks.getText().toString().trim());
            obj.put("quickPay", QuickPay);
            obj.put("ccf", ccf);
            obj.put("billAmt", BillAmount);
            obj.put("totalAmt", dTotalAmount);
            obj.put("catName", CatName);
            obj.put("mobileNumber", etPhoneNo.getText().toString().trim());
            obj.put("blrName", BlrName);
            obj.put("fieldValues", GetBillerParameters.getFieldValuesJsonCommonPayBill(blrID, Pincode, PosMobileNumber, LatLong, TerminalID, Keyword, AgentId, AgentReferenceId, field1, field26, field27, field28, field29));

        } catch (Exception e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(URL_PAY_BILL)
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .addHeaders("Authorization", tokenStr)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.cancel();

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String statusCode = obj.getString("statusCode");

                            if (statusCode.equals("0")) {
                                Intent intent = new Intent(ActivityaElectricityBill.this, ActivityReceiptBbps.class);
                                intent.putExtra("Response", response.toString());
                                startActivity(intent);
                                finish();
                            } else {
                                String _pay_bill_error = GetBillerParameters.getPayBillError(new JSONObject(response.toString()));

                                if (_pay_bill_error.equals("TAKE_TO_RECEIPT")){
                                    Intent intent = new Intent(ActivityaElectricityBill.this, ActivityReceiptBbps.class);
                                    intent.putExtra("Response", response.toString());
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(ActivityaElectricityBill.this, _pay_bill_error, Toast.LENGTH_LONG).show();
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        /*try {
                            JSONObject obj = new JSONObject(response.toString());
                            String statusCode = obj.getString("statusCode");
                            String statusDescription = obj.getString("statusDescription");

                            if (statusCode.equals("0")) {
                                String commonClassObject = obj.getString("commonClassObject");
                                JSONObject objCommonClass = new JSONObject(commonClassObject);
                                String res = "";

                                Toast.makeText(ActivityaElectricityBill.this, "" + statusDescription, Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(ActivityaElectricityBill.this, MainActivity.class);
                                startActivity(intent);
                                finish();

                            } else {
                                Toast.makeText(ActivityaElectricityBill.this, GetBillerParameters.popupError(obj), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.cancel();

                        try {
                            String _pay_bill_error = GetBillerParameters.getPayBillError(new JSONObject(anError.getErrorBody().toString()));

                            if (_pay_bill_error.equals("TAKE_TO_RECEIPT")){
                                Intent intent = new Intent(ActivityaElectricityBill.this, ActivityReceiptBbps.class);
                                intent.putExtra("Response", anError.getErrorBody().toString());
                                startActivity(intent);
                                finish();
                            } else {
                                Toast.makeText(ActivityaElectricityBill.this, _pay_bill_error, Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        /*try {
                            Toast.makeText(ActivityaElectricityBill.this, GetBillerParameters.popupError(new JSONObject(anError.getErrorBody().toString())), Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/

                    }

                });

    }

    private void getCCF(String SelectedPaymentMode, String BillAmount) {
        String url = "https://indusindtest.iserveu.online/BBPS/bbps/getCCf";

        JSONObject obj = new JSONObject();
        try {
            obj.put("billAmt", BillAmount);
            obj.put("blrId", blrID);
            obj.put("payChannel", PAY_CHANNEL);
            obj.put("payMode", SelectedPaymentMode);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .addHeaders("Authorization", tokenStr)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String statusCode = obj.getString("statusCode");
                                String statusDescription = obj.getString("statusDescription");

                                if (statusCode.equals("0")) {
                                    String commonClassObject = obj.getString("commonClassObject");
                                    JSONObject objCommonClass = new JSONObject(commonClassObject);
                                    ccf = objCommonClass.getString("ccf");
                                    etCCF.setText(ccf);
                                    dTotalAmount = Double.parseDouble(Bill_Amount) + Double.parseDouble(ccf);
                                    etTotalAmount.setText("" + dTotalAmount);
                                    Log.e("Total Amount: ", "" + dTotalAmount);
                                    Log.e("CCF: ", ccf);

                                } else {
                                    Toast.makeText(ActivityaElectricityBill.this, GetBillerParameters.popupError(obj), Toast.LENGTH_LONG).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            JSONObject obj = null;
                            try {
                                Toast.makeText(ActivityaElectricityBill.this, GetBillerParameters.popupError(new JSONObject(anError.getErrorBody().toString())), Toast.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void buttonVisibilityAmountEditableOperation(String billAcceptanceType, String adhocPayment) {

        if (billAcceptanceType.equals("0")) {
            //Fetch Bill active
            QuickPay = "0";

            llBtnFetchPay.setVisibility(View.VISIBLE);
            btnFetchBill.setVisibility(View.VISIBLE);
            btnPayBill.setVisibility(View.GONE);
        } else if (billAcceptanceType.equals("1")) {
            //Pay Bill active
            QuickPay = "1";

            llBtnFetchPay.setVisibility(View.VISIBLE);
            llMainThree.setVisibility(View.VISIBLE);
            btnFetchBill.setVisibility(View.GONE);
            btnPayBill.setVisibility(View.VISIBLE);
        } else if (billAcceptanceType.equals("2")) {
            //Both active

            llBtnFetchPay.setVisibility(View.VISIBLE);
            llMainThree.setVisibility(View.VISIBLE);
            btnFetchBill.setVisibility(View.VISIBLE);
            btnPayBill.setVisibility(View.VISIBLE);
        }

        if (adhocPayment.equals("0")) {
            setEdittextEditable(etOptionAmount, false, R.drawable.disable_edittext);
        } else if (adhocPayment.equals("1")) {
            setEdittextEditable(etOptionAmount, true, R.drawable.border_white_bg);
        }

    }

    private void resetAllFields() {
        llMainThree.setVisibility(View.GONE);
        cvMainContentTwo.setVisibility(View.GONE);
        llBtnFetchPay.setVisibility(View.GONE);

        llConsumerNo.setVisibility(View.GONE);
        llMobileNo.setVisibility(View.GONE);
        llOption3.setVisibility(View.GONE);
        llOption4.setVisibility(View.GONE);

        tvSampleBill.setVisibility(View.GONE);
        etConsumerNo.setText("");
        etPhoneNo.setText("");
        etOption3.setText("");

        etOptionAmount.setText("");
        etTotalAmount.setText("");
        etCCF.setText("");
        etRemarks.setText("");

        //reset bill details(Fetch Bill data)
        tvCustomerName.setText("");
        tvBillDate.setText("");
        tvBillAmount.setText("");
        tvBillPeriod.setText("");
        tvBillNumber.setText("");
        tvDueDate.setText("");
        tvFixedCharges.setText("");
        tvAdditionalCharges.setText("");
        tvEarlyPaymentFees.setText("");
        tvLatePaymentFees.setText("");

        setEdittextEditable(etCCF, true, R.drawable.border_white_bg);
        setEdittextEditable(etTotalAmount, true, R.drawable.border_white_bg);

        setupPaymentMode();

    }

    public void setState(String state_id, String state_nm) {

        Electricity_State_Model electricity_state_model = new Electricity_State_Model();
        electricity_state_model.setState_id(state_id);
        electricity_state_model.setState_nm(state_nm);

        state_models_arr.add(electricity_state_model);

    }

    public void setBoard(String board_id, String board_name, HashMap<String, Object> other_parameters) {

        BillerListPojo billerListPojo = new BillerListPojo();
        billerListPojo.setBlrId(board_id);
        billerListPojo.setBlrName(board_name);
        billerListPojo.setOther_parameters(other_parameters);

        biller_item_arr.add(billerListPojo);

        Log.d(TAG, "" + board_id + "" + board_name);
    }

    public void setEdittextEditable(EditText et, Boolean editable, int drawable) {
        et.setEnabled(editable);
        et.setBackgroundResource(drawable);
        et.setPadding(12, 0, 12, 0);
    }

    private void setupPaymentMode() {
        selected_payment_type = "";

        final List<String> PaymentList = new ArrayList<>(Arrays.asList(Const.arrPaymentType));
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, PaymentList) {
            @Override
            public boolean isEnabled(int position) {
                if (position != 1) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if (position == 1) {
                    // Set the disable item text color
                    tv.setTextColor(Color.BLACK);
                } else {
                    tv.setTextColor(Color.GRAY);
                }
                return view;
            }
        };

        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerPaymentMode.setAdapter(spinnerArrayAdapter);
        spinnerArrayAdapter.notifyDataSetChanged();


        spinnerPaymentMode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                selected_payment_type = PaymentList.get(position);
                Bill_Amount = etOptionAmount.getText().toString().trim();
                if (position == 1) {
                    if (etOptionAmount.getText().toString().trim().equals("") || etOptionAmount.getText().toString().trim().equals("0")
                            || etOptionAmount.getText().toString().trim().equals(null)) {
                        Toast.makeText(ActivityaElectricityBill.this, "Please enter amount.", Toast.LENGTH_SHORT).show();
                    } else
                        getCCF(selected_payment_type, Bill_Amount);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void field26Validation(EditText etField26, String blrID) {
        int MAX_LENGTH = 0;

        switch (blrID) {
            case "NBPDCL000BHI01":
            case "SBPDCL000BHI01":
            case "WBSEDCL00WBL01":
            case "CESC00000KOL01":
            case "DDED00000DAD01":
            case "WESCO0000ODI01":
            case "KESCO0000UTP01":
            case "UPPCL0000UTP02":
            case "APDCL0000ASM02":
            case "APDCL0000ASM01":
            case "UGVCL0000GUJ01":
            case "DGVCL0000GUJ01":
            case "MGVCL0000GUJ01":
            case "PGVCL0000GUJ01":
            case "GED000000GOA01":
            case "SPDCLOB00ANP01":
            case "CSPDCL000CHH01":
            case "JUSC00000JAM01":
            case "TNEB00000TND01":
            case "RELI00000MUM03":
            case "TATAPWR00MUM01":
            case "SKPR00000SIK02":
            case "SKPR00000SIK01":
            case "HPSEB0000HIP02":
            case "MPDC00000MEG01":
            case "AVVNL0000RAJ01":
            case "BESLOB000RAJ02":
            case "BKESL0000RAJ02":
            case "JDVVNL000RAJ01":
            case "KEDLOB000RAJ02":
            case "BESCOM000KAR01":
            case "HESCOM000KAR01":
            case "MPEZ00000MAP02":
            case "MPCZ00000MAP02":
            case "MPPO00000MAP0Y":
            case "KSEBL0000KER01":
                etField26.setInputType(InputType.TYPE_CLASS_NUMBER);
                break;

            case "GOVE00000PUDN0 ":
            case "PSPCL0000PUN01 ":
            case "CESU00000ODI01 ":
            case "NESCO0000ODI01 ":
            case "SOUTHCO00ODI01 ":
            case "UPCL00000UTT01 ":
            case "UPPCL0000UTP01 ":
            case "EPDCLOB00ANP01 ":
            case "JBVNL0000JHA01 ":
            case "PEDM00000MIZ01 ":
            case "TSEC00000TRI01 ":
            case "MAHA00000MAH01 ":
            case "BEST00000MUM01 ":
            case "DOPN00000NAG01 ":
            case "JVVNL0000RAJ01 ":
            case "ELEC00000CHA3L ":
            case "MESCOM000KAR01 ":
            case "CESCOM000KAR01 ":
            case "GESCOM000KAR01 ":
            case "MPCZ00000MAP01 ":
            case "MPEZ00000MAP01 ":
            case "MPPK00000MAP01 ":
            case "UHBVN0000HAR01 ":
            case "DHBVN0000HAR01 ":
                etField26.setInputType(InputType.TYPE_CLASS_TEXT);
                break;

            default:
                etField26.setInputType(InputType.TYPE_CLASS_TEXT);
                break;

        }


        switch (blrID) {

            case "DDED00000DAD01":
                MAX_LENGTH = 6;
                break;
            case "HESCOM000KAR01":
                MAX_LENGTH = 7;
                break;
            case "WBSEDCL00WBL01":
            case "RELI00000MUM03":
                MAX_LENGTH = 9;
                break;
            case "BESCOM000KAR01":
            case "JUSC00000JAM01":
            case "MPPO00000MAP0Y":
            case "BEST00000MUM01":
            case "MESCOM000KAR01":
            case "CESCOM000KAR01":
            case "GESCOM000KAR01":
                MAX_LENGTH = 10;
                break;
            case "CESC00000KOL01":
            case "APDCL0000ASM01":
            case "UGVCL0000GUJ01":
            case "DGVCL0000GUJ01":
            case "MGVCL0000GUJ01":
            case "PGVCL0000GUJ01":
            case "GED000000GOA01":
                MAX_LENGTH = 11;
                break;
            case "SKPR00000SIK02":
            case "SKPR00000SIK01":
            case "TSEC00000TRI01":
            case "MPDC00000MEG01":
            case "SBPDCL000BHI01":
            case "NBPDCL000BHI01":
            case "TNEB00000TND01":
            case "GOVE00000PUDN0":
            case "DHBVN0000HAR01":
            case "PSPCL0000PUN01":
            case "PEDM00000MIZ01":
            case "UHBVN0000HAR01":
            case "WESCO0000ODI01":
            case "UPPCL0000UTP02":
            case "APDCL0000ASM02":
            case "TATAPWR00MUM01":
            case "HPSEB0000HIP02":
            case "AVVNL0000RAJ01":
            case "NESCO0000ODI01":
            case "SOUTHCO00ODI01":
            case "JVVNL0000RAJ01":
            case "CESU00000ODI01":
                MAX_LENGTH = 12;
                break;

            case "KSEBL0000KER01":
            case "UPCL00000UTT01":
            case "SPDCLOB00ANP01":
                MAX_LENGTH = 13;
                break;
            case "JBVNL0000JHA01":
            case "MPCZ00000MAP02":
            case "KESCO0000UTP01":
                MAX_LENGTH = 15;
                break;
            case "CSPDCL000CHH01":
            case "MPCZ00000MAP01":
            case "ELEC00000CHA3L":
            case "EPDCLOB00ANP01":
            case "UPPCL0000UTP01":
                MAX_LENGTH = 20;
                break;
            case "MPEZ00000MAP02":
                MAX_LENGTH = 25;
                break;
            case "BESLOB000RAJ02":
            case "BKESL0000RAJ02":
            case "JDVVNL000RAJ01":
            case "KEDLOB000RAJ02":
            case "MAHA00000MAH01":
            case "DOPN00000NAG01":
            case "MPEZ00000MAP01":
            case "MPPK00000MAP01":
                MAX_LENGTH = 30;
                break;

            default:
                MAX_LENGTH = 30;
        }

        etField26.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_LENGTH)});

    }

    private void field27Validation(EditText etField27, String blrID) {
        etField27.setInputType(InputType.TYPE_CLASS_TEXT);
        etField27.setFilters(new InputFilter[]{new InputFilter.LengthFilter(30)});
    }
}
