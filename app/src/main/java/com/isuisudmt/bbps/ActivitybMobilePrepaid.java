package com.isuisudmt.bbps;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isuisudmt.Constants;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.bbps.utils.BillerListPojo;
import com.isuisudmt.bbps.utils.Const;
import com.isuisudmt.bbps.utils.Electricity_State_Model;
import com.isuisudmt.bbps.utils.GetBillerParameters;
import com.isuisudmt.bbps.utils.Operator_model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.isuisudmt.bbps.utils.Const.BBPS_PREF;
import static com.isuisudmt.bbps.utils.Const.DEFAULT_BILLER_COLLECTION;
import static com.isuisudmt.bbps.utils.Const.MOBILE_POSTPAID;
import static com.isuisudmt.bbps.utils.Const.PAY_CHANNEL;
import static com.isuisudmt.bbps.utils.Const.PAY_MODE;
import static com.isuisudmt.bbps.utils.Const.PREFIX_DEFAULT_BILLER_DOCUMENT;
import static com.isuisudmt.bbps.utils.Const.SF_AGENT_ID;
import static com.isuisudmt.bbps.utils.Const.SF_KEYWORD;
import static com.isuisudmt.bbps.utils.Const.SF_LAT_LONG;
import static com.isuisudmt.bbps.utils.Const.SF_MOBILE_NUMBER;
import static com.isuisudmt.bbps.utils.Const.SF_PINCODE;
import static com.isuisudmt.bbps.utils.Const.SF_TERMINAL_ID;
import static com.isuisudmt.bbps.utils.Const.URL_FETCH_BILL;
import static com.isuisudmt.bbps.utils.Const.URL_GET_CCF;
import static com.isuisudmt.bbps.utils.Const.URL_PAY_BILL;
import static com.isuisudmt.bbps.utils.Const.URL_PAY_BILL_PREPAID;

public class ActivitybMobilePrepaid extends AppCompatActivity {

    Spinner spinnerOperator;
    EditText mobile_number, amount;
    TextView tvField26, tvField1;
    LinearLayout llMainContentOne, llField26, llField1;

    ImageView ivOperator, ivPaymentType;
    ProgressBar pbOperator, pbPaymentType;

    //Part2
    CardView cvMainContentTwo;
    TextView tvCustomerName, tvBillDate, tvBillAmount, tvBillPeriod, tvBillNumber, tvDueDate, tvFixedCharges, tvAdditionalCharges,
            tvEarlyPaymentFees, tvLatePaymentFees, tvModifySearch;
    Button btnProceedToPay;
    LinearLayout llFetchResponseCustomerName, llFetchResponseBillDate, llFetchResponseBillAmount, llFetchResponseBillPeriod,
            llFetchResponseBillNumber, llFetchResponseDueDate, llFetchResponseFixedCharges, llFetchResponseAdditionalCharges,
            llFetchResponseEarlyPaymentFees, llFetchResponseLatePaymentFees;


    //Part3
    EditText etOptionAmount, etCCF, etTotalAmount, etRemarks;
    Spinner spinnerPaymentMode;
    LinearLayout llMainThree;


    //Buttons
    LinearLayout llBtnFetchPay;
    Button btnPayBill, btnFetchBill;

    String _userName;
    String BlrName = "", CatName = "", Bill_Amount = "", Reference_id = "", selected_type = "", AdhocPayment = "", selected_payment_type = "", AgentReferenceId = "", tokenStr = "", ccf = "", QuickPay = "", TextField1 = "";
    Boolean IS_BILL_FETCHED = false;
    Double dTotalAmount = 0.0;

    SessionManager session;
    String operator_code = "";
    Map<String, Object> otherParameters;
    ArrayList<Operator_model> operator_models_arr;
    ArrayAdapter operator_arr_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activityb_mobile_prepaid);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Mobile Prepaid");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        session = new SessionManager(ActivitybMobilePrepaid.this);
        HashMap<String, String> _user = session.getUserDetails();
        tokenStr = _user.get(SessionManager.KEY_TOKEN);
        HashMap<String, String> user = session.getUserSession();
        _userName = user.get(session.userName);

        llMainContentOne = (LinearLayout) findViewById(R.id.ll_main_content_one);

        spinnerOperator = (Spinner) findViewById(R.id.operator_spinner);
        mobile_number = (EditText) findViewById(R.id.mobile_number);
        amount = (EditText) findViewById(R.id.et_amount);

        TextView tvBoardOrOperator = (TextView) findViewById(R.id.tv_board_operator);
        tvBoardOrOperator.setText("Operator");

        btnPayBill = findViewById(R.id.btn_paybill);

        operator_models_arr = new ArrayList<>();

        setOperator("CODE", "Please select operator");
        setOperator("AR", "AIRTEL");
        setOperator("BS", "BSNL");
        setOperator("ID", "IDEA");
        setOperator("VF", "VODAFONE");
        setOperator("RJ", "RELIANCE JIO");


        operator_arr_adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, operator_models_arr);
        spinnerOperator.setAdapter(operator_arr_adapter);


        spinnerOperator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (operator_models_arr.get(position).code.equalsIgnoreCase("CODE")) {

                    operator_code = "";

                } else {
                    operator_code = operator_models_arr.get(position).code;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnPayBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IS_BILL_FETCHED == true)
                    QuickPay = "0";
                else
                    QuickPay = "1";

                String _postpaid_mobile_no = mobile_number.getText().toString().trim();
                String billAmount = amount.getText().toString().trim();
                //dTotalAmount = Double.parseDouble(billAmount);

                double dTotalAmount1;

                try {
                    dTotalAmount1 = new Double(billAmount);
                } catch (NumberFormatException e) {
                    dTotalAmount1 = 0; // your default value
                }

                int checkWalletBalance = Double.compare(Const.Wallet2Amount, dTotalAmount1);

                if(operator_code.equals(""))
                    Toast.makeText(ActivitybMobilePrepaid.this, "Please Select Valid operator", Toast.LENGTH_SHORT).show();
                else if (_postpaid_mobile_no.equals(""))
                    Toast.makeText(ActivitybMobilePrepaid.this, "Please enter Mobile Number" + TextField1, Toast.LENGTH_SHORT).show();
                else if (billAmount.equals(""))
                    Toast.makeText(ActivitybMobilePrepaid.this, "Please enter Amount", Toast.LENGTH_SHORT).show();
                else if (checkWalletBalance < 0)
                    Toast.makeText(ActivitybMobilePrepaid.this, Const.ERROR_MSG_LOW_BALANCE_WALLET_2, Toast.LENGTH_SHORT).show();
                else {
                    payFinalBill(_postpaid_mobile_no,billAmount, operator_code);
                }

            }
        });



    }

    public void setOperator(String code, String operator) {

        Operator_model operator_model = new Operator_model();
        operator_model.setCode(code);
        operator_model.setName(operator);
        operator_models_arr.add(operator_model);

    }


    private void payFinalBill(String mobile_number,String amount,String operator_code) {
        ProgressDialog dialog = new ProgressDialog(ActivitybMobilePrepaid.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        JSONObject obj = new JSONObject();
        try {
            obj.put("user_name",_userName);
            obj.put("MobileNumber",mobile_number);
            obj.put("SERCODE", operator_code);
            obj.put("AMT",amount );
            obj.put("STV", "0");

        } catch (Exception e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(URL_PAY_BILL_PREPAID)
                .setPriority(Priority.HIGH)
                .addJSONObjectBody(obj)
                .addHeaders("Authorization", tokenStr)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.cancel();

                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String statusCode = obj.getString("status");
                            String statusMsg = obj.getString("message");

                            if (statusCode.equals("0")) {
                                Intent intent = new Intent(ActivitybMobilePrepaid.this, ActivityReceiptBbps2.class);
                                intent.putExtra("Response", response.toString());
                                startActivity(intent);
                                finish();
                            } else {

                                Toast.makeText(ActivitybMobilePrepaid.this, msg(obj.toString()), Toast.LENGTH_LONG).show();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.cancel();

                        Toast.makeText(ActivitybMobilePrepaid.this, msg(anError.getErrorBody().toString()), Toast.LENGTH_LONG).show();

                    }

                });

    }

    private String msg(String response) {
        String errormsg = "";
        try {
            JSONObject obj = new JSONObject(response.toString());
            if(obj.getString("status").equals("-1")){
                errormsg = obj.optString("message");
                errormsg = new JSONArray(obj.optString("errors")).getJSONObject(0).optString("msg");

               /* JSONArray objarray = new JSONArray(obj.optString("errors"));
                JSONObject objMsg = new JSONObject(objarray.toString());
                errormsg = objMsg.optString("msg");*/
            }
            else {
                errormsg = obj.optString("message");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
           return errormsg;
        }


}