package com.isuisudmt.bbps.utils;

import android.content.Context;
import android.widget.Toast;

import com.isuisudmt.bbps.ActivitydBroadbandPostpaid;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.isuisudmt.bbps.utils.Const.BBPS_PREF;
import static com.isuisudmt.bbps.utils.Const.SF_LAT_LONG;
import static com.isuisudmt.bbps.utils.Const.SF_MOBILE_NUMBER;
import static com.isuisudmt.bbps.utils.Const.SF_PINCODE;
import static com.isuisudmt.bbps.utils.Const.SF_TERMINAL_ID;
import static com.isuisudmt.utils.Constants.PAY_CHANNEL;

public class GetBillerParameters {

    public static JSONObject getFieldValuesJsonCommonPayBill(String BLR_ID, String Pincode, String PosMobileNumber, String LatLong, String TerminalID, String Keyword, String AgentId, String AgentReferenceId, String Field1, String Field26, String Field27, String Field28, String Field29) {
        JSONObject objFieldValues = new JSONObject();
        try {

            objFieldValues.put("Field19", Pincode);
            objFieldValues.put("Field17", PosMobileNumber);
            objFieldValues.put("Field18", LatLong);
            objFieldValues.put("Field16", TerminalID);
            objFieldValues.put("BlrId", BLR_ID);
            objFieldValues.put("Keyword", Keyword);
            objFieldValues.put("AgntId", AgentId);
            objFieldValues.put("PayChannel", PAY_CHANNEL);
            objFieldValues.put("AgntRefId", AgentReferenceId);
            objFieldValues.put("Field1", Field1); //Payee Mobile Number
            objFieldValues.put("Field26", Field26); //Postpaid Mobile Number
            objFieldValues.put("Field27", Field27);
            objFieldValues.put("Field28", Field28);
            objFieldValues.put("Field29", Field29);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return objFieldValues;

    }

    public static JSONObject getFieldValuesJsonCommonFetchBill(Context context, String Field1, String Field26, String Field27, String Field28, String Field29) {
        JSONObject objFieldValues = new JSONObject();
        try {

            objFieldValues.put("Field19", context.getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE).getString(SF_PINCODE, ""));
            objFieldValues.put("Field17", context.getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE).getString(SF_MOBILE_NUMBER, ""));
            objFieldValues.put("Field18", context.getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE).getString(SF_LAT_LONG, ""));
            objFieldValues.put("Field16", context.getSharedPreferences(BBPS_PREF, Context.MODE_PRIVATE).getString(SF_TERMINAL_ID, ""));
            objFieldValues.put("Field26", Field26);
            objFieldValues.put("Field1", Field1);
            objFieldValues.put("Field27", Field27);
            objFieldValues.put("Field28", Field28);
            objFieldValues.put("Field29", Field29);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return objFieldValues;

    }

    public static String popupError(JSONObject obj) {
        String ERROR_MSG = "";
        String commonClassObject = "", statusDescription = "";

        try {

            commonClassObject = obj.optString("commonClassObject");
            statusDescription = obj.optString("statusDescription");


            if (commonClassObject.equals("null")|| commonClassObject.equals(null)|| commonClassObject.equals("")){
                ERROR_MSG = statusDescription;
            } else {
                JSONObject jsonObjectCommonClassObject = new JSONObject(commonClassObject);
                String msg = jsonObjectCommonClassObject.optString("msg");
                String errors = jsonObjectCommonClassObject.optString("errors");

                if (errors.equals("")) {
                    ERROR_MSG = msg;
                } else {
                    //Currently showing first error msg, Show all errors
                    JSONArray jsonArray = new JSONArray(errors);
                    String err_msg = jsonArray.getJSONObject(0).optString("err_msg");
                    ERROR_MSG = err_msg;
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return ERROR_MSG;
    }

    public static String getFetchBillError(JSONObject obj){
        String ERROR_MSG = "";
        String statusCode = "", commonClassObject = "", statusDescription = "";

        try {
            statusCode = obj.optString("statusCode");
            commonClassObject = obj.optString("commonClassObject");
            statusDescription = obj.optString("statusDescription");

            if(statusCode.equals("-1")){
                if(!commonClassObject.equals("")){
                    String errors = new JSONObject(commonClassObject).optString("errors");
                    if(!errors.equals("")){
                        String err_type = new JSONArray(errors).getJSONObject(0).optString("err_type");
                        if(err_type.equals("1")){
                            ERROR_MSG = new JSONArray(errors).getJSONObject(0).optString("err_msg");
                        }else if(err_type.equals("0")){
                            ERROR_MSG = "Cannot fetch the bill amount , coz" + statusDescription;
                        }else{
                            ERROR_MSG = statusDescription;
                        }
                    }else{
                        ERROR_MSG = statusDescription;
                    }
                }else{
                    ERROR_MSG = statusDescription;
                }
            }else{
                ERROR_MSG = "Failed! Please Try Again";
            }

        } catch (Exception e){
            e.printStackTrace();
        }

        return ERROR_MSG;

    }

    public static String getPayBillError(JSONObject obj) {
        String ERROR_MSG = "";
        String statusCode = "", commonClassObject = "", statusDescription = "";

        try {
            statusCode = obj.optString("statusCode");
            commonClassObject = obj.optString("commonClassObject");
            statusDescription = obj.optString("statusDescription");

            if(statusCode.equals("-1")){
                if(!commonClassObject.equals("")){
                    String errors = new JSONObject(commonClassObject).optString("errors");
                    String msg = new JSONObject(commonClassObject).optString("msg");

                    if(!errors.equals("")){
                        String err_type = new JSONArray(errors).getJSONObject(0).optString("err_type");
                        if(err_type.equals("1")){
                            ERROR_MSG = new JSONArray(errors).getJSONObject(0).optString("err_msg");
                        }else if(err_type.equals("0")){
                            ERROR_MSG = "Cannot pay the bill amount , coz"+statusDescription;
                        }
                    }else{
                        //Take to receipt
                        ERROR_MSG = "TAKE_TO_RECEIPT";
                    }
                }else{
                    ERROR_MSG = statusDescription;
                }
            }else{
                ERROR_MSG = "Failed! Please Try Again";
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return ERROR_MSG;
    }



    //BILLERID -ELECTRICITY
    public static String BLR_ID_ELECTRICITY_1 = "WESCO0000ODI01";//ODI
    public static String BLR_ID_ELECTRICITY_2 = "CESU00000ODI01";//ODI
    public static String BLR_ID_ELECTRICITY_3 = "NESCO0000ODI01";//ODI
    public static String BLR_ID_ELECTRICITY_4 = "SOUTHCO00ODI01";//ODI
    public static String BLR_ID_ELECTRICITY_5 = "EPDCLOB00ANP01";//ANP
    public static String BLR_ID_ELECTRICITY_6 = "SPDCLOB00ANP01";//ANP
    public static String BLR_ID_ELECTRICITY_7 = "APDCL0000ASM02";//ASM
    public static String BLR_ID_ELECTRICITY_8 = "APDCL0000ASM01";//ASM
    public static String BLR_ID_ELECTRICITY_9 = "NBPDCL000BHI01";//BIH
    public static String BLR_ID_ELECTRICITY_10 = "SBPDCL000BHI01";//BIH
    public static String BLR_ID_ELECTRICITY_11 = "ELEC00000CHA3L";//CHA
    public static String BLR_ID_ELECTRICITY_12 = "CSPDCL000CHH01";//CHH
    public static String BLR_ID_ELECTRICITY_13 = "DDED00000DAD01";//DAD
    public static String BLR_ID_ELECTRICITY_14 = "BSESRAJPLDEL01";//DEL
    public static String BLR_ID_ELECTRICITY_15 = "BSESYAMPLDEL01";//DEL
    public static String BLR_ID_ELECTRICITY_16 = "NDMC00000DEL02";//DEL
    public static String BLR_ID_ELECTRICITY_17 = "TATAPWR00DEL01";//DEL
    public static String BLR_ID_ELECTRICITY_19 = "GED000000GOA01";//GOA
    public static String BLR_ID_ELECTRICITY_20 = "UGVCL0000GUJ01";//GUJ
    public static String BLR_ID_ELECTRICITY_21 = "DGVCL0000GUJ01";//GUJ
    public static String BLR_ID_ELECTRICITY_22 = "MGVCL0000GUJ01";//GUJ
    public static String BLR_ID_ELECTRICITY_23 = "PGVCL0000GUJ01";//GUJ
    public static String BLR_ID_ELECTRICITY_24 = "UHBVN0000HAR01";//HAR
    public static String BLR_ID_ELECTRICITY_25 = "DHBVN0000HAR01";//HAR
    public static String BLR_ID_ELECTRICITY_26 = "HPSEB0000HIP02";//HIP
    public static String BLR_ID_ELECTRICITY_27 = "JBVNL0000JHA01";//JHA
    public static String BLR_ID_ELECTRICITY_62 = "JUSC00000JAM01";//JHA
    public static String BLR_ID_ELECTRICITY_28 = "MESCOM000KAR01";//KAR
    public static String BLR_ID_ELECTRICITY_29 = "BESCOM000KAR01";//KAR
    public static String BLR_ID_ELECTRICITY_30 = "CESCOM000KAR01";//KAR
    public static String BLR_ID_ELECTRICITY_31 = "GESCOM000KAR01";//KAR
    public static String BLR_ID_ELECTRICITY_32 = "HESCOM000KAR01";//KAR
    public static String BLR_ID_ELECTRICITY_33 = "KSEBL0000KER01";//KER
    public static String BLR_ID_ELECTRICITY_34 = "MAHA00000MAH01";//MAH
    public static String BLR_ID_ELECTRICITY_63 = "RELI00000MUM03";//MAH
    public static String BLR_ID_ELECTRICITY_64 = "BEST00000MUM01";//MAH
    public static String BLR_ID_ELECTRICITY_65 = "TATAPWR00MUM01";//MAH
    public static String BLR_ID_ELECTRICITY_35 = "MPEZ00000MAP02";//MAP
    public static String BLR_ID_ELECTRICITY_36 = "MPCZ00000MAP01";//MAP
    public static String BLR_ID_ELECTRICITY_37 = "MPCZ00000MAP02";//MAP
    public static String BLR_ID_ELECTRICITY_38 = "MPEZ00000MAP01";//MAP
    public static String BLR_ID_ELECTRICITY_39 = "MPPK00000MAP01";//MAP
    public static String BLR_ID_ELECTRICITY_40 = "MPPO00000MAP0Y";//MAP
    public static String BLR_ID_ELECTRICITY_41 = "MPDC00000MEG01";//MEG
    public static String BLR_ID_ELECTRICITY_42 = "PEDM00000MIZ01";//MIZ
    public static String BLR_ID_ELECTRICITY_43 = "DOPN00000NAG01";//NAG
    public static String BLR_ID_ELECTRICITY_44 = "GOVE00000PUDN0";//PUD
    public static String BLR_ID_ELECTRICITY_45 = "PSPCL0000PUN01";//PUN
    public static String BLR_ID_ELECTRICITY_46 = "JVVNL0000RAJ01";//RAJ
    public static String BLR_ID_ELECTRICITY_47 = "AVVNL0000RAJ01";//RAJ
    public static String BLR_ID_ELECTRICITY_48 = "BESLOB000RAJ02";//RAJ
    public static String BLR_ID_ELECTRICITY_49 = "BKESL0000RAJ02";//RAJ
    public static String BLR_ID_ELECTRICITY_50 = "JDVVNL000RAJ01";//RAJ
    public static String BLR_ID_ELECTRICITY_51 = "KEDLOB000RAJ02";//RAJ
    public static String BLR_ID_ELECTRICITY_52 = "SKPR00000SIK02";//SIK
    public static String BLR_ID_ELECTRICITY_53 = "SKPR00000SIK01";//SIK
    public static String BLR_ID_ELECTRICITY_54 = "TNEB00000TND01";//TND
    public static String BLR_ID_ELECTRICITY_55 = "TSEC00000TRI01";//TRI
    public static String BLR_ID_ELECTRICITY_56 = "KESCO0000UTP01";//UTP
    public static String BLR_ID_ELECTRICITY_57 = "UPPCL0000UTP01";//UTP
    public static String BLR_ID_ELECTRICITY_58 = "UPPCL0000UTP02";//UTP
    public static String BLR_ID_ELECTRICITY_59 = "UPCL00000UTT01";//UTT
    public static String BLR_ID_ELECTRICITY_60 = "WBSEDCL00WBL01";//WBL
    public static String BLR_ID_ELECTRICITY_61 = "CESC00000KOL01";//WBL


    //BILLERID -MOBILE POSTPAID
    public static String BLR_ID_MOBILE_POSTPAID_1 = "ATPOST000NAT01";
    public static String BLR_ID_MOBILE_POSTPAID_2 = "ATPOST000NAT02";
    public static String BLR_ID_MOBILE_POSTPAID_3 = "BSNLMOB00NAT01";
    public static String BLR_ID_MOBILE_POSTPAID_4 = "IDEA00000NAT01";
    public static String BLR_ID_MOBILE_POSTPAID_5 = "JIO000000NAT01";
    public static String BLR_ID_MOBILE_POSTPAID_6 = "JIO000000NAT02";
    public static String BLR_ID_MOBILE_POSTPAID_7 = "TATADCDMANAT01";
    public static String BLR_ID_MOBILE_POSTPAID_8 = "TATADGSM0NAT01";
    public static String BLR_ID_MOBILE_POSTPAID_9 = "VODA00000NAT96";
    public static String BLR_ID_MOBILE_POSTPAID_10 = "VODA00000NATVA";
    public static String BLR_ID_MOBILE_POSTPAID_11 = "MTNL00000MAH8E";


    //BILLERID -DTH
    public static String BLR_ID_DTH_1 = "DISH00000NAT01";
    public static String BLR_ID_DTH_2 = "TATASKY00NAT01";
    public static String BLR_ID_DTH_3 = "VIDEOCON0NAT01";


    //BILLERID -BROADBAND POSTPAID
    public static String BLR_ID_BROADBAND_POSTPAID_1 = "ACT000000NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_2 = "ATBROAD00NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_3 = "ATBROAD00NAT02";
    public static String BLR_ID_BROADBAND_POSTPAID_4 = "COMWBB000NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_5 = "CONBB0000PUN01";
    public static String BLR_ID_BROADBAND_POSTPAID_6 = "DENB00000NATIO";
    public static String BLR_ID_BROADBAND_POSTPAID_7 = "DVOIS0000NAT02";
    public static String BLR_ID_BROADBAND_POSTPAID_8 = "FLAS00000NATVZ";
    public static String BLR_ID_BROADBAND_POSTPAID_9 = "FUSNBB000NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_10 = "HATHWAY00NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_11 = "INST00000BIHKL";
    public static String BLR_ID_BROADBAND_POSTPAID_12 = "INST00000CHHZV";
    public static String BLR_ID_BROADBAND_POSTPAID_13 = "MNET00000ASM5W";
    public static String BLR_ID_BROADBAND_POSTPAID_14 = "NETP00000PUNS8";
    public static String BLR_ID_BROADBAND_POSTPAID_15 = "NEXTRA000NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_16 = "SPENET000NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_17 = "TIKO00000NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_18 = "TIMB00000NATRQ";
    public static String BLR_ID_BROADBAND_POSTPAID_19 = "TTN000000NAT01";
    public static String BLR_ID_BROADBAND_POSTPAID_20 = "VFIB00000NATJJ";
    public static String BLR_ID_BROADBAND_POSTPAID_21 = "EXCE00000NATDP";
    public static String BLR_ID_BROADBAND_POSTPAID_22 = "SWIF00000NATVE";


    //BILLERID -WATER
    public static String BLR_ID_WATER_1 = "BMC000000MAP01";
    public static String BLR_ID_WATER_2 = "BWSSB0000KAR01";
    public static String BLR_ID_WATER_3 = "DELH00000DEL6Q";
    public static String BLR_ID_WATER_4 = "DEPA00000MIZ9U";
    public static String BLR_ID_WATER_5 = "DLJB00000DEL01";
    public static String BLR_ID_WATER_6 = "GMC000000MAP01";
    public static String BLR_ID_WATER_7 = "GWMC00000WGL01";
    public static String BLR_ID_WATER_8 = "HMWSS0000HYD01";
    public static String BLR_ID_WATER_9 = "HUDA00000HAR01";
    public static String BLR_ID_WATER_10 = "IMC000000MAP01";
    public static String BLR_ID_WATER_11 = "JMC000000MAP01";
    public static String BLR_ID_WATER_12 = "KERA00000KERMO";
    public static String BLR_ID_WATER_13 = "MCA000000PUN01";
    public static String BLR_ID_WATER_14 = "MCC000000KAR01";
    public static String BLR_ID_WATER_15 = "MCG000000GUR01";
    public static String BLR_ID_WATER_16 = "MCJ000000PUN01";
    public static String BLR_ID_WATER_17 = "MCL000000PUN01";
    public static String BLR_ID_WATER_18 = "MUNI00000CHANI";
    public static String BLR_ID_WATER_19 = "NDMC00000DEL01";
    public static String BLR_ID_WATER_20 = "PMC000000PUN01";
    public static String BLR_ID_WATER_21 = "PUNE00000MAHSE";
    public static String BLR_ID_WATER_22 = "RMC000000JHA01";
    public static String BLR_ID_WATER_23 = "SMC000000DNH01";
    public static String BLR_ID_WATER_24 = "SMC000000GUJ01";
    public static String BLR_ID_WATER_25 = "UITWOB000BHW02";
    public static String BLR_ID_WATER_26 = "UJS000000UTT01";
    public static String BLR_ID_WATER_27 = "UNN000000MAP01";
    public static String BLR_ID_WATER_28 = "VASA00000THAE9";
    public static String BLR_ID_WATER_29 = "PORT00000ANI1K";
    public static String BLR_ID_WATER_30 = "JALK00000UTP0P";


    //BILLERID -LANDLINE_POSTPAID
    public static String BLR_ID_LANDLINE_POSTPAID_1 = "ATLLI0000NAT01";
    public static String BLR_ID_LANDLINE_POSTPAID_2 = "ATLLI0000NAT02";
    public static String BLR_ID_LANDLINE_POSTPAID_3 = "BSNL00000NAT5C";
    public static String BLR_ID_LANDLINE_POSTPAID_4 = "BSNL00000NATPZ";
    public static String BLR_ID_LANDLINE_POSTPAID_5 = "BSNLLLCORNAT01";
    public static String BLR_ID_LANDLINE_POSTPAID_6 = "BSNLLLINDNAT01";
    public static String BLR_ID_LANDLINE_POSTPAID_7 = "MTNL00000DEL01";
    public static String BLR_ID_LANDLINE_POSTPAID_8 = "MTNL00000MUM01";
    public static String BLR_ID_LANDLINE_POSTPAID_9 = "TATADLLI0NAT01";


    //BILLERID -GAS
    public static String BLR_ID_GAS_1 = "ADAN00000NAT01";
    public static String BLR_ID_GAS_2 = "AGL000000MAP01";
    public static String BLR_ID_GAS_3 = "ASSA00000ASMA5";
    public static String BLR_ID_GAS_4 = "BHAG00000NATBJ";
    public static String BLR_ID_GAS_5 = "CGSM00000GUJ01";
    public static String BLR_ID_GAS_6 = "CUGL00000UTP01";
    public static String BLR_ID_GAS_7 = "GAIL00000IND01";
    public static String BLR_ID_GAS_8 = "GGL000000UTP01";
    public static String BLR_ID_GAS_9 = "GUJGAS000GUJ01";
    public static String BLR_ID_GAS_10 = "HCG000000HAR02";
    public static String BLR_ID_GAS_11 = "IEPL00000GUJ01";
    public static String BLR_ID_GAS_12 = "INDRAPGASDEL02";
    public static String BLR_ID_GAS_13 = "IOAG00000DEL01";
    public static String BLR_ID_GAS_14 = "MAHA00000MUM01";
    public static String BLR_ID_GAS_15 = "MNGL00000MAH02";
    public static String BLR_ID_GAS_16 = "SGL000000GUJ01";
    public static String BLR_ID_GAS_17 = "SITI00000UTP03";
    public static String BLR_ID_GAS_18 = "TNGCLOB00TRI01";
    public static String BLR_ID_GAS_19 = "UCPGPL000MAH01";
    public static String BLR_ID_GAS_20 = "VGL000000GUJ01";
    public static String BLR_ID_GAS_21 = "GAIL00000NATTZ";
    public static String BLR_ID_GAS_22 = "MEGH00000NATLV";


    //BILLERID -EDUCATION_FEES(15)
    public static String BLR_ID_EDUCATION_FEES_1 = "MOUN00000DEL3N";


    //BILLERID -CABLE_TV(13)
    public static String BLR_ID_CABLE_TV_1 = "HATH00000NATRZ";


    //BILLERID -LPG_GAS(12)
    public static String BLR_ID_LPG_GAS_1 = "BHAR00000NATR4";
    public static String BLR_ID_LPG_GAS_2 = "HPCL00000NAT01";
    public static String BLR_ID_LPG_GAS_3 = "INDI00000NATT5";


    //BILLERID -FASTTAG(11)
    public static String BLR_ID_FASTTAG_1 = "AXIS00000NATSN";
    public static String BLR_ID_FASTTAG_2 = "BANK00000NATDH";
    public static String BLR_ID_FASTTAG_3 = "INDI00000NATTR";
    public static String BLR_ID_FASTTAG_4 = "INDU00000NATR2";
    //public static String BLR_ID_FASTTAG_5 = "TOLL00000NAT72";
    public static String BLR_ID_FASTTAG_6 = "EQUI00000NATNF";
    public static String BLR_ID_FASTTAG_7 = "KOTA00000NATJZ";


    //BILLERID -LIFE_INSURANCE(10)
    public static String BLR_ID_LIFE_INSURANCE_1 = "EXID00000NAT25";
    public static String BLR_ID_LIFE_INSURANCE_2 = "FUTU00000NAT09";
    public static String BLR_ID_LIFE_INSURANCE_3 = "HDFC00000NATV4";
    public static String BLR_ID_LIFE_INSURANCE_4 = "IPRU00000NAT01";
    public static String BLR_ID_LIFE_INSURANCE_5 = "PRAM00000NATYI";
    public static String BLR_ID_LIFE_INSURANCE_6 = "RELI00000NAT3O";
    public static String BLR_ID_LIFE_INSURANCE_7 = "SHRI00000NATRI";
    public static String BLR_ID_LIFE_INSURANCE_8 = "STAR00000NATXZ";
    public static String BLR_ID_LIFE_INSURANCE_9 = "TATA00000NATLP";


    //BILLERID -LOAN_REPAYMENT(8)
    public static String BLR_ID_LOAN_REPAYMENT_1 = "AAVA00000NATMF";
    public static String BLR_ID_LOAN_REPAYMENT_2 = "ADIT00000NATRA";
    public static String BLR_ID_LOAN_REPAYMENT_3 = "AVAI00000NAT7J";
    public static String BLR_ID_LOAN_REPAYMENT_4 = "BAJA00000NATV1";
    public static String BLR_ID_LOAN_REPAYMENT_5 = "BFL000000NAT01";
    public static String BLR_ID_LOAN_REPAYMENT_6 = "CAPR00000NATC0";
    public static String BLR_ID_LOAN_REPAYMENT_7 = "CAPR00000NATUB";
    public static String BLR_ID_LOAN_REPAYMENT_8 = "CLIX00000NATST";
    public static String BLR_ID_LOAN_REPAYMENT_9 = "ESSK00000NATFR";
    public static String BLR_ID_LOAN_REPAYMENT_10 = "FAIR00000NAT6Z";
    public static String BLR_ID_LOAN_REPAYMENT_11 = "FLEX00000NATJL";
    public static String BLR_ID_LOAN_REPAYMENT_12 = "HERO00000NAT7F";
    public static String BLR_ID_LOAN_REPAYMENT_13 = "I2IF00000NAT6K";
    public static String BLR_ID_LOAN_REPAYMENT_14 = "IDFC00000NATCK";
    public static String BLR_ID_LOAN_REPAYMENT_15 = "INDI00000NAT2P";
    public static String BLR_ID_LOAN_REPAYMENT_16 = "INDI00000NATYG";
    public static String BLR_ID_LOAN_REPAYMENT_17 = "JANA00000NATO4";
    public static String BLR_ID_LOAN_REPAYMENT_18 = "LAMP00000NAT7E";
    public static String BLR_ID_LOAN_REPAYMENT_19 = "LAND00000NATRD";
    public static String BLR_ID_LOAN_REPAYMENT_20 = "LOKS00000NATC9";
    public static String BLR_ID_LOAN_REPAYMENT_21 = "MANA00000NATWG";
    public static String BLR_ID_LOAN_REPAYMENT_22 = "MOTI00000NATHD";
    public static String BLR_ID_LOAN_REPAYMENT_23 = "PAIS00000NATCV";
    public static String BLR_ID_LOAN_REPAYMENT_24 = "SHRI00000NAT7D";
    public static String BLR_ID_LOAN_REPAYMENT_25 = "SNAP00000NAT61";
    public static String BLR_ID_LOAN_REPAYMENT_26 = "SVAT00000NATUB";
    public static String BLR_ID_LOAN_REPAYMENT_27 = "TATA00000NATGS";
    public static String BLR_ID_LOAN_REPAYMENT_28 = "VART00000NATHC";
    public static String BLR_ID_LOAN_REPAYMENT_29 = "VAST00000NATLW";


    //BILLERID -HEALTH_INSURANCE(14)
    public static String BLR_ID_HEALTH_INSURANCE_1 = "RELI00000NATQ9";


    /*************************************************************************************************************************************/
    /*************************************************************************************************************************************/
    /*************************************************************************************************************************************/
    /*************************************************************************************************************************************/
    /*************************************************************************************************************************************/

    //PARAMS- ELECTRICITY (Total 23)
    public static String PARAM_BLR_ELECTRICITY_1 = "Consumer Number";
    public static String PARAM_BLR_ELECTRICITY_2 = "Consumer ID";
    public static String PARAM_BLR_ELECTRICITY_3 = "Service Number";
    public static String PARAM_BLR_ELECTRICITY_4 = "CA Number";
    public static String PARAM_BLR_ELECTRICITY_5 = "Account No without(/)";
    public static String PARAM_BLR_ELECTRICITY_6 = "Business Partner Number";
    public static String PARAM_BLR_ELECTRICITY_7 = "Account Number";
    public static String PARAM_BLR_ELECTRICITY_8 = "Contract Account No";
    public static String PARAM_BLR_ELECTRICITY_9 = "Account ID";
    public static String PARAM_BLR_ELECTRICITY_10 = "Customer ID / Account ID";
    public static String PARAM_BLR_ELECTRICITY_11 = "Account ID (RAPDRP) OR Consumer Number / Connection ID (Non-RAPDRP)";
    public static String PARAM_BLR_ELECTRICITY_12 = "Consumer Number/IVRS";
    public static String PARAM_BLR_ELECTRICITY_13 = "IVRS";
    public static String PARAM_BLR_ELECTRICITY_14 = "Customer Number";
    public static String PARAM_BLR_ELECTRICITY_15 = "K Number";
    public static String PARAM_BLR_ELECTRICITY_16 = "Contract Acc Number";
    public static String PARAM_BLR_ELECTRICITY_17 = "ACCOUNTNO";
    public static String PARAM_BLR_ELECTRICITY_18 = "Electricity Account No";
    public static String PARAM_BLR_ELECTRICITY_19 = "Customer ID (Not Consumer No)";

    public static String PARAM_BLR_ELECTRICITY_24_FIELD27 = "Customer Mobile Number";
    public static String PARAM_BLR_ELECTRICITY_25_FIELD27 = "Customer Mobile Number";
    public static String PARAM_BLR_ELECTRICITY_27_FIELD27 = "Subdivision Code";
    public static String PARAM_BLR_ELECTRICITY_34_FIELD27 = "Billing Unit(BU)";


    //PARAMS- MOBILE POSTPAID(Total 11)
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_1 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_2 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_3 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_4 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_5 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_6 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_7 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_8 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_9 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_10 = "Mobile Number";
    public static String PARAM_BLR_ID_MOBILE_POSTPAID_11 = "Mobile Number";

    //PARAMS- DTH(3)
    public static String PARAM_BLR_ID_DTH_1 = "Registered Mobile Number / Viewing Card Number";
    public static String PARAM_BLR_ID_DTH_2 = "Subscriber Number";
    public static String PARAM_BLR_ID_DTH_3 = "Customer ID / Registered Telephone No";

    //PARAMS- BROADBAND POSTPAID(Total 23)
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_1 = "Account Number/User Name";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_2 = "Landline Number with STD code";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_3 = "Landline Number with STD code";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_4 = "Customer Id";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_5 = "Directory Number";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_6 = "User Id";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_7 = "customerId";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_8 = "Customer Number";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_9 = "Customer Id";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_10 = "Customer ID";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_11 = "UserId/Mobile Number";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_12 = "Customer Id  /User Name";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_13 = "UserId";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_14 = "Billing Account Number";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_15 = "Consumer ID";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_16 = "CAN/Account ID";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_17 = "Service Id";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_18 = "CustomerId/RMN";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_19 = "User Name";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_20 = "Account No";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_21 = "Customer ID";
    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_22 = "Landline Number";

    public static String PARAM_BLR_ID_BROADBAND_POSTPAID_21_FIELD27 = "Mobile Number";


    //PARAMS- WATER(Total 39)
    //Field 26
    public static String PARAM_BLR_ID_WATER_1 = "Connection ID";
    public static String PARAM_BLR_ID_WATER_2 = "RR Number";
    public static String PARAM_BLR_ID_WATER_3 = "Customer ID";
    public static String PARAM_BLR_ID_WATER_4 = "Consumer Number";
    public static String PARAM_BLR_ID_WATER_5 = "K No";
    public static String PARAM_BLR_ID_WATER_6 = "Connection ID";
    public static String PARAM_BLR_ID_WATER_7 = "CONNECTION ID";
    public static String PARAM_BLR_ID_WATER_8 = "CAN Number";
    public static String PARAM_BLR_ID_WATER_9 = "Site Code";
    public static String PARAM_BLR_ID_WATER_10 = "Service Number";
    public static String PARAM_BLR_ID_WATER_11 = "Service Number";
    public static String PARAM_BLR_ID_WATER_12 = "Consumer ID";
    public static String PARAM_BLR_ID_WATER_13 = "Account No";
    public static String PARAM_BLR_ID_WATER_14 = "Customer_ID";
    public static String PARAM_BLR_ID_WATER_15 = "K No";
    public static String PARAM_BLR_ID_WATER_16 = "Account No";
    public static String PARAM_BLR_ID_WATER_17 = "Email Id";
    public static String PARAM_BLR_ID_WATER_18 = "Account No without(/)";
    public static String PARAM_BLR_ID_WATER_19 = "Consumer Number";
    public static String PARAM_BLR_ID_WATER_20 = "Consumer Number";
    public static String PARAM_BLR_ID_WATER_21 = "Consumer No";
    public static String PARAM_BLR_ID_WATER_22 = "Consumer No";
    public static String PARAM_BLR_ID_WATER_23 = "Form No";
    public static String PARAM_BLR_ID_WATER_24 = "Connection Number";
    public static String PARAM_BLR_ID_WATER_25 = "Customer ID";
    public static String PARAM_BLR_ID_WATER_26 = "Consumer Number (Last 7 Digits)";
    public static String PARAM_BLR_ID_WATER_27 = "Business Partner Number";
    public static String PARAM_BLR_ID_WATER_28 = "Zone ID and Ward ID";
    public static String PARAM_BLR_ID_WATER_29 = "Citizen ID";
    public static String PARAM_BLR_ID_WATER_30 = "Computer Code";

    public static String PARAM_BLR_ID_WATER_9_FIELD27 = "Consumer No";
    public static String PARAM_BLR_ID_WATER_13_FIELD27 = "Consumer Mobile No";
    public static String PARAM_BLR_ID_WATER_16_FIELD27 = "Consumer Mobile No";
    public static String PARAM_BLR_ID_WATER_17_FIELD27 = "Consumer Number";
    public static String PARAM_BLR_ID_WATER_28_FIELD27 = "Connection No";

    public static String PARAM_BLR_ID_WATER_13_FIELD28 = "UID";
    public static String PARAM_BLR_ID_WATER_16_FIELD28 = "Consumer Email ID";
    public static String PARAM_BLR_ID_WATER_17_FIELD28 = "Mobile Number";

    public static String PARAM_BLR_ID_WATER_16_FIELD29 = "UID";


    //PARAMS- LANDLINE_POSTPAID(Total 9)
    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_1 = "Landline Number with STD code";
    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_2 = "Landline Number with STD code";
    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_3 = "Telephone Number";
    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_4 = "Account Number";

    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_7 = "Account Number";
    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_8 = "Telephone Number";
    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_9 = "Landline Number with STD Code (without 0)";


    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_7_FIELD27 = "Telephone Number";//Field27
    public static String PARAM_BLR_ID_LANDLINE_POSTPAID_8_FIELD27 = "Account Number";//Field27


    //PARAMS- GAS(22)
    public static String PARAM_BLR_ID_GAS_1 = "Customer ID";
    public static String PARAM_BLR_ID_GAS_2 = "Customer No";
    public static String PARAM_BLR_ID_GAS_3 = "CustomerID";
    public static String PARAM_BLR_ID_GAS_4 = "CRN No";
    public static String PARAM_BLR_ID_GAS_5 = "Customer Number";
    public static String PARAM_BLR_ID_GAS_6 = "Customer Code / CRN Number";
    public static String PARAM_BLR_ID_GAS_7 = "BP NO";
    public static String PARAM_BLR_ID_GAS_8 = "CRN Number";
    public static String PARAM_BLR_ID_GAS_9 = "Customer ID";
    public static String PARAM_BLR_ID_GAS_10 = "CRN Number";
    public static String PARAM_BLR_ID_GAS_11 = "Customer_ID";
    public static String PARAM_BLR_ID_GAS_12 = "BP Number";
    public static String PARAM_BLR_ID_GAS_13 = "Customer ID";
    public static String PARAM_BLR_ID_GAS_14 = "CA Number";
    public static String PARAM_BLR_ID_GAS_15 = "BP No";
    public static String PARAM_BLR_ID_GAS_16 = "Customer ID";
    public static String PARAM_BLR_ID_GAS_17 = "ARN Number";
    public static String PARAM_BLR_ID_GAS_18 = "Consumer Number";
    public static String PARAM_BLR_ID_GAS_19 = "Customer No";
    public static String PARAM_BLR_ID_GAS_20 = "Consumer Number";
    public static String PARAM_BLR_ID_GAS_21 = "BP No";
    public static String PARAM_BLR_ID_GAS_22 = "CRN NO";


    //PARAMS -EDUCATION_FEES(1)
    public static String PARAM_BLR_ID_EDUCATION_FEES_1 = "Student Id";


    //PARAMS -CABLE_TV(1)
    public static String PARAM_BLR_ID_CABLE_TV_1 = "Account No/ VC No/ MAC ID/ VSC No/ RMN";


    //PARAMS -LPG_GAS(8)
    public static String PARAM_BLR_ID_LPG_GAS_1 = "Registered Contact Number";
    public static String PARAM_BLR_ID_LPG_GAS_2 = "Consumer Number";
    public static String PARAM_BLR_ID_LPG_GAS_3 = "Mobile Number";

    public static String PARAM_BLR_ID_LPG_GAS_1_FIELD_27 = "LPG ID";
    public static String PARAM_BLR_ID_LPG_GAS_2_FIELD_27 = "Distributor ID";
    public static String PARAM_BLR_ID_LPG_GAS_3_FIELD_27 = "Consumer Number";
    public static String PARAM_BLR_ID_LPG_GAS_3_FIELD_28 = "Distributor Code";
    public static String PARAM_BLR_ID_LPG_GAS_3_FIELD_29 = "Consumer ID";


    //PARAMS -FASTTAG(7)
    public static String PARAM_BLR_ID_FASTTAG_1 = "Vehicle Registration Number";
    public static String PARAM_BLR_ID_FASTTAG_2 = "Vehicle Number";
    public static String PARAM_BLR_ID_FASTTAG_3 = "Vehicle Registration Number";
    public static String PARAM_BLR_ID_FASTTAG_4 = "Vehicle Registration Number";
    //public static String PARAM_BLR_ID_FASTTAG_5 = "";
    public static String PARAM_BLR_ID_FASTTAG_6 = "Vehicle Registration Number";
    public static String PARAM_BLR_ID_FASTTAG_7 = "Vehicle Registered Number";

    public static String PARAM_BLR_ID_FASTTAG_1_FIELD_27 = "Amount";


    //PARAMS -LIFE_INSURANCE(16)
    public static String PARAM_BLR_ID_LIFE_INSURANCE_1 = "DOB(YYYYMMDD)";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_2 = "DOB";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_3 = "Policy Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_4 = "Policy Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_5 = "Policy Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_6 = "Policy Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_7 = "Policy Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_8 = "Policy Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_9 = "Policy Number";

    public static String PARAM_BLR_ID_LIFE_INSURANCE_1_FIELD_27 = "POLICY NO";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_2_FIELD_27 = "Policy Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_3_FIELD_27 = "Date Of Birth (DDMMYYYY)";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_7_FIELD_27 = "Date Of Birth (DDMMYYYY)";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_8_FIELD_27 = "Date Of Birth";

    public static String PARAM_BLR_ID_LIFE_INSURANCE_8_FIELD_28 = "Mobile Number";
    public static String PARAM_BLR_ID_LIFE_INSURANCE_8_FIELD_29 = "Email Id";


    //PARAMS -LOAN_REPAYMENT(37)
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_1 = "Loan no";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_2 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_3 = "mobile";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_4 = "Customer ID/Loan Account Number/Mobile Number/Provisional RC No/Registration Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_5 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_6 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_7 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_8 = "loanApplicationNumber";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_9 = "Loan Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_10 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_11 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_12 = "Application ID";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_13 = "Loan ID";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_14 = "Loan No";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_15 = "Loan Agreement No";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_16 = "LanNo";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_17 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_18 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_19 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_20 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_21 = "Agreement Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_22 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_23 = "Load ID";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_24 = "LOAN NUMBER";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_25 = "Registered Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_26 = "Customer ID";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_27 = "Loan Account Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_28 = "Loan Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_29 = "Loan Account No";


    public static String PARAM_BLR_ID_LOAN_REPAYMENT_2_FIELD_27 = "Registered Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_3_FIELD_27 = "DOB";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_10_FIELD_27 = "Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_16_FIELD_27 = "DOB (In dd-mm-yyyy)";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_18_FIELD_27 = "Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_19_FIELD_27 = "Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_24_FIELD_27 = "Mobile Number";
    public static String PARAM_BLR_ID_LOAN_REPAYMENT_27_FIELD_27 = "Mobile Number";


    //PARAMS -HEALTH_INSURANCE(2)
    public static String PARAM_BLR_ID_HEALTH_INSURANCE_1 = "Policy Number";
    public static String PARAM_BLR_ID_HEALTH_INSURANCE_1_FIELD_27 = "DOB(YYYY-MM-DD)";


    /*************************************************************************************************************************************/
    /*************************************************************************************************************************************/
    /*************************************************************************************************************************************/
    /*************************************************************************************************************************************/
    /*************************************************************************************************************************************/

    public static String getParamElectricity(String BLR_ID) {

        if (BLR_ID.equals(BLR_ID_ELECTRICITY_1) || BLR_ID.equals(BLR_ID_ELECTRICITY_3) || BLR_ID.equals(BLR_ID_ELECTRICITY_4) ||
                BLR_ID.equals(BLR_ID_ELECTRICITY_16) || BLR_ID.equals(BLR_ID_ELECTRICITY_20) || BLR_ID.equals(BLR_ID_ELECTRICITY_21) ||
                BLR_ID.equals(BLR_ID_ELECTRICITY_22) || BLR_ID.equals(BLR_ID_ELECTRICITY_23) || BLR_ID.equals(BLR_ID_ELECTRICITY_27) ||
                BLR_ID.equals(BLR_ID_ELECTRICITY_31) || BLR_ID.equals(BLR_ID_ELECTRICITY_33) || BLR_ID.equals(BLR_ID_ELECTRICITY_34) ||
                BLR_ID.equals(BLR_ID_ELECTRICITY_44) || BLR_ID.equals(BLR_ID_ELECTRICITY_54) || BLR_ID.equals(BLR_ID_ELECTRICITY_57) ||
                BLR_ID.equals(BLR_ID_ELECTRICITY_58) || BLR_ID.equals(BLR_ID_ELECTRICITY_63) || BLR_ID.equals(BLR_ID_ELECTRICITY_64) ||
                BLR_ID.equals(BLR_ID_ELECTRICITY_65))
            return PARAM_BLR_ELECTRICITY_1;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_2) || BLR_ID.equals(BLR_ID_ELECTRICITY_7) || BLR_ID.equals(BLR_ID_ELECTRICITY_8) ||
                BLR_ID.equals(BLR_ID_ELECTRICITY_26) || BLR_ID.equals(BLR_ID_ELECTRICITY_41) || BLR_ID.equals(BLR_ID_ELECTRICITY_43) ||
                BLR_ID.equals(BLR_ID_ELECTRICITY_55) || BLR_ID.equals(BLR_ID_ELECTRICITY_60))
            return PARAM_BLR_ELECTRICITY_2;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_5) || BLR_ID.equals(BLR_ID_ELECTRICITY_6))
            return PARAM_BLR_ELECTRICITY_3;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_9) || BLR_ID.equals(BLR_ID_ELECTRICITY_10) || BLR_ID.equals(BLR_ID_ELECTRICITY_14) ||
                BLR_ID.equals(BLR_ID_ELECTRICITY_15) || BLR_ID.equals(BLR_ID_ELECTRICITY_17))
            return PARAM_BLR_ELECTRICITY_4;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_11))
            return PARAM_BLR_ELECTRICITY_5;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_12) || BLR_ID.equals(BLR_ID_ELECTRICITY_62))
            return PARAM_BLR_ELECTRICITY_6;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_13) || BLR_ID.equals(BLR_ID_ELECTRICITY_24) || BLR_ID.equals(BLR_ID_ELECTRICITY_25) ||
                BLR_ID.equals(BLR_ID_ELECTRICITY_45))
            return PARAM_BLR_ELECTRICITY_7;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_19))
            return PARAM_BLR_ELECTRICITY_8;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_28))
            return PARAM_BLR_ELECTRICITY_9;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_29))
            return PARAM_BLR_ELECTRICITY_10;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_30) || BLR_ID.equals(BLR_ID_ELECTRICITY_32))
            return PARAM_BLR_ELECTRICITY_11;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_35) || BLR_ID.equals(BLR_ID_ELECTRICITY_38) || BLR_ID.equals(BLR_ID_ELECTRICITY_40))
            return PARAM_BLR_ELECTRICITY_12;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_36) || BLR_ID.equals(BLR_ID_ELECTRICITY_37))
            return PARAM_BLR_ELECTRICITY_13;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_39) || BLR_ID.equals(BLR_ID_ELECTRICITY_42))
            return PARAM_BLR_ELECTRICITY_14;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_46) || BLR_ID.equals(BLR_ID_ELECTRICITY_47) || BLR_ID.equals(BLR_ID_ELECTRICITY_48) ||
                BLR_ID.equals(BLR_ID_ELECTRICITY_49) || BLR_ID.equals(BLR_ID_ELECTRICITY_50) || BLR_ID.equals(BLR_ID_ELECTRICITY_51))
            return PARAM_BLR_ELECTRICITY_15;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_52) || BLR_ID.equals(BLR_ID_ELECTRICITY_53))
            return PARAM_BLR_ELECTRICITY_16;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_56))
            return PARAM_BLR_ELECTRICITY_17;

        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_59))
            return PARAM_BLR_ELECTRICITY_18;
        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_61))
            return PARAM_BLR_ELECTRICITY_19;

        else
            return "";
    }


    public static String getParamMobilePostpaid(String BLR_ID) {
        if (BLR_ID.equals(BLR_ID_MOBILE_POSTPAID_1))
            return PARAM_BLR_ID_MOBILE_POSTPAID_1;
        else if (BLR_ID.equals(BLR_ID_MOBILE_POSTPAID_2))
            return PARAM_BLR_ID_MOBILE_POSTPAID_2;
        else if (BLR_ID.equals(BLR_ID_MOBILE_POSTPAID_3))
            return PARAM_BLR_ID_MOBILE_POSTPAID_3;
        else if (BLR_ID.equals(BLR_ID_MOBILE_POSTPAID_4))
            return PARAM_BLR_ID_MOBILE_POSTPAID_4;
        else if (BLR_ID.equals(BLR_ID_MOBILE_POSTPAID_5))
            return PARAM_BLR_ID_MOBILE_POSTPAID_5;
        else if (BLR_ID.equals(BLR_ID_MOBILE_POSTPAID_6))
            return PARAM_BLR_ID_MOBILE_POSTPAID_6;
        else if (BLR_ID.equals(BLR_ID_MOBILE_POSTPAID_7))
            return PARAM_BLR_ID_MOBILE_POSTPAID_7;
        else if (BLR_ID.equals(BLR_ID_MOBILE_POSTPAID_8))
            return PARAM_BLR_ID_MOBILE_POSTPAID_8;
        else if (BLR_ID.equals(BLR_ID_MOBILE_POSTPAID_9))
            return PARAM_BLR_ID_MOBILE_POSTPAID_9;
        else if (BLR_ID.equals(BLR_ID_MOBILE_POSTPAID_10))
            return PARAM_BLR_ID_MOBILE_POSTPAID_10;
        else if (BLR_ID.equals(BLR_ID_MOBILE_POSTPAID_11))
            return PARAM_BLR_ID_MOBILE_POSTPAID_11;
        else
            return "";
    }

    public static String getParamDTH(String BLR_ID) {
        if (BLR_ID.equals(BLR_ID_DTH_1))
            return PARAM_BLR_ID_DTH_1;
        else if (BLR_ID.equals(BLR_ID_DTH_2))
            return PARAM_BLR_ID_DTH_2;
        else if (BLR_ID.equals(BLR_ID_DTH_3))
            return PARAM_BLR_ID_DTH_3;
        else
            return "";
    }

    public static String getParamBroadbandPostpaid(String BLR_ID) {

        if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_1))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_1;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_2))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_2;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_3))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_3;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_4))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_4;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_5))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_5;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_6))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_6;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_7))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_7;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_8))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_8;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_9))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_9;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_10))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_10;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_11))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_11;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_12))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_12;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_13))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_13;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_14))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_14;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_15))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_15;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_16))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_16;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_17))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_17;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_18))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_18;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_19))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_19;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_20))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_20;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_21))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_21;
        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_22))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_22;

        else
            return "";
    }

    public static String getParamWater(String BLR_ID) {
        if (BLR_ID.equals(BLR_ID_WATER_1))
            return PARAM_BLR_ID_WATER_1;
        else if (BLR_ID.equals(BLR_ID_WATER_2))
            return PARAM_BLR_ID_WATER_2;
        else if (BLR_ID.equals(BLR_ID_WATER_3))
            return PARAM_BLR_ID_WATER_3;
        else if (BLR_ID.equals(BLR_ID_WATER_4))
            return PARAM_BLR_ID_WATER_4;
        else if (BLR_ID.equals(BLR_ID_WATER_5))
            return PARAM_BLR_ID_WATER_5;
        else if (BLR_ID.equals(BLR_ID_WATER_6))
            return PARAM_BLR_ID_WATER_6;
        else if (BLR_ID.equals(BLR_ID_WATER_7))
            return PARAM_BLR_ID_WATER_7;
        else if (BLR_ID.equals(BLR_ID_WATER_8))
            return PARAM_BLR_ID_WATER_8;
        else if (BLR_ID.equals(BLR_ID_WATER_9))
            return PARAM_BLR_ID_WATER_9;
        else if (BLR_ID.equals(BLR_ID_WATER_10))
            return PARAM_BLR_ID_WATER_10;
        else if (BLR_ID.equals(BLR_ID_WATER_11))
            return PARAM_BLR_ID_WATER_11;
        else if (BLR_ID.equals(BLR_ID_WATER_12))
            return PARAM_BLR_ID_WATER_12;
        else if (BLR_ID.equals(BLR_ID_WATER_13))
            return PARAM_BLR_ID_WATER_13;
        else if (BLR_ID.equals(BLR_ID_WATER_14))
            return PARAM_BLR_ID_WATER_14;
        else if (BLR_ID.equals(BLR_ID_WATER_15))
            return PARAM_BLR_ID_WATER_15;
        else if (BLR_ID.equals(BLR_ID_WATER_16))
            return PARAM_BLR_ID_WATER_16;
        else if (BLR_ID.equals(BLR_ID_WATER_17))
            return PARAM_BLR_ID_WATER_17;
        else if (BLR_ID.equals(BLR_ID_WATER_18))
            return PARAM_BLR_ID_WATER_18;
        else if (BLR_ID.equals(BLR_ID_WATER_19))
            return PARAM_BLR_ID_WATER_19;
        else if (BLR_ID.equals(BLR_ID_WATER_20))
            return PARAM_BLR_ID_WATER_20;
        else if (BLR_ID.equals(BLR_ID_WATER_21))
            return PARAM_BLR_ID_WATER_21;
        else if (BLR_ID.equals(BLR_ID_WATER_22))
            return PARAM_BLR_ID_WATER_22;
        else if (BLR_ID.equals(BLR_ID_WATER_23))
            return PARAM_BLR_ID_WATER_23;
        else if (BLR_ID.equals(BLR_ID_WATER_24))
            return PARAM_BLR_ID_WATER_24;
        else if (BLR_ID.equals(BLR_ID_WATER_25))
            return PARAM_BLR_ID_WATER_25;
        else if (BLR_ID.equals(BLR_ID_WATER_26))
            return PARAM_BLR_ID_WATER_26;
        else if (BLR_ID.equals(BLR_ID_WATER_27))
            return PARAM_BLR_ID_WATER_27;
        else if (BLR_ID.equals(BLR_ID_WATER_28))
            return PARAM_BLR_ID_WATER_28;
        else if (BLR_ID.equals(BLR_ID_WATER_29))
            return PARAM_BLR_ID_WATER_29;
        else if (BLR_ID.equals(BLR_ID_WATER_30))
            return PARAM_BLR_ID_WATER_30;
        else
            return "";
    }

    public static String getParamLandlinePostpaid(String BLR_ID) {
        if (BLR_ID.equals(BLR_ID_LANDLINE_POSTPAID_1))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_1;
        else if (BLR_ID.equals(BLR_ID_LANDLINE_POSTPAID_2))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_2;
        else if (BLR_ID.equals(BLR_ID_LANDLINE_POSTPAID_3))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_3;
        else if (BLR_ID.equals(BLR_ID_LANDLINE_POSTPAID_4))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_4;
        else if (BLR_ID.equals(BLR_ID_LANDLINE_POSTPAID_7))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_7;
        else if (BLR_ID.equals(BLR_ID_LANDLINE_POSTPAID_8))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_8;
        else if (BLR_ID.equals(BLR_ID_LANDLINE_POSTPAID_9))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_9;
        else
            return "";
    }

    public static String getParamGas(String BLR_ID) {
        if (BLR_ID.equals(BLR_ID_GAS_1))
            return PARAM_BLR_ID_GAS_1;
        else if (BLR_ID.equals(BLR_ID_GAS_2))
            return PARAM_BLR_ID_GAS_2;
        else if (BLR_ID.equals(BLR_ID_GAS_3))
            return PARAM_BLR_ID_GAS_3;
        else if (BLR_ID.equals(BLR_ID_GAS_4))
            return PARAM_BLR_ID_GAS_4;
        else if (BLR_ID.equals(BLR_ID_GAS_5))
            return PARAM_BLR_ID_GAS_5;
        else if (BLR_ID.equals(BLR_ID_GAS_6))
            return PARAM_BLR_ID_GAS_6;
        else if (BLR_ID.equals(BLR_ID_GAS_7))
            return PARAM_BLR_ID_GAS_7;
        else if (BLR_ID.equals(BLR_ID_GAS_8))
            return PARAM_BLR_ID_GAS_8;
        else if (BLR_ID.equals(BLR_ID_GAS_9))
            return PARAM_BLR_ID_GAS_9;
        else if (BLR_ID.equals(BLR_ID_GAS_10))
            return PARAM_BLR_ID_GAS_10;
        else if (BLR_ID.equals(BLR_ID_GAS_11))
            return PARAM_BLR_ID_GAS_11;
        else if (BLR_ID.equals(BLR_ID_GAS_12))
            return PARAM_BLR_ID_GAS_12;
        else if (BLR_ID.equals(BLR_ID_GAS_13))
            return PARAM_BLR_ID_GAS_13;
        else if (BLR_ID.equals(BLR_ID_GAS_14))
            return PARAM_BLR_ID_GAS_14;
        else if (BLR_ID.equals(BLR_ID_GAS_15))
            return PARAM_BLR_ID_GAS_15;
        else if (BLR_ID.equals(BLR_ID_GAS_16))
            return PARAM_BLR_ID_GAS_16;
        else if (BLR_ID.equals(BLR_ID_GAS_17))
            return PARAM_BLR_ID_GAS_17;
        else if (BLR_ID.equals(BLR_ID_GAS_18))
            return PARAM_BLR_ID_GAS_18;
        else if (BLR_ID.equals(BLR_ID_GAS_19))
            return PARAM_BLR_ID_GAS_19;
        else if (BLR_ID.equals(BLR_ID_GAS_20))
            return PARAM_BLR_ID_GAS_20;
        else if (BLR_ID.equals(BLR_ID_GAS_21))
            return PARAM_BLR_ID_GAS_21;
        else if (BLR_ID.equals(BLR_ID_GAS_22))
            return PARAM_BLR_ID_GAS_22;
        else
            return "";
    }

    public static String getParamEducationFees(String BLR_ID) {
        if (BLR_ID.equals(BLR_ID_EDUCATION_FEES_1))
            return PARAM_BLR_ID_EDUCATION_FEES_1;
        else
            return "";
    }

    public static String getParamCableTV(String BLR_ID) {
        if (BLR_ID.equals(BLR_ID_CABLE_TV_1))
            return PARAM_BLR_ID_CABLE_TV_1;
        else
            return "";
    }

    public static String getParamLpgGas(String BLR_ID) {
        if (BLR_ID.equals(BLR_ID_LPG_GAS_1))
            return PARAM_BLR_ID_LPG_GAS_1;
        else if (BLR_ID.equals(BLR_ID_LPG_GAS_2))
            return PARAM_BLR_ID_LPG_GAS_2;
        else if (BLR_ID.equals(BLR_ID_LPG_GAS_3))
            return PARAM_BLR_ID_LPG_GAS_3;
        else
            return "";
    }

    public static String getParamFasttag(String BLR_ID) {
        if (BLR_ID.equals(BLR_ID_FASTTAG_1))
            return PARAM_BLR_ID_FASTTAG_1;
        else if (BLR_ID.equals(BLR_ID_FASTTAG_2))
            return PARAM_BLR_ID_FASTTAG_2;
        else if (BLR_ID.equals(BLR_ID_FASTTAG_3))
            return PARAM_BLR_ID_FASTTAG_3;
        else if (BLR_ID.equals(BLR_ID_FASTTAG_4))
            return PARAM_BLR_ID_FASTTAG_4;
        else if (BLR_ID.equals(BLR_ID_FASTTAG_6))
            return PARAM_BLR_ID_FASTTAG_6;
        else if (BLR_ID.equals(BLR_ID_FASTTAG_7))
            return PARAM_BLR_ID_FASTTAG_7;
        else
            return "";
    }

    public static String getParamLifeInsurance(String BLR_ID) {
        if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_1))
            return PARAM_BLR_ID_LIFE_INSURANCE_1;
        else if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_2))
            return PARAM_BLR_ID_LIFE_INSURANCE_2;
        else if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_3))
            return PARAM_BLR_ID_LIFE_INSURANCE_3;
        else if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_4))
            return PARAM_BLR_ID_LIFE_INSURANCE_4;
        else if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_5))
            return PARAM_BLR_ID_LIFE_INSURANCE_5;
        else if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_6))
            return PARAM_BLR_ID_LIFE_INSURANCE_6;
        else if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_7))
            return PARAM_BLR_ID_LIFE_INSURANCE_7;
        else if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_8))
            return PARAM_BLR_ID_LIFE_INSURANCE_8;
        else if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_9))
            return PARAM_BLR_ID_LIFE_INSURANCE_9;
        else
            return "";
    }

    public static String getParamLoanRepayment(String BLR_ID) {
        if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_1))
            return PARAM_BLR_ID_LOAN_REPAYMENT_1;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_2))
            return PARAM_BLR_ID_LOAN_REPAYMENT_2;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_3))
            return PARAM_BLR_ID_LOAN_REPAYMENT_3;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_4))
            return PARAM_BLR_ID_LOAN_REPAYMENT_4;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_5))
            return PARAM_BLR_ID_LOAN_REPAYMENT_5;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_6))
            return PARAM_BLR_ID_LOAN_REPAYMENT_6;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_7))
            return PARAM_BLR_ID_LOAN_REPAYMENT_7;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_8))
            return PARAM_BLR_ID_LOAN_REPAYMENT_8;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_9))
            return PARAM_BLR_ID_LOAN_REPAYMENT_9;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_10))
            return PARAM_BLR_ID_LOAN_REPAYMENT_10;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_11))
            return PARAM_BLR_ID_LOAN_REPAYMENT_11;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_12))
            return PARAM_BLR_ID_LOAN_REPAYMENT_12;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_13))
            return PARAM_BLR_ID_LOAN_REPAYMENT_13;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_14))
            return PARAM_BLR_ID_LOAN_REPAYMENT_14;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_15))
            return PARAM_BLR_ID_LOAN_REPAYMENT_15;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_16))
            return PARAM_BLR_ID_LOAN_REPAYMENT_16;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_17))
            return PARAM_BLR_ID_LOAN_REPAYMENT_17;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_18))
            return PARAM_BLR_ID_LOAN_REPAYMENT_18;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_19))
            return PARAM_BLR_ID_LOAN_REPAYMENT_19;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_20))
            return PARAM_BLR_ID_LOAN_REPAYMENT_20;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_21))
            return PARAM_BLR_ID_LOAN_REPAYMENT_21;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_22))
            return PARAM_BLR_ID_LOAN_REPAYMENT_22;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_23))
            return PARAM_BLR_ID_LOAN_REPAYMENT_23;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_24))
            return PARAM_BLR_ID_LOAN_REPAYMENT_24;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_25))
            return PARAM_BLR_ID_LOAN_REPAYMENT_25;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_26))
            return PARAM_BLR_ID_LOAN_REPAYMENT_26;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_27))
            return PARAM_BLR_ID_LOAN_REPAYMENT_27;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_28))
            return PARAM_BLR_ID_LOAN_REPAYMENT_28;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_29))
            return PARAM_BLR_ID_LOAN_REPAYMENT_29;
        /*else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_30))
            return PARAM_BLR_ID_LOAN_REPAYMENT_30;
        */
        else
            return "";
    }

    public static String getParamHealthInsurance(String BLR_ID) {
        if (BLR_ID.equals(BLR_ID_HEALTH_INSURANCE_1))
            return PARAM_BLR_ID_HEALTH_INSURANCE_1;
        else
            return "";
    }

    public static String getParamCommonField27(String BLR_ID) {
        if (BLR_ID.equals(BLR_ID_ELECTRICITY_24))
            return PARAM_BLR_ELECTRICITY_24_FIELD27;
        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_25))
            return PARAM_BLR_ELECTRICITY_25_FIELD27;
        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_27))
            return PARAM_BLR_ELECTRICITY_27_FIELD27;
        else if (BLR_ID.equals(BLR_ID_ELECTRICITY_34))
            return PARAM_BLR_ELECTRICITY_34_FIELD27;

        if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_2))
            return PARAM_BLR_ID_LOAN_REPAYMENT_2_FIELD_27;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_3))
            return PARAM_BLR_ID_LOAN_REPAYMENT_3_FIELD_27;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_10))
            return PARAM_BLR_ID_LOAN_REPAYMENT_10_FIELD_27;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_16))
            return PARAM_BLR_ID_LOAN_REPAYMENT_16_FIELD_27;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_18))
            return PARAM_BLR_ID_LOAN_REPAYMENT_18_FIELD_27;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_19))
            return PARAM_BLR_ID_LOAN_REPAYMENT_19_FIELD_27;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_24))
            return PARAM_BLR_ID_LOAN_REPAYMENT_24_FIELD_27;
        else if (BLR_ID.equals(BLR_ID_LOAN_REPAYMENT_27))
            return PARAM_BLR_ID_LOAN_REPAYMENT_27_FIELD_27;

        else if (BLR_ID.equals(BLR_ID_HEALTH_INSURANCE_1))
            return PARAM_BLR_ID_HEALTH_INSURANCE_1_FIELD_27;

        else if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_1))
            return PARAM_BLR_ID_LIFE_INSURANCE_1_FIELD_27;
        else if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_2))
            return PARAM_BLR_ID_LIFE_INSURANCE_2_FIELD_27;
        else if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_3))
            return PARAM_BLR_ID_LIFE_INSURANCE_3_FIELD_27;
        else if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_7))
            return PARAM_BLR_ID_LIFE_INSURANCE_7_FIELD_27;
        else if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_8))
            return PARAM_BLR_ID_LIFE_INSURANCE_8_FIELD_27;

        else if (BLR_ID.equals(BLR_ID_FASTTAG_1))
            return PARAM_BLR_ID_FASTTAG_1_FIELD_27;

        else if (BLR_ID.equals(BLR_ID_LPG_GAS_1))
            return PARAM_BLR_ID_LPG_GAS_1_FIELD_27;
        else if (BLR_ID.equals(BLR_ID_LPG_GAS_2))
            return PARAM_BLR_ID_LPG_GAS_2_FIELD_27;
        else if (BLR_ID.equals(BLR_ID_LPG_GAS_3))
            return PARAM_BLR_ID_LPG_GAS_3_FIELD_27;

        else if (BLR_ID.equals(BLR_ID_BROADBAND_POSTPAID_21))
            return PARAM_BLR_ID_BROADBAND_POSTPAID_21_FIELD27;

        else if (BLR_ID.equals(BLR_ID_WATER_9))
            return PARAM_BLR_ID_WATER_9_FIELD27;
        else if (BLR_ID.equals(BLR_ID_WATER_13))
            return PARAM_BLR_ID_WATER_13_FIELD27;
        else if (BLR_ID.equals(BLR_ID_WATER_16))
            return PARAM_BLR_ID_WATER_16_FIELD27;
        else if (BLR_ID.equals(BLR_ID_WATER_17))
            return PARAM_BLR_ID_WATER_17_FIELD27;
        else if (BLR_ID.equals(BLR_ID_WATER_28))
            return PARAM_BLR_ID_WATER_28_FIELD27;

        else if (BLR_ID.equals(BLR_ID_LANDLINE_POSTPAID_7))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_7_FIELD27;
        else if (BLR_ID.equals(BLR_ID_LANDLINE_POSTPAID_8))
            return PARAM_BLR_ID_LANDLINE_POSTPAID_8_FIELD27;

        else
            return "";
    }

    public static String getParamCommonField28(String BLR_ID) {
        if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_8))
            return PARAM_BLR_ID_LIFE_INSURANCE_8_FIELD_28;

        else if (BLR_ID.equals(BLR_ID_LPG_GAS_3))
            return PARAM_BLR_ID_LPG_GAS_3_FIELD_28;

        else if (BLR_ID.equals(BLR_ID_WATER_13))
            return PARAM_BLR_ID_WATER_13_FIELD28;
        else if (BLR_ID.equals(BLR_ID_WATER_16))
            return PARAM_BLR_ID_WATER_16_FIELD28;
        else if (BLR_ID.equals(BLR_ID_WATER_17))
            return PARAM_BLR_ID_WATER_17_FIELD28;

        else
            return "";
    }

    public static String getParamCommonField29(String BLR_ID) {
        if (BLR_ID.equals(BLR_ID_LIFE_INSURANCE_8))
            return PARAM_BLR_ID_LIFE_INSURANCE_8_FIELD_29;

        else if (BLR_ID.equals(BLR_ID_LPG_GAS_3))
            return PARAM_BLR_ID_LPG_GAS_3_FIELD_29;

        else if (BLR_ID.equals(BLR_ID_WATER_16))
            return PARAM_BLR_ID_WATER_16_FIELD29;

        else
            return "";
    }


}

