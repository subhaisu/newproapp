package com.isuisudmt.bbps.utils;


import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;

/**
 * This class represents the constants used in the BBPS.
 *
 * @author Prabhakar Panda
 * @date 08/10/2020.
 */

public class Const {
    /**
     * Base Urls
     */

    public static final String URL_IS_UPDATED = "https://indusindtest.iserveu.online/BBPS/bbps/isUpdated";
    public static final String URL_VIEW_REQUIRED_INFO = "https://indusindtest.iserveu.online/BBPS/bbps/viewRequiredInfo";

    public static final String URL_FETCH_BILL = "https://indusindtest.iserveu.online/BBPS/bbps/fetchBill";
    public static final String URL_PAY_BILL = "https://indusindtest.iserveu.online/BBPS/bbps/payBill";
    public static final String URL_GET_CCF = "https://indusindtest.iserveu.online/BBPS/bbps/getCCf";

    public static final String URL_PAY_BILL_PREPAID = "https://wallet-deduct-recharge-vn3k2k7q7q-uc.a.run.app/walletDeductMobileRecharge";

    public static final String URL_GET_REPORT_BBPS = "https://us-central1-creditapp-29bf2.cloudfunctions.net/new_node_bigquery_report/all_transaction_report";

    public static final String URL_GET_REPORT_PREPAID = "https://us-central1-creditapp-29bf2.cloudfunctions.net/new_node_bigquery_report_staging_temp/all_transaction_report";


    //isUpdated Response
    public static String isUpdatedResponse = "";
    public static String viewRequiredInfoResponse = "";

    public static Double Wallet2Amount = 0.0;


    //ERROR TOAST MSG
    public static String ERROR_MSG_LOW_BALANCE_WALLET_2 = "Wallet2 doesn't have sufficient balance to make this transaction";
    public static String ERROR_MSG_NULL_CUSTOMER_MOBILE_NO = "Please enter Customer Mobile Number";
    public static String ERROR_MSG_INVALID_CUSTOMER_MOBILE_NO = "Please enter valid phone number";

    //Firestore Collection
    public static String DEFAULT_BILLER_COLLECTION = "DefaultBiller_Collection";
    public static String PREFIX_DEFAULT_BILLER_DOCUMENT = "CategoryId_";

    //catId(Except Electricity)
    public static String BROADBAND_POSTPAID = "1";
    public static String DTH = "2";
    public static String ELECTRICITY = "3";
    public static String GAS = "4";
    public static String LANDLINE_POSTPAID = "5";
    public static String MOBILE_POSTPAID = "6";
    public static String WATER = "7";
    public static String LOAN_REPAYMENT = "8";
    public static String LIFE_INSURANCE = "10";
    public static String FASTAG = "11";
    public static String LPG_GAS = "12";
    public static String CABLE_TV = "13";
    public static String HEALTH_INSURANCE = "14";
    public static String EDUCATION_FEES = "15";

    //BBPS Required Parameters
    public static String BBPS_PREF = "bbpsPref" ;
    public static String SF_PINCODE = "pincodeKey";
    public static String SF_MOBILE_NUMBER = "mobileNumberKey";
    public static String SF_LAT_LONG = "latlongKey";
    public static String SF_TERMINAL_ID = "terminalIdKey";
    public static String SF_AGENT_ID = "agentIDKey";
    public static String SF_KEYWORD = "keyboardKey";

    public static String PAY_CHANNEL = "BSC";
    public static String PAY_MODE = "Cash";

    public static String[] arrPaymentType = {"Choose Payment Mode", "Cash", "Credit Card", "Debit Card", "IMPS", "Internet Banking", "NEFT", "Prepaid Card", "UPI", "Wallet"};

}
