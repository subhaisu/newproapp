package com.isuisudmt.bbps.utils;

public class GetBillerSampleImage {

    public static String getBillImage(String blrID) {
        switch (blrID) {

            case "WESCO0000ODI01":
                return "https://storage.googleapis.com/biller/OD-wu.jpg";
            case "MESCOM000KAR01":
                return "https://storage.googleapis.com/biller/KA-mescom1.webp";
            case "KSEBL0000KER01":
                return "https://storage.googleapis.com/biller/KE-kseb.webp";
            case "UPCL00000UTT01":
                return "https://storage.googleapis.com/biller/UT-upcl.webp";
            case "GESCOM000KAR01":
                return "https://storage.googleapis.com/biller/KA-gescom.jpg";
            case "CESC00000KOL01":
                return "https://storage.googleapis.com/biller/WB-CESC.jpg";
            case "MPDC00000MEG01":
                return "https://storage.googleapis.com/biller/MG-mpdcl.jpg";
            case "SOUTHCO00ODI01":
                return "https://storage.googleapis.com/biller/OD-su.jpg";
            case "HESCOM000KAR01":
                return "https://storage.googleapis.com/biller/KA-hescom.jpg";
            case "RELI00000MUM03":
                return "https://storage.googleapis.com/biller/MA-ae.jpg";
            case "AVVNL0000RAJ01":
                return "https://storage.googleapis.com/biller/RA-avvnl.jpg";
            case "JDVVNL000RAJ01":
                return "https://storage.googleapis.com/biller/RA-jdvnl.jpg";
            case "UPPCL0000UTP01":
                return "https://storage.googleapis.com/biller/UP-UPPCL.jpg";
            case "SPDCLOB00ANP01":
                return "https://storage.googleapis.com/biller/AP-spdcl.jpg";
            case "WBSEDCL00WBL01":
                return "https://storage.googleapis.com/biller/WB-WBSEDCL.jpg";
            case "PSPCL0000PUN01":
                return "https://storage.googleapis.com/biller/PU-pspcl.jpg";
            case "APDCL0000ASM01":
                return "https://storage.googleapis.com/biller/AS-apdcl.jpg";
            case "APDCL0000ASM02":
                return "https://storage.googleapis.com/biller/AS-apdcl.jpg";
            case "DGVCL0000GUJ01":
                return "https://storage.googleapis.com/biller/GU-dgvcl.jpg";
            case "DHBVN0000HAR01":
                return "https://storage.googleapis.com/biller/HA-dhbvn.jpg";
            case "JVVNL0000RAJ01":
                return "https://storage.googleapis.com/biller/RA-jvvnl.jpg";
            case "PGVCL0000GUJ01":
                return "https://storage.googleapis.com/biller/GU-pgvcl.jpg";
            case "UGVCL0000GUJ01":
                return "https://storage.googleapis.com/biller/GU-ugvcl.jpg";
            case "EPDCLOB00ANP01":
                return "https://storage.googleapis.com/biller/A-apepdcl.jpg";
            case "CSPDCL000CHH01":
                return "https://storage.googleapis.com/biller/C-cspdcl.jpg";
            case "BEST00000MUM01":
                return "https://storage.googleapis.com/biller/MA-BEST.jpg";
            case "MPPO00000MAP0Y":
                return "https://storage.googleapis.com/biller/MP-pkvb.jpg";
            case "MPEZ00000MAP02":
                return "https://storage.googleapis.com/biller/MP-pkvb.jpg";
            case "MPEZ00000MAP01":
                return "https://storage.googleapis.com/biller/MP-pkvb.jpg";
            case "SBPDCL000BHI01":
                return "https://storage.googleapis.com/biller/BI-sbpd.jpg";
            case "TATAPWR00MUM01":
                return "https://storage.googleapis.com/biller/MA-TataPower.jpg";
            case "BESCOM000KAR01":
                return "https://storage.googleapis.com/biller/KA-bescom.jpg";
            case "JUSC00000JAM01":
                return "https://storage.googleapis.com/biller/JH-jusco.jpg";
            case "NESCO0000ODI01":
                return "https://storage.googleapis.com/biller/OD-nu.jpg";
            case "BESLOB000RAJ02":
                return "https://storage.googleapis.com/biller/RA-besl.jpg";
            case "KEDLOB000RAJ02":
                return "https://storage.googleapis.com/biller/RA-kedl.jpg";
            case "NBPDCL000BHI01":
                return "https://storage.googleapis.com/biller/BI-nbpd.jpg";
            case "MPPK00000MAP01":
                return "https://storage.googleapis.com/biller/MP-pkvvcl.jpg";


            default:
                return "";
        }
    }
}
