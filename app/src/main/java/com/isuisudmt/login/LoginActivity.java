package com.isuisudmt.login;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isuisudmt.Constants;
import com.isuisudmt.MainActivity;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.SplashActivity;
import com.isuisudmt.mpin.AuthMPINActivity;
import com.isuisudmt.password.ForgotPasswordActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;
import static com.isuisudmt.utils.Constants.APP_UNIQUE_ID;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    TextInputEditText user_name, password;
    Button submit;
    SessionManager session;
    String _user_name, _password;
    ProgressBar progressBar;
    LoginPresenter loginPresenter;
    TextView forgotpassword;

    SharedPreferences sp;
    public static final String ISU_PREF = "isuPref" ;
    public static final String USER_NAME = "userNameKey";
    public static final String USER_MPIN = "mpinKey";

    Boolean CrashTest = false;//Will be true when setting up crashlytics
    Boolean isIserveU = true;//Will be true while releasing apps for iserveu

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        user_name = findViewById(R.id.user_name);
        password = findViewById(R.id.password);
        submit = findViewById(R.id.submit);
        progressBar = findViewById(R.id.progressBar);
        forgotpassword = findViewById(R.id.forgotPassword);

        sp = getSharedPreferences(ISU_PREF, Context.MODE_PRIVATE);

        loginPresenter = new LoginPresenter(this);

        forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);

            }
        });

        // Session Manager
        session = new SessionManager(getApplicationContext());

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //throw new RuntimeException("This is a crash");


               /* _user_name = user_name.getText().toString().trim();
                _password = password.getText().toString().trim();

                if(_user_name.length()!=0 && _password.length()!=0) {
                    progressBar.setVisibility(View.VISIBLE);
                    //loginPresenter.getV1Response("https://itpl.iserveu.tech/generate/v1/");

                    //checkSessionExistance(_user_name);

                    loginPresenter.getV1Response("https://itpl.iserveu.tech/generate/v1/");

                }else {
                    Toast.makeText(LoginActivity.this, "Please check login credential", Toast.LENGTH_LONG).show();
                }*/



                //new add code

                if (CrashTest == true) {
                    throw new RuntimeException("This is a crash test");
                } else {
                    _user_name = user_name.getText().toString().trim();
                    _password = password.getText().toString().trim();
                    if (_user_name.length() != 0 && _password.length() != 0) {
                        progressBar.setVisibility(View.VISIBLE);
                        //loginPresenter.getV1Response("https://itpl.iserveu.tech/generate/v1/");
                        //checkSessionExistance(_user_name);
                        loginPresenter.getV1Response("https://itpl.iserveu.tech/generate/v1/");
                    } else {
                        Toast.makeText(LoginActivity.this, "Please check login credential", Toast.LENGTH_LONG).show();
                    }
                }


            }
        });
    }

   /* private void loadLogin(String base64) {

        Log.i("base64 :url ::: ", base64);

        JSONObject js = new JSONObject();
        try {
            js.put("username", _user_name);
            js.put("password", _password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Make request for JSONObject
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.POST, base64, js,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i("response :: ", "" + response);
                            String _token = response.getString("token");
                            String _admin = response.getString("adminName");

                            // Use user real data
                            session.createLoginSession(_token, _admin);

                            progressBar.setVisibility(View.GONE);

                            Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(i);
                            finish();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("error", "Error: " + error.getMessage());
            }
        }) {

            *//**
             * Passing some request headers
             *//*
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };

        // Adding request to request queue
        Volley.newRequestQueue(this).add(jsonObjReq);


    }*/



    private void loadLogin(String base64) {
        JSONObject obj = new JSONObject();
        try {
            obj.put("username", _user_name);
            obj.put("password", _password);
            AndroidNetworking.post(base64)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String _token = obj.getString("token");
                                String _admin = obj.getString("adminName");

                                Log.e(TAG, "Token: " + _token);

                                // Use user real data
                                session.createLoginSession(_token, _admin);
                                progressBar.setVisibility(View.INVISIBLE);
                                String app_admin_name = getString(R.string.app_admin_name);

                                Constants.USER_NAME = _user_name;
                               /* //Note When not building for IserveU use this condition
                                if(!_admin.equalsIgnoreCase(_user_name) && _admin.equalsIgnoreCase(app_admin_name)) {

                                //Note When building for IserveU use this condition


                          *//*       if (_admin.equalsIgnoreCase("demoisu")
                                         || _admin.equalsIgnoreCase("mposadmin")
                                         || _admin.equalsIgnoreCase("Ucashnew")
                                         || _admin.equalsIgnoreCase("dpay")
                                         || _admin.equalsIgnoreCase("habizw103")
                                         || _admin.equalsIgnoreCase("atharvw82")
                                         || _admin.equalsIgnoreCase("indcashw167")
                                         || _admin.equalsIgnoreCase("ucashapinew")
                                         || _admin.equalsIgnoreCase("techadmin")

                                 ) {*//*


*//*

                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(i);
                                    finish();
*//*

                                    String oldUser = sp.getString(USER_NAME, "");

                                    if (oldUser != null && oldUser.equalsIgnoreCase(_user_name)){
                                        String mpin = sp.getString(USER_MPIN, "");
                                        if (mpin == null || mpin.length() == 0){
                                            Intent i = new Intent(getApplicationContext(), AuthMPINActivity.class);
                                            i.putExtra("token", _token);
                                            startActivity(i);
                                            finish();
                                        } else {
                                            loginWithMpin(_token, mpin);
                                        }
                                    } else {
                                        SharedPreferences.Editor editor = sp.edit();
                                        editor.putString(USER_NAME, _user_name);
                                        editor.apply();

                                        Intent i = new Intent(getApplicationContext(), AuthMPINActivity.class);
                                        i.putExtra("token", _token);
                                        startActivity(i);
                                        finish();
                                    }

                                }else {
                                    showSessionAlert("Sorry !!! you cannot use application as admin or different admin user, Please check login details or contact our help desk for more information.");
                                    // Toast.makeText(LoginActivity.this, "Sorry !!! you cannot use application as admin or different admin user, Please check login details or contact our help desk for more information", Toast.LENGTH_LONG).show();
                                }*/

                                if (isIserveU == true) {
                                    if (_admin.equalsIgnoreCase("demoisu")
                                            || _admin.equalsIgnoreCase("mposadmin")
                                            || _admin.equalsIgnoreCase("Ucashnew")
                                            || _admin.equalsIgnoreCase("dpay")
                                            || _admin.equalsIgnoreCase("habizw103")
                                            || _admin.equalsIgnoreCase("atharvw82")
                                            || _admin.equalsIgnoreCase("indcashw167")
                                            || _admin.equalsIgnoreCase("ucashapinew")
                                            || _admin.equalsIgnoreCase("techadmin")
                                    ) {
                                        String oldUser = sp.getString(USER_NAME, "");
                                        if (oldUser != null && oldUser.equalsIgnoreCase(_user_name)) {
                                            String mpin = sp.getString(USER_MPIN, "");
                                            if (mpin == null || mpin.length() == 0) {
                                                Intent i = new Intent(getApplicationContext(), AuthMPINActivity.class);
                                                i.putExtra("token", _token);
                                                startActivity(i);
                                                finish();
                                            } else {
                                                loginWithMpin(_token, mpin);
                                            }
                                        } else {
                                            SharedPreferences.Editor editor = sp.edit();
                                            editor.putString(USER_NAME, _user_name);
                                            editor.apply();
                                            Intent i = new Intent(getApplicationContext(), AuthMPINActivity.class);
                                            i.putExtra("token", _token);
                                            startActivity(i);
                                            finish();
                                        }
                                    } else {
                                        showSessionAlert("Sorry !!! you cannot use application as admin or different admin user, Please check login details or contact our help desk for more information.");
                                        // Toast.makeText(LoginActivity.this, "Sorry !!! you cannot use application as admin or different admin user, Please check login details or contact our help desk for more information", Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    if (!_admin.equalsIgnoreCase(_user_name) && _admin.equalsIgnoreCase(app_admin_name)) {
                                        String oldUser = sp.getString(USER_NAME, "");
                                        if (oldUser != null && oldUser.equalsIgnoreCase(_user_name)) {
                                            String mpin = sp.getString(USER_MPIN, "");
                                            if (mpin == null || mpin.length() == 0) {
                                                Intent i = new Intent(getApplicationContext(), AuthMPINActivity.class);
                                                i.putExtra("token", _token);
                                                startActivity(i);
                                                finish();
                                            } else {
                                                loginWithMpin(_token, mpin);
                                            }
                                        } else {
                                            SharedPreferences.Editor editor = sp.edit();
                                            editor.putString(USER_NAME, _user_name);
                                            editor.apply();
                                            Intent i = new Intent(getApplicationContext(), AuthMPINActivity.class);
                                            i.putExtra("token", _token);
                                            startActivity(i);
                                            finish();
                                        }
                                    } else {
                                        showSessionAlert("Sorry !!! you cannot use application as admin or different admin user, Please check login details or contact our help desk for more information.");
                                        // Toast.makeText(LoginActivity.this, "Sorry !!! you cannot use application as admin or different admin user, Please check login details or contact our help desk for more information", Toast.LENGTH_LONG).show();
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            progressBar.setVisibility(View.INVISIBLE);
                            Toast.makeText(LoginActivity.this, "Incorrect login and password", Toast.LENGTH_LONG).show();

                        }
                    });
        }catch ( Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public void fetchedV1Response(boolean status, String response) {
        if (response != null) {
            loadLogin(response);
        }
    }

   /* private String getDeviceIP(){
        try {
            WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
            return Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        }catch (Exception e){
            e.printStackTrace();
        }

    }*/

    private void loginWithMpin(String uN, String pin) {

        ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        String url = "https://us-central1-creditapp-29bf2.cloudfunctions.net/user_mpin/login";
        JSONObject obj = new JSONObject();

        try {
            obj.put("token", uN);
            obj.put("m_pin", pin);

            AndroidNetworking.post(url)
                    .setPriority(Priority.HIGH)
                    .addJSONObjectBody(obj)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                int status = response.getInt("status");
                                String message = response.getString("message");

                                if (status == 1){
                                    //load main activity
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString(USER_MPIN, pin);
                                    editor.apply();

                                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(i);
                                    finish();
                                }
                                dialog.dismiss();
                            } catch (JSONException e) {
                                e.printStackTrace();
                                dialog.dismiss();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            try {

                                // 0 mismatch
                                // -1 expired
                                // 1 success
                                JSONObject errorObject = new JSONObject(anError.getErrorBody());

                                int status = errorObject.getInt("status");

                                if (status == 0){
                                    String message = "MPIN has been changed.\nEnter the correct one or regenerate.";
                                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
                                } else if (status == -1){
                                    String message = "MPIN has been expired.\nRegenrated the MPIN.";
                                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
                                }


                                Intent i = new Intent(LoginActivity.this, AuthMPINActivity.class);
                                i.putExtra("token", uN);
                                startActivity(i);
                                finish();

                                SharedPreferences.Editor editor = sp.edit();
                                editor.putString(USER_MPIN, "");
                                editor.apply();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            dialog.dismiss();
                        }
                    });


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private String getDeviceID(){
        return Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }


    private void checkSessionExistance(String user_name){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents

        DocumentReference docRef = db.collection("CoreApp_Session_Manager").document(user_name);
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if(document.contains("login_status")) {

                                String value = document.getData().get("login_status").toString();

                                if(value.equalsIgnoreCase("false")){
                                     updateSession(user_name);

                                }else{
                                    showSessionAlert(""); // Toast.makeText(SplashActivity.this, "", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    } else {
                        updateSession(user_name);
                    }
                } else {

                }
            }
        });
    }

    public void showSessionAlert(String message){
        try {
            AlertDialog.Builder alertbuilderupdate;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                alertbuilderupdate = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                alertbuilderupdate = new AlertDialog.Builder(this);
            }
            alertbuilderupdate.setCancelable(false);
//            String message = "Session is already running !!! Please login after sometimes.";
            alertbuilderupdate.setTitle("Alert")
                    .setMessage(message)
                    .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();
                            finish();
                        }
                    });
            AlertDialog alert11 = alertbuilderupdate.create();
            alert11.show();
        }catch (Exception e){

        }
    }

    private void updateSession(String user_name){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> map = new HashMap<>();
        map.put("app_unique_id", getPackageName());
        map.put("device_id", getDeviceID());
        map.put("login_datetime", System.currentTimeMillis());
        map.put("login_status", true);
            db.collection("CoreApp_Session_Manager").document(user_name)
                    .set(map)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d(TAG, "DocumentSnapshot successfully written!");
                            loginPresenter.getV1Response("https://itpl.iserveu.tech/generate/v1/");

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w(TAG, "Error writing document", e);
                            loginPresenter.getV1Response("https://itpl.iserveu.tech/generate/v1/");

                        }
                    });
        }

}
