package com.isuisudmt.cashoutbank;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.isuisudmt.Constants;
import com.isuisudmt.MainActivity;
import com.isuisudmt.R;
import com.isuisudmt.SessionManager;
import com.isuisudmt.Util;
import com.isuisudmt.cahoutwallet.WalletCashoutActivity;
import com.isuisudmt.login.LoginActivity;
import com.moos.library.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import static com.isuisudmt.BuildConfig.GET_USER_ROLE;
import static com.isuisudmt.Constants.session_time;

public class WalletToBankActivity extends AppCompatActivity {

    Button addAccount;
    SessionManager session;
    String token,admin;
    TextView accNo,amontplacehoder;
    ProgressDialog pd;
    EditText addAmountEdit;
    Button transferAmount;

    String accNoStr="",ifscStr="",bankNameStr="",bankCodeStr="";
    ImageView back;
    TextView wal1_bal_edt;
    ImageView img_prof,imgInfo;
    Handler timer_handler;
    String _userName;
    boolean session_logout = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_tobank);

        session = new SessionManager(getApplicationContext());
        HashMap<String, String> user = session.getUserDetails();
        token = user.get(SessionManager.KEY_TOKEN);
        admin = user.get(SessionManager.KEY_ADMIN);

        HashMap<String, String> user_ = session.getUserSession();
        _userName = user_.get(session.userName);

        accNo = findViewById(R.id.acc_no);
        addAccount = findViewById(R.id.addAccount);
        addAmountEdit = findViewById(R.id.addAmountEdit);
        transferAmount = findViewById(R.id.transferAmount);
        wal1_bal_edt = findViewById(R.id.wal1_bal_edt);
        img_prof = findViewById(R.id.img_prof);
        imgInfo = findViewById(R.id.imgInfo);
        amontplacehoder = findViewById(R.id.amontplacehoder);
        imgInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Util.showAlert(WalletToBankActivity.this,"Alert","Sorry, Account No. is not active.");
            }
        });

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        pd = new ProgressDialog(WalletToBankActivity.this);
        addAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAPI();
            }
        });

        transferAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addAmountEdit.getText().toString().length()>0) {
                    callTransferAmount();
                }else{
                    Toast.makeText(WalletToBankActivity.this,"Please enter amount to transfer.",Toast.LENGTH_SHORT).show();
                }

            }
        });


        checkBankDetail();
    }

    public void callTransferAmount() {
        pd.setMessage("Loading..");
        pd.setCancelable(false);
        pd.show();
        try {
            AndroidNetworking.get("https://apps.iserveu.tech/generate/v79")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----"+key);
                                byte[] data = Base64.decode(key,Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");
                                callEncriptedTransferAmount(encodedUrl,pd);

                            } catch (JSONException e) {
//                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
//                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            pd.dismiss();
                        }
                    });

        }catch (Exception e){
//            e.printStackTrace();
        }
    }


    private void callEncriptedTransferAmount(String encodedUrl,ProgressDialog pd) {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("amount",addAmountEdit.getText().toString());
            AndroidNetworking.post(encodedUrl)
                    .addHeaders("Authorization",token)
                    .addJSONObjectBody(jsonObject)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                pd.dismiss();
                                JSONObject obj = new JSONObject(response.toString());
                                String statusDesc = obj.getString("statusDesc");

                                showAlertt(WalletToBankActivity.this,"Alert",statusDesc);
                                addAmountEdit.setText("");
                                Util.hideKeyboard(WalletToBankActivity.this,addAmountEdit);


                                /*{"status":0,"statusDesc":"Transaction Successfull","txnId":109204,"bankRespCode":"00","bankMessage":"Transaction successfull"}*/



                            } catch (JSONException e) {
//                                e.printStackTrace();
                                pd.dismiss();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            String str = anError.getErrorBody();
                            try {
                                JSONObject errorObj = new JSONObject(str);
                                String statusDesc = errorObj.getString("statusDesc");

                                Util.showAlert(WalletToBankActivity.this,"Alert",statusDesc);
                                pd.dismiss();
                            } catch (JSONException e) {
//                                e.printStackTrace();
                            }
//                            pd.dismiss();


                        }
                    });

        }catch (Exception e){
//            e.printStackTrace();
        }


    }

    private void callAPI() {
        View _view = getLayoutInflater().inflate(R.layout.add_bankdetail_bottom_sheet, null);
        BottomSheetDialog dialog = new BottomSheetDialog(WalletToBankActivity.this);
        dialog.setContentView(_view);

        Button saveBtn = _view.findViewById(R.id.saveBtn);
        EditText bankName = _view.findViewById(R.id.accNo);
        EditText accNoEdit = _view.findViewById(R.id.accNo);
        EditText ifscCode = _view.findViewById(R.id.ifscCode);
        EditText bankCode = _view.findViewById(R.id.bankCode);
        ProgressBar progbar = _view.findViewById(R.id.progbar);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bankNameStr = bankName.getText().toString();
                String accNoStr = accNoEdit.getText().toString();
                String ifscCodeStr = ifscCode.getText().toString();
                String bankCodeStr = bankCode.getText().toString();

                SaveBankDetail(dialog,progbar,bankNameStr,accNoStr,ifscCodeStr,bankCodeStr);
            }
        });




        dialog.show();



    }

    private void SaveBankDetail(BottomSheetDialog dialog,ProgressBar progressBar, String bankNameStr, String accNoStr, String ifscCodeStr,String bankCodeStrr) {
        progressBar.setVisibility(View.VISIBLE);

        AndroidNetworking.get("https://itpl.iserveu.tech/generate/v70")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject obj = new JSONObject(response.toString());
                            String key = obj.getString("hello");
                            System.out.println(">>>>-----"+key);
                            byte[] data = Base64.decode(key,Base64.DEFAULT);
                            String encodedUrl = new String(data, "UTF-8");

                            EncriptedSaveBankDetail(encodedUrl,dialog,progressBar,bankNameStr,accNoStr,ifscCodeStr,bankCodeStrr);

                        } catch (JSONException e) {
//                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
//                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressBar.setVisibility(View.GONE);
                    }
                });

    }


    private void EncriptedSaveBankDetail(String encodedUrl,BottomSheetDialog dialog,ProgressBar progressBar, String bankNameStr, String accNoStr, String ifscCodeStr,String bankCodeStrr) {
        //progressBar.setVisibility(View.VISIBLE);
        //String  token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTg4MTQwMDM2MzA2LCJleHAiOjE1ODgxNDE4MzZ9.cpx9v6wrBYokW8LBkO7et4FdrSbYbxLcTN30n59vDsQMVpFj0yixpvVBoBowPq25Q20MMz05s-wKqfUO9jdYiQ";

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("accountNumber",accNoStr);
            jsonObject.put("ifsc",ifscCodeStr);
            jsonObject.put("bankName",bankNameStr);
            jsonObject.put("bankCode",bankCodeStrr);

            AndroidNetworking.post(encodedUrl)
                    .addHeaders("Authorization",token)
                    .addJSONObjectBody(jsonObject)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String active = obj.getString("active");
                                dialog.dismiss();
                                progressBar.setVisibility(View.GONE);
                                if(active.equalsIgnoreCase("false")){
                                    String message = obj.getString("message");
                                    showAlertOnBack(WalletToBankActivity.this,"Alert",message);
                                    addAmountEdit.setVisibility(View.GONE);
                                    transferAmount.setVisibility(View.GONE);

                                }else{
                                    String accountNo = obj.getString("accountNumber");
                                    accNo.setText("Acc No: "+accountNo);
                                    addAmountEdit.setVisibility(View.VISIBLE);
                                    transferAmount.setVisibility(View.VISIBLE);
                                    addAccount.setVisibility(View.GONE);

                                }

                            } catch (JSONException e) {
//                                e.printStackTrace();
                                progressBar.setVisibility(View.GONE);

                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            String str = anError.getErrorBody();
                            progressBar.setVisibility(View.GONE);

                        }
                    });

        }catch (Exception e){
//            e.printStackTrace();
        }


    }


    public void checkBankDetail() {
        try {
            pd.setMessage("Loading..");
            pd.setCancelable(false);
            pd.show();
            AndroidNetworking.get("https://itpl.iserveu.tech/generate/v68")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----"+key);
                                byte[] data = Base64.decode(key,Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                ShowBankDetails(encodedUrl);

                            } catch (JSONException e) {
//                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
//                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        }catch (Exception e){
//            e.printStackTrace();
        }
    }
    public void ShowBankDetails(String encodeUrl) {

        //String  token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpdHBsIiwiYXVkaWVuY2UiOiJ3ZWIiLCJjcmVhdGVkIjoxNTg4MTU2MTIzNzMzLCJleHAiOjE1ODgxNTc5MjN9.u2QZ5zMjBPQmQDTwsYAM0KXda8Dtlu_IJIxgtkZqRT8hV4CR43B5T9xJIDZdHzueopegpElzSQwQBlUCYmzKNw";
        //encodeUrl = "https://wallet.iserveu.online/cashout/getbank/cashout.json";


        AndroidNetworking.get(encodeUrl)
                .addHeaders("Authorization",token)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            getWallet1Balance(); // calling wallet Balance


                            String active = response.getString("active");
                            /*
                             * If account is active showing bank Info
                             * */
                            if(active.equalsIgnoreCase("true")){
                                String message = response.getString("message");
                                bankNameStr = response.getString("bankName");
                                accNoStr = response.getString("accountNumber");
                                ifscStr = response.getString("ifsc");
                                bankCodeStr = response.getString("bankCode");
                                accNo.setText("Acc No. :"+accNoStr);
                                addAccount.setVisibility(View.GONE);
                                transferAmount.setVisibility(View.VISIBLE);
                                addAmountEdit.setVisibility(View.VISIBLE);
                                amontplacehoder.setVisibility(View.VISIBLE);
                                imgInfo.setVisibility(View.GONE);
                                transferAmount.setBackground(ContextCompat.getDrawable(WalletToBankActivity.this, R.drawable.enable_button_background));
                                transferAmount.setEnabled(true);
                                addAmountEdit.setEnabled(true);
                                addAmountEdit.setFocusable(true);
                                img_prof.setVisibility(View.VISIBLE);

                            }else{
                                accNoStr = response.getString("accountNumber");

                                if(accNoStr.equalsIgnoreCase("")){
                                    accNo.setText("xxxx xxxx xxxx");
                                    addAccount.setVisibility(View.VISIBLE);
                                    Util.showAlert(WalletToBankActivity.this,"Alert","No Bank account linked with this ID.");
                                    transferAmount.setVisibility(View.GONE);
                                    addAmountEdit.setVisibility(View.GONE);
                                    amontplacehoder.setVisibility(View.GONE);
                                    imgInfo.setVisibility(View.GONE);
                                    img_prof.setVisibility(View.VISIBLE);


                                }
                                else{

                                    //Util.showAlert(WalletToBankActivity.this,"Alert","Sorry, Account No. is not active.");

                                    accNo.setText("Acc no: "+accNoStr);
                                    addAccount.setVisibility(View.GONE);
                                    imgInfo.setVisibility(View.VISIBLE);
                                    img_prof.setVisibility(View.VISIBLE);
                                    transferAmount.setVisibility(View.VISIBLE);
                                    addAmountEdit.setVisibility(View.VISIBLE);
                                    amontplacehoder.setVisibility(View.VISIBLE);
                                    transferAmount.setBackground(ContextCompat.getDrawable(WalletToBankActivity.this, R.drawable.disable_button_background));
                                    transferAmount.setEnabled(false);
                                    addAmountEdit.setEnabled(false);
                                    addAmountEdit.setFocusable(false);
                                }

                            }


                        } catch (JSONException e) {
//                                e.printStackTrace();
                            pd.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        String str = anError.getErrorBody();
                        pd.dismiss();

                    }
                });


    }




    public void getWallet1Balance() {
        try {

            AndroidNetworking.get("https://itpl.iserveu.tech/generate/v72")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject obj = new JSONObject(response.toString());
                                String key = obj.getString("hello");
                                System.out.println(">>>>-----"+key);
                                byte[] data = Base64.decode(key,Base64.DEFAULT);
                                String encodedUrl = new String(data, "UTF-8");

                                getencriptedWallet1Balance(encodedUrl);

                            } catch (JSONException e) {
//                                e.printStackTrace();
                            } catch (UnsupportedEncodingException e) {
//                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });

        }catch (Exception e){
//            e.printStackTrace();
        }
    }

    public void getencriptedWallet1Balance(String encodedUrl){
        AndroidNetworking.get(encodedUrl)
                .setPriority(Priority.HIGH)
                //.addBodyParameter(map)
                .addHeaders("Authorization",token)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        pd.dismiss();
                        String wallet1_balance = response;
                        wal1_bal_edt.setText("Balance : ₹ "+wallet1_balance);
                    }
                    @Override
                    public void onError(ANError anError) {
                        anError.getErrorBody();
                        pd.dismiss();
                    }
                });
    }


    public void showAlertt(Context context, String title, String message) {

        try {
            if (!isFinishing() && !isDestroyed()) {

                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(context);
                }
                builder.setTitle(title)
                        .setMessage(message)
                        .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                dialog.dismiss();
                                getWallet1Balance();
                            }
                        })
                        .show();
            }
        }catch (Exception e){

        }
    }

    public  void showAlertOnBack(Context context, String title, String message) {

        try {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Light_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(context);
            }
            builder.setTitle(title)
                    .setCancelable(false)
                    .setMessage(message)
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // continue with delete
                            dialog.dismiss();
                            onBackPressed();

                        }
                    })
                    .show();

        }catch (Exception e){

        }
    }

   /* @Override
    protected void onResume() {
        super.onResume();
        startTimer();
        if(_userName!=null) {
            checkSessionExistance(_userName,true);
        }
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();


        if(_userName!=null && session_logout==false) {
            checkSessionExistance(_userName,false);
        }
        //startTimer();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        if(_userName!=null && session_logout==false) {
            checkSessionExistance(_userName,false);
        }
    }

    public void startTimer(){
        if(timer_handler!=null) {
            timer_handler.removeCallbacksAndMessages(null);
        }
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                timer_handler = new Handler();
                timer_handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(timer_handler!=null) {
                            timer_handler.removeCallbacksAndMessages(null);
                        }

                        if(!isFinishing())
                        {
                            session_logout = true;
                            if(_userName!=null) {
                                checkSessionExistance(_userName,false);
                            }
                            showSessionAlert();
                        }
                        //Toast.makeText(MainActivity.this, "LOGOUT", Toast.LENGTH_SHORT).show();
                    }
                }, session_time);
            }
        });
    }
    public void showSessionAlert(){
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session Timeout !!! Please login again.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();

                        Intent i = new Intent(WalletToBankActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }


    private void checkSessionExistance(String user_name,boolean status){
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        //asynchronously retrieve all documents

        DocumentReference docRef = db.collection("CoreApp_Session_Manager").document(user_name.toLowerCase());
        docRef.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot document = task.getResult();
                    if (document.exists()) {
                        try {
                            if(document.contains("login_status")) {

                                String value = document.getData().get("login_status").toString();
                                String deviceID = document.getData().get("device_id").toString();

                                if(deviceID.equalsIgnoreCase(com.isuisudmt.utils.Constants.getDeviceID(WalletToBankActivity.this))){

                                    com.isuisudmt.utils.Constants.updateSession(WalletToBankActivity.this, user_name, status);

                                }else{
                                    showSessionAlert2(); // Toast.makeText(SplashActivity.this, "", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    } else {

                    }
                } else {

                }
            }
        });
    }

    public void showSessionAlert2(){
        android.app.AlertDialog.Builder alertbuilderupdate;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            alertbuilderupdate = new android.app.AlertDialog.Builder(this);
        }
        alertbuilderupdate.setCancelable(false);
        String message = "Session logon by some other device while inactive. Please check and try to login after sometimes.";
        alertbuilderupdate.setTitle("Alert")
                .setMessage(message)
                .setPositiveButton(this.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                        Intent i = new Intent(WalletToBankActivity.this, LoginActivity.class);
                        // set the new task and clear flags
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        finish();
                    }
                })
                .show();
    }*/

}
